INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '2', 'Envíos', 'Envios', 'fa fa-plane');
INSERT INTO `perfiles_detalles` (`id_perfil_detalle`, `id_perfil`, `id_usuario`, `MenusubId`) VALUES (NULL, '1', '1', '5');

ALTER TABLE `envios` ADD `tipo` VARCHAR(1) NOT NULL AFTER `folio`;
ALTER TABLE `envios` CHANGE `nom_envia` `nom_envia` VARCHAR(100) NOT NULL, CHANGE `calle_envia` `calle_envia` VARCHAR(55) NOT NULL, CHANGE `ciudad_envia` `ciudad_envia` VARCHAR(45) NOT NULL, CHANGE `edo_envia` `edo_envia` VARCHAR(45) NOT NULL, CHANGE `cp_envia` `cp_envia` VARCHAR(5) NOT NULL, CHANGE `tel_envia` `tel_envia` VARCHAR(15) NOT NULL;

ALTER TABLE `usuarios` ADD `nombre` VARCHAR(55) NOT NULL AFTER `id_perfil`, ADD `apellidos` VARCHAR(75) NOT NULL AFTER `nombre`;
ALTER TABLE `envios` ADD `fecha_entregado` DATETIME NOT NULL AFTER `entregado`;
ALTER TABLE `usuarios` ADD `id_tienda` INT NOT NULL AFTER `apellidos`;
ALTER TABLE `usuarios` ADD  CONSTRAINT `tienda_fk_usuarios` FOREIGN KEY (`id_tienda`) REFERENCES `tienda`(`id`) ON DELETE RESTRICT ON UPDATE NO ACTION;
ALTER TABLE `tienda` ADD `direccion` TEXT NOT NULL AFTER `lugar`;

ALTER TABLE `envios` ADD `id_cliente` INT NOT NULL AFTER `id`;
ALTER TABLE `envios` ADD `id_cliente_envia` INT NOT NULL AFTER `tel_recibe`;

ALTER TABLE `envios` ADD `metodo` VARCHAR(1) NOT NULL AFTER `tipo`;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES (NULL, '2', 'Corte de Caja', 'CorteCaja', 'fa fa-usd');
INSERT INTO `perfiles_detalles` (`id_perfil_detalle`, `id_perfil`, `id_usuario`, `MenusubId`) VALUES (NULL, '1', '1', '6');

/* ***********************15-08-22*******************************/
ALTER TABLE `tienda` ADD `telefono` VARCHAR(20) NOT NULL AFTER `lugar`;
ALTER TABLE `tienda` ADD `letra` VARCHAR(5) NOT NULL AFTER `telefono`;


ALTER TABLE `tienda` CHANGE `telefono` `telefono` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;
ALTER TABLE `envios` ADD `comentarios` TEXT NOT NULL AFTER `id_usuario`;
ALTER TABLE `clientes` CHANGE `telefono` `telefono` VARCHAR(55) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL, CHANGE `telefono2` `telefono2` VARCHAR(55) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;
ALTER TABLE `clientes` CHANGE `telefono` `telefono` VARCHAR(55) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL;

/* *****************19-09-22***************** */
ALTER TABLE `envios` CHANGE `tel_recibe` `tel_recibe` VARCHAR(35) NOT NULL;
ALTER TABLE `envios` CHANGE `tel_envia` `tel_envia` VARCHAR(35) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;
ALTER TABLE `envios` ADD `semana` TINYINT NOT NULL AFTER `reg`;

/* **********user ftp************* */
/*
mangoo@tulcingopaqueteria.com
mangoo123456
*/

/* **********************24-08-23**************************** */
ALTER TABLE `clientes` ADD `tipo` VARCHAR(1) NOT NULL DEFAULT '1' COMMENT '1=EEUU,2=MX' AFTER `apellidos`;
/* **********************30-08-23************************************ */
ALTER TABLE `tienda` ADD `caja` VARCHAR(1) NOT NULL AFTER `letra`;