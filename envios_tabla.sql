-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 06-07-2022 a las 20:34:49
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tulcingo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envios`
--

DROP TABLE IF EXISTS `envios`;
CREATE TABLE IF NOT EXISTS `envios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_recibe` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_recibe` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `calle_recibe` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `ciudad_recibe` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `edo_recibe` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `cp_recibe` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `tel_recibe` int(15) NOT NULL,
  `nom_envia` int(100) NOT NULL,
  `apellido_envia` varchar(55) COLLATE utf8_spanish_ci NOT NULL,
  `calle_envia` int(55) NOT NULL,
  `ciudad_envia` int(45) NOT NULL,
  `edo_envia` int(45) NOT NULL,
  `cp_envia` int(5) NOT NULL,
  `tel_envia` int(15) NOT NULL,
  `id_origen` int(11) NOT NULL,
  `id_destino` int(11) NOT NULL,
  `folio` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `reg` datetime NOT NULL,
  `entregado` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0' COMMENT '0=no,1=entregado',
  `id_usuario` int(11) NOT NULL,
  `estatus` varchar(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
