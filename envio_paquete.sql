-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 06-07-2022 a las 20:35:30
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tulcingo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envio_paquete`
--

DROP TABLE IF EXISTS `envio_paquete`;
CREATE TABLE IF NOT EXISTS `envio_paquete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_envio` int(11) NOT NULL,
  `num_paquete` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `total` float NOT NULL,
  `contenido` text COLLATE utf8_spanish_ci NOT NULL,
  `num_sub` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `libras` float NOT NULL,
  `precio` float NOT NULL,
  `valor_est` float NOT NULL,
  `impuesto` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `envio_fk_paquete` (`id_envio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `envio_paquete`
--
ALTER TABLE `envio_paquete`
  ADD CONSTRAINT `envio_fk_paquete` FOREIGN KEY (`id_envio`) REFERENCES `envios` (`id`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
