<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Tulcingo Travel</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(); ?>public/img/logo.png" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url();?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url();?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url();?>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url();?>plugins/style.css" rel="stylesheet">
    <script>
        var base_url="<?php echo base_url(); ?>";
    </script>
</head>

<style type="text/css">
    .bg-blue{
        background-color:#e80406 !important;
    }
    .login-page {
        background-color: #ffffff !important;
    }
    .cx3 {
        background-color: #e80406 !important;
    }
    .cx2 {
        background-color: #398335 !important; opacity: 0.2;
    }
    .cx1{
        background-color: white !important;
    }
    .col-blue {
        color: rgb(233,98,0) !important;
    }
    .col_vd{
        color: #398335;
    }
    .img_logo{
        margin-top: 30%
    }
    @media (max-width:500px){
        .login_texto img{
            margin-top: 0px;
        }
        .logo{
            display: none;
        }

    }
</style>
<body class="login-page">
    <div class="sesion_texto" style="width: 400px;
    height: 400px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -200px;
    margin-top: -200px;
    z-index: 1;
    display: none;" align="center">
            <!-- style="background: #ffffffad; border-radius: 20px;" 
            
            -->   
        <img style="" src="<?php echo base_url();?>public/img/logo.png" width="50%" class="m-t-90"  />
        
        <h2 style="color: white;">Iniciando Sesión</h2>
        <!--<h3 style="color: black;">Conectado a la base de datos</h3>-->
    </div>

    <div class="cx3"></div>
    <div class="cx2"></div>
    <div class="cx1"></div>
    
    <div class="login-box" align="center">
        <div class="card login_texto" style="border-radius: 41px;">
            <div class="body">
                <form id="sign_in" method="POST">
                    <div class="row align-center">
                        <div class="col-md-6" >
                            <br><br><br><br><br><br><br><br><br>
                           <img style="align-items: center;" src="<?php echo base_url();?>public/img/logo.png" width="90%" />
                           <!--<img src="<?php echo base_url();?>images/handu4.jpg" width="70%" />-->
                        </div>
                        <div class="col-md-6" style="border-left: 1px solid #398335;">
                            <i class="material-icons font-40 m-t-20 col_vd">lock</i>
                            <h2>Accede a tu cuenta</h2>
                            <h5>Estamos contentos de que regreses</h5>
                            <br>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="usuario" placeholder="Nombre de Usuario" required autofocus>
                                </div>
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" placeholder="Contraseña" required>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-xs-12">
                                    <button class="btn btn-block col_vd waves-effect" style="background-color:#398335;  color: black !important;" type="submit">INICIAR SESIÓN</button>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </form>
            </div>
        <div>    
    </div>
    </div>
    <div class="text-center msjlog" id="loader">
        <div class="lds-ripple"><div></div><div></div></div>
    </div>
    <div class="alert bg-pink" id="error" style="background:rgba(0, 0, 0, 0.2); position: relative;">
        <i class="material-icons">error </i> <strong>Error! </strong>&nbsp El nombre de usuario y/o contraseña son incorrectos 
    </div>
    
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url();?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url();?>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url();?>plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url();?>plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url();?>public/js/login.js"></script>
</body>

</html>