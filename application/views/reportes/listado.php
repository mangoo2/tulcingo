<div class="page-wrapper">  
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Manifiestos</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>Inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Manifiestos</li>
                    </ol>
                    <!--<a href="<?php echo base_url();?>Envios/alta"><button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i
                            class="fa fa-plus-circle"></i> Agregar nueva</button></a>-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Lista de Envíos</h4>
                        <!--<h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>-->
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Zona</label>
                                    <select class="form-control custom-select" id="zona">
                                        <option value="1">Zona 1</option>
                                        <option value="2">Zona 2</option>
                                        <option value="3">Zona 3</option>
                                        <option value="4">Zona 4</option>
                                        <option value="5">Zona 5</option>
                                        <option value="6">Zona 6</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Cliente</label>
                                    <select class="form-control custom-select" id="cliente">

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Fecha Inicio</label>
                                    <input type="date" id="registro" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Fecha Fin</label>
                                    <input type="date" id="registro_fin" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <br>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-danger" id="exportar"> <i class="fa fa-file-pdf"></i> Exportar Manifiesto</button>
                                </div>
                            </div>

                        </div>
                        <div class="table-responsive m-t-40">
                           
                        </div>
                    </div>
                </div>

            </div>
            
        </div>
    </div>
</div>
