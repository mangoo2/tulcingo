<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

    $tot_paqs=0;
    $fecha_entregado="";
    setlocale(LC_ALL, 'es_ES');
    foreach ($env as $item){
      $envia = $item->nom_envia." ".$item->apellido_envia;
      $recibe = $item->nom_recibe." ".$item->apellido_recibe;
      $tipo = $item->tipo;
      $id=$item->id;
      $folio=$item->folio;
      //$fecha_entregado=$item->fecha_entregado;
      //$dateObj   = DateTime::createFromFormat('!m', date("m", strtotime($item->fecha_entregado)));
      //$monthName = strftime('%B', $dateObj->getTimestamp());
      $fecha_entregado=date("d-m-Y", strtotime($item->fecha_entregado));
      $hora_entregado=date("h:i:s a", strtotime($item->fecha_entregado));
      $fecha_entregado = strftime("%d/%B/%Y", strtotime($fecha_entregado));

      $dest="";
      $getdest= $this->General_model->getselectwhereall("tienda",array("id"=>$item->id_destino));
      foreach ($getdest as $d) {  
        $dest = $d->nombre;
        if($tipo==1){
          $letra = $d->letra;
        }
      }
      $orig="";
      $getorg= $this->General_model->getselectwhereall("tienda",array("id"=>$item->id_origen));
      foreach ($getorg as $d) {  
        $orig = $d->nombre;
        if($tipo==2){
          $letra = $d->letra;
        }
        //$letrao = $d->letra;
      }
    }
    $contenido="";
    foreach ($env_paq as $i) {         
      $num_paq = $i->num_paquete;
      $tot_paqs++;
      $contenido.=$i->contenido."<br>";
    }
    
    if($tipo=="1"){ //eu
      $pri = substr($orig,0,1); 
      $num_env = $pri."".$folio;
    }else{
      if(is_numeric($letra)==true){
          $letra = intVal($letra);
          $num_env = $letra+intVal($folio);
        }else{
        $num_env = $letra."".$folio; 
      }
    }
//=======================================================================================
 
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(110, 210), true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Etiqueta de Entrega');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set margins
$pdf->SetMargins('6', '6', '6');
//$pdf->SetFooterMargin('10');

// set auto page breaks
//$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);// margen del footer
// set auto page breaks
//$pdf->SetAutoPageBreak(true,200);

$pdf->SetFont('dejavusans', '', 14);
// add a page

$pdf->AddPage();
$html = '
          <table width="100%" border="0" >
            <tr>
              <td style="font-size:20px" align="center" colspan="4"><b>'.$num_env.' - '.$tot_paqs.' PAQ</b></td>
            </tr>
            <tr>
              <td style="font-size:16px" align="center" colspan="4"><b>'.$orig.' '.$letra.'</b></td>
            </tr>
            <tr>
              <td style="font-size:16px" align="center" colspan="4"><b>'.$fecha_entregado.'</b></td>
            </tr>
            <tr>
              <td style="font-size:10px" align="left" colspan="4">'.$contenido.'</td>
            </tr>
            <tr>
              <td style="font-size:12px" align="left" colspan="4"><i>E:'.$envia.'</i></td>
            </tr>
            <tr>
              <td style="font-size:12px" align="left" colspan="4"><i>R:'.$recibe.'</i></td>
            </tr>
            <tr>
              <td style="font-size:12px" align="left" colspan="4"><i>'.$hora_entregado.'</i></td>
            </tr>
            <tr>
              <td style="font-size:12px" align="left" colspan="4"><i>Impreso por: '.$imprime.'</i></td>
            </tr>
            <tr>
              <td style="font-size:12px" align="left" colspan="4"><i>Entregó: ______________</i></td>
            </tr>
          </table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->IncludeJS('print(true);');
$pdf->Output('Captura.pdf', 'I');

?>