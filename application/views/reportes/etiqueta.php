<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

    $tot_paqs=0;
    foreach ($env as $item){
      $envia = $item->nom_envia." ".$item->apellido_envia;
      $recibe = $item->nom_recibe." ".$item->apellido_recibe;
      $tipo = $item->tipo;
      $id=$item->id;
      $folio=$item->folio;
      $dest="";
      $getdest= $this->General_model->getselectwhereall("tienda",array("id"=>$item->id_destino));
      foreach ($getdest as $d) {  
        $dest = $d->nombre;
      }
      $orig="";
      $getorg= $this->General_model->getselectwhereall("tienda",array("id"=>$item->id_origen));
      foreach ($getorg as $d) {  
        $orig = $d->nombre;
        $letra = $d->letra;
      }
    }
    foreach ($env_paq as $i) {         
      $num_paq = $i->num_paquete;
      $tot_paqs++;
    }
    
    if($tipo=="1"){ //eu
      $pri = substr($orig,0,1); 
      $num_env = $pri."".$folio;
    }else{
      if(is_numeric($letra)==true){
        $letra = intVal($letra);
        $num_env = $letra+intVal($folio);
      }else{
        $num_env = $letra."".$folio; 
      }
      //$num_env = $letra."".$folio;
    }
//=======================================================================================
 
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(100, 210), true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Etiquetas');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set margins
$pdf->SetMargins('4', '2', '4');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
//$pdf->SetAutoPageBreak(true, 170);// margen del footer

$pdf->SetFont('dejavusans', '', 15);

$pdf->AddPage('L');
$html = '
          <table width="100%" border="0">
            <tr>
              <td colspan="4"></td>
            </tr>
            <tr>
              <td style="font-size:45px" align="center" colspan="4">'.$num_env.' - '.$tot_paqs.' PAQ</td>
            </tr>
            <tr>
              <td style="font-size:45px" align="center" colspan="4">'.$dest.'</td>
            </tr>
            <tr>
              <td style="font-size:28px" align="center" colspan="4">E:'.$envia.'</td>
            </tr>
            <tr>
              <td style="font-size:28px" align="center" colspan="4">R:'.$recibe.'</td>
            </tr>

          </table>
          ';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->IncludeJS('print(true);');

$pdf->Output('Captura.pdf', 'I');
?>