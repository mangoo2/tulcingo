<?php
require_once('TCPDF4/tcpdf.php');
$this->load->helper('url');

foreach ($env as $item) {
  $envia = $item->nom_envia . " " . $item->apellido_envia;
  $tel_envia = $item->tel_envia;
  $direccion_envia = $item->calle_envia . ", " . $item->ciudad_envia . ", C.P. " . $item->cp_envia;

  $recibe = $item->nom_recibe . " " . $item->apellido_recibe;
  $tel_recibe = $item->tel_recibe;
  $direccion_recibe = $item->calle_recibe . ", " . $item->ciudad_recibe . ", C.P. " . $item->cp_recibe;

  $id = $item->id;

  $getdest = $this->General_model->getselectwhereall("tienda", array("id" => $item->id_destino));
  foreach ($getdest as $d) {
    $dest = $d->nombre;
    $direccion = $d->direccion;
  }

  $getuser = $this->General_model->getselectwhereall("usuarios", array("id" => $item->id_usuario));
  foreach ($getuser as $u) {
    $user = $u->usuario;
    $user_nom = $u->nombre ." ". $u->apellidos;
  }
}

$sum_paq = 0;
$sum_ve = 0;
$sum_lbs = 0;
$sum_total = 0;
foreach ($env_paq as $i) {
  $paq = $i->num_paquete;
  $id_envio = $i->id_envio;
  $contenido = $i->contenido;
  $subpaq = $i->num_sub;
  $lbs = $i->libras;
  $precio = $i->precio;
  $total = $i->total;
  $valor_est = $i->valor_est;

  $sum_paq +=  $paq;
  $sum_ve +=  $valor_est;
  $sum_lbs +=  $lbs;
  $sum_total +=  $total;
}

$logo = base_url() . "public/img/logo.png";
//=======================================================================================
class MYPDF extends TCPDF
{

  //Page header
  public function Header()
  {
    $html = '
          
          <table width="100%" border="1" cellpadding="4px" class="info_fac">
            <tr>
              <td width="100%"></td>
            </tr>
            
          ';
    $this->writeHTML($html, true, false, true, false, '');
  }
  // Page footer
  public function Footer()
  {
    $html2 = '';

    $html2 .= '
      <table width="100%" border="0" cellpadding="2" class="fontFooterp">
        <tr>
          <td width="100%"></td>
        </tr>
      </table>
    ';

    $this->writeHTML($html2, true, false, true, false, '');
  }
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Recibo');
$pdf->SetSubject('recibo de envío');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('5', '10', '5');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('10');

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);
// add a page
$pdf->AddPage('P');
$html = '
          <table width="100%" border="0" cellspacing="4">
            <tbody>
              <tr>
                <td colspan="4">Despachó: ' . $user_nom . '</td>
              </tr>
              <tr>
                <td colspan="1" width="10%">
                  <img src="' . $logo . '" width="100" height="100">
                </td>
                <td colspan="1" width="40%"><span style="font-weight:bold">TULCINGO TRAVEL</span> <br />
                  85-16 Roosevelt Ave. 1fl., Jackson<br />
                  Height, NY 11372<br />
                  Tel: 718 639-9153, 718 639-9154
                </td>
                <td colspan="2" width="50%"><span style="font-weight:bold">Paquete #: <span style="font-size: 250%">' . $id_envio . '  </span></span> <br /><br />
                  Destino: '.$dest.' <br>
                  '.$direccion.' <br>
                  Tel: 243 43 10019 <br />
                  06/07/2022 03:40:29
                </td>
              </tr>
              <tr>
                <td colspan="2"></td>
                <td colspan="2" style="font-weight:bold">*SALIENDO DE LA SUCURSAL, NO SE ACEPTAN RECLAMOS
                </td>
              </tr>
              <tr>
                <td colspan="2"><span style="font-weight:bold">REMITENTE</span> <br />
                  NOMBRE: ' . $envia . ' <br />
                  TEL: ' . $tel_envia . ' <br />
                  DIRECCION: ' . $direccion_envia . '
                </td>
                <td colspan="2"><span style="font-weight:bold">BENEFICIARIO</span> <br />
                  NOMBRE: ' . $recibe . ' <br />
                  TEL: ' . $tel_recibe . ' <br />
                  DIRECCION: ' . $direccion_recibe . '
                </td>
              </tr>
              <tr>
                <td colspan="4">
                  <table width="100%" align="center" style="border-collapse:collapse">
                    <thead>
                      <tr style="background-color:#c0c0c0">
                        <th style="border:1px solid black;border-collapse:collapse" width="5%">PAQ</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="50%">CONTENIDO</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="7.5%">SUBPAQ</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="7.5%">VALOR ESTIMADO</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="7.5%">OTROS CARGOS</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="7.5%">LBS</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="7.5%">COSTO LB X PAQ</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="7.5%">TOTAL</th>
                      </tr>
                    </thead>
                    <tbody>';
                    foreach ($env_paq as $key => $env) {
                      $html .= '
                        <tr>
                          <td style="border:1px solid black;border-collapse:collapse">' . $env->num_paquete . '</td>
                          <td style="border:1px solid black;border-collapse:collapse">' . $env->contenido . '</td>
                          <td style="border:1px solid black;border-collapse:collapse">' . $env->num_sub . '</td>
                          <td style="border:1px solid black;border-collapse:collapse">' . $env->valor_est . '</td>
                          <td style="border:1px solid black;border-collapse:collapse">0</td>
                          <td style="border:1px solid black;border-collapse:collapse">' . $env->libras . '</td>
                          <td style="border:1px solid black;border-collapse:collapse">0</td>
                          <td style="border:1px solid black;border-collapse:collapse">' . $env->total . '</td>
                        </tr>';
                    }
                    $html .= '
                      <tr>
                        <td style="border-left:1px solid black;border-bottom:1px solid black;border-collapse:collapse">' . $sum_paq . '</td>
                        <td style="border-right:1px solid black;border-bottom:1px solid black;border-collapse:collapse"> </td>
                        <td style="border-left:1px solid black;border-bottom:1px solid black;border-collapse:collapse"> </td>
                        <td style="border-right:1px solid black;border-bottom:1px solid black;border-collapse:collapse">' . $sum_ve . ' </td>
                        <td style="border:1px solid black;border-collapse:collapse">0</td>
                        <td style="border:1px solid black;border-collapse:collapse">' . $sum_lbs . '</td>
                        <td style="border:1px solid black;border-collapse:collapse">0</td>
                        <td style="border:1px solid black;border-collapse:collapse">' . $sum_total . '</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td colspan="3" width="70%"></td>
                <td colspan="1"  width="30%" align="center">Acepto de Conformidad<br /><br />
                  ________________
                </td>
              </tr>
              <tr>
                <td colspan="4"><small>Nota: En caso de alguna perdida, se le devolverá el valor que usted declaro.
                    <span style="font-weight:bold">La empresa no se hace responsable por articulos no declarados</span>.
                    Es estrictamente prohibido meter dinero en los sobres o paquetes, esto es un delito federal.
                    <span style="font-weight:bold">La compañia no se hace responsable por paquetes no revisados por su familiar en
                      MEXICO.</span><br />
                    GRACIAS POR SU COMPRENSION.
                  </small>
                </td>
              </tr>
            </tbody>
          </table>

          <table width="100%" border="0" cellspacing="4">
            <tbody>
              <tr>
                <td colspan="4">Despachó: ' . $user_nom . '</td>
              </tr>
              <tr>
                <td colspan="1" width="10%">
                  <img src="' . $logo . '" width="100" height="100">
                </td>
                <td colspan="1" width="40%"><span style="font-weight:bold">TULCINGO TRAVEL</span> <br />
                  85-16 Roosevelt Ave. 1fl., Jackson<br />
                  Height, NY 11372<br />
                  Tel: 718 639-9153, 718 639-9154
                </td>
                <td colspan="2" width="50%"><span style="font-weight:bold">Paquete #: <span style="font-size: 250%">' . $id_envio . '  </span></span> <br /><br />
                  Destino: '.$dest.' <br>
                  '.$direccion.' <br>
                  Tel : 243 43 10019 <br />
                  06/07/2022 03:40:29
                </td>
              </tr>
              <tr>
                <td colspan="2"></td>
                <td colspan="2" style="font-weight:bold">*SALIENDO DE LA SUCURSAL, NO SE ACEPTAN RECLAMOS
                </td>
              </tr>
              <tr>
                <td colspan="2"><span style="font-weight:bold">REMITENTE</span> <br />
                  NOMBRE: ' . $envia . ' <br />
                  TEL: ' . $tel_envia . ' <br />
                  DIRECCION: ' . $direccion_envia . '
                </td>
                <td colspan="2"><span style="font-weight:bold">BENEFICIARIO</span> <br />
                  NOMBRE: ' . $recibe . ' <br />
                  TEL: ' . $tel_recibe . ' <br />
                  DIRECCION: ' . $direccion_recibe . '
                </td>
              </tr>
              <tr>
                <td colspan="4">
                  <table width="100%" align="center" style="border-collapse:collapse">
                    <thead>
                      <tr style="background-color:#c0c0c0">
                        <th style="border:1px solid black;border-collapse:collapse" width="5%">PAQ</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="50%">CONTENIDO</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="7.5%">SUBPAQ</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="7.5%">VALOR ESTIMADO</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="7.5%">OTROS CARGOS</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="7.5%">LBS</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="7.5%">COSTO LB X PAQ</th>
                        <th style="border:1px solid black;border-collapse:collapse" width="7.5%">TOTAL</th>
                      </tr>
                    </thead>
                    <tbody>';
                    foreach ($env_paq as $key => $env) {
                      $html .= '
                        <tr>
                          <td style="border:1px solid black;border-collapse:collapse">' . $env->num_paquete . '</td>
                          <td style="border:1px solid black;border-collapse:collapse">' . $env->contenido . '</td>
                          <td style="border:1px solid black;border-collapse:collapse">' . $env->num_sub . '</td>
                          <td style="border:1px solid black;border-collapse:collapse">' . $env->valor_est . '</td>
                          <td style="border:1px solid black;border-collapse:collapse">0</td>
                          <td style="border:1px solid black;border-collapse:collapse">' . $env->libras . '</td>
                          <td style="border:1px solid black;border-collapse:collapse">0</td>
                          <td style="border:1px solid black;border-collapse:collapse">' . $env->total . '</td>
                        </tr>';
                    }
                    $html .= '
                      <tr>
                        <td style="border-left:1px solid black;border-bottom:1px solid black;border-collapse:collapse">' . $sum_paq . '</td>
                        <td style="border-right:1px solid black;border-bottom:1px solid black;border-collapse:collapse"> </td>
                        <td style="border-left:1px solid black;border-bottom:1px solid black;border-collapse:collapse"> </td>
                        <td style="border-right:1px solid black;border-bottom:1px solid black;border-collapse:collapse">' . $sum_ve . ' </td>
                        <td style="border:1px solid black;border-collapse:collapse">0</td>
                        <td style="border:1px solid black;border-collapse:collapse">' . $sum_lbs . '</td>
                        <td style="border:1px solid black;border-collapse:collapse">0</td>
                        <td style="border:1px solid black;border-collapse:collapse">' . $sum_total . '</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td colspan="3" width="70%"></td>
                <td colspan="1"  width="30%" align="center">Acepto de Conformidad<br /><br />
                  ________________
                </td>
              </tr>
              <tr>
                <td colspan="4"><small>Nota: En caso de alguna perdida, se le devolverá el valor que usted declaro.
                    <span style="font-weight:bold">La empresa no se hace responsable por articulos no declarados</span>.
                    Es estrictamente prohibido meter dinero en los sobres o paquetes, esto es un delito federal.
                    <span style="font-weight:bold">La compañia no se hace responsable por paquetes no revisados por su familiar en
                      MEXICO.</span><br />
                    GRACIAS POR SU COMPRENSION.
                  </small>
                </td>
              </tr>
            </tbody>
          </table>

          ';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->IncludeJS('print(true);');
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
