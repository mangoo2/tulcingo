<?php
require_once('TCPDF4/tcpdf.php');
$this->load->helper('url');

foreach ($env as $item) {
  $envia = $item->nom_envia . " " . $item->apellido_envia;
  $tel_envia = $item->tel_envia;
  $direccion_envia = $item->calle_envia . ", " . $item->ciudad_envia . " " .$item->edo_recibe." ". $item->cp_envia;

  $recibe = $item->nom_recibe . " " . $item->apellido_recibe;
  $tel_recibe = $item->tel_recibe;
  $direccion_recibe = $item->calle_recibe . ", " . $item->ciudad_recibe  . " " .$item->edo_envia." ". $item->cp_recibe;

  $id = $item->id;
  $folio=$item->folio;
  $tipo = $item->tipo;
  $tel_dest=""; $dest="";
  $getdest = $this->General_model->getselectwhereall("tienda", array("id" => $item->id_destino));
  foreach ($getdest as $d) {
    $dest = $d->nombre;
    $direc_tienda = $d->direccion;
    $tel_dest=$d->telefono;
  }

  $direc_tienda_org=""; $tel_org=""; $orig="";
  $getorg= $this->General_model->getselectwhereall("tienda",array("id"=>$item->id_origen));
  foreach ($getorg as $d) {  
    $direc_tienda_org = $d->direccion;
    $tel_org=$d->telefono;
    $orig = $d->nombre;
    $letra = $d->letra;
  }

  $getuser = $this->General_model->getselectwhereall("usuarios", array("id" => $item->id_usuario));
  foreach ($getuser as $u) {
    $user = $u->usuario;
    $user_nom = $u->nombre ." ". $u->apellidos;
  }
}

$sum_paq = 0;
$sum_sub = 0;
$sum_lbs = 0;
$sum_precio = 0;
$sum_total = 0;
foreach ($env_paq as $i) {
  $paq = $i->num_paquete;
  $id_envio = $i->id_envio;
  $contenido = $i->contenido;
  $subpaq = $i->num_sub;
  $lbs = $i->libras;
  $precio = $i->precio;
  $total = $i->total;
  
  $sum_sub += $subpaq;
  $sum_lbs +=  $lbs;
  $sum_precio += $precio;
  $sum_total +=  $total;
}

if($tipo=="1"){ //eu
  $pri = substr($orig,0,1); 
  $num_env = $pri."".$folio;
}else{
  if(is_numeric($letra)==true){
    $letra = intVal($letra);
    $num_env = $letra+intVal($folio);
  }else{
    $num_env = $letra."".$folio; 
  }
  //$num_env = $letra."".$folio;
}

$logo = base_url() . "public/img/logo.png";
//=======================================================================================
class MYPDF extends TCPDF
{

  //Page header
  public function Header()
  {
    $html = '
          
          <table width="100%" border="0" cellpadding="4px" class="info_fac">
            <tr>
              <td width="100%"></td>
            </tr>
          </table>
          ';
    $this->writeHTML($html, true, false, true, false, '');
  }
  // Page footer
  public function Footer()
  {
    $html2 = '';

    $html2 .= '
      <table width="100%" border="0" cellpadding="2" class="fontFooterp">
        <tr>
          <td width="100%"></td>
        </tr>
      </table>
    ';

    $this->writeHTML($html2, true, false, true, false, '');
  }
}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mangoo Software');
$pdf->SetTitle('Recibo');
$pdf->SetSubject('recibo de envío');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('5', '10', '5');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('10');

// set auto page breaks
$pdf->SetAutoPageBreak(true,10);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7.8);
// add a page
$pdf->AddPage('P');

$html = '
        <table style="min-height:1500px;" class="table" width="100%" border="0">
          <tbody >
            <tr>
              <td colspan="4">Despachó: ' . $user_nom . '</td>
            </tr>
            <tr>
              <td colspan="1" width="20%">
                <img src="'.$logo.'" width="150">
              </td>
              <td colspan="1" width="30%">TULCINGO TRAVEL <br />
                '.$direc_tienda_org.'<br>Tel: '.$tel_org.'
              </td>
              <td colspan="1" width="30%"><span style="font-weight:bold">Paquete #:</span> <span style="font-weight:bold; font-size:16pt">'.$num_env.' </span> <br />
                Destino: '.$dest.' <br>'.$direc_tienda.'<br>Tel: '.$tel_dest.'
              </td>
              <td align="center" colspan="1" width="20%">Acepto Conformidad<br /><br />
                __________________________
              </td>
            </tr>
            <tr>
              <td colspan="2" style="font-weight:bold"><br>
              </td>
            </tr>
            <tr>
              <td colspan="2"></td>
              <td colspan="2" style="font-weight:bold">*SALIENDO DE LA SUCURSAL, NO SE ACEPTAN RECLAMOS
              </td>
            </tr>
            <tr>
              <td colspan="2" style="font-weight:bold"><br>
              </td>
            </tr>
            <tr>
              <td colspan="2"><span style="font-weight:bold;text-decoration:underline">REMITENTE</span> <br />
                NOMBRE: '.$envia.' <br />
                TEL: '.$tel_envia.' <br />
                DIRECCIÓN: '.$direccion_envia.'
              </td>
              <td colspan="2"><span style="font-weight:bold;text-decoration:underline">BENEFICIARIO</span> <br />
                NOMBRE: '.$recibe.' <br />
                TEL: '.$tel_recibe.' <br />
                DIRECCIÓN: '.$direccion_recibe.'
              </td>
            </tr>
            <tr>
              <td colspan="4" height="220px">
                <table width="100%" align="center" style="border-collapse:collapse;">
                  <thead>
                  <tr><td><br></td></tr>
                    <tr>
                      <th style="border:1px solid black;border-collapse:collapse" width="5%">PAQ</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="50%">CONTENIDO</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="7.5%">SUBPAQ</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="7.5%">$ EST</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="7.5%">OTROS</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="7.5%">LBS</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="7.5%">PRECIO</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="7.5%">TOTAL</th>
                    </tr>
                  </thead>
                  <tbody>';
                  $i=0;
                  foreach ($env_paq as $key => $env) {
                    $i++;
                    $sum_paq = $i;
                    $html .= '
                    <tr>
                      <td style="border:1px solid black;border-collapse:collapse;">1</td>
                      <td style="font-size:9px; border:1px solid black;border-collapse:collapse" align="left">'.$env->contenido.'</td>
                      <td style="border:1px solid black;border-collapse:collapse">'.$env->num_sub.'</td>
                      <td style="border:1px solid black;border-collapse:collapse">0</td>
                      <td style="border:1px solid black;border-collapse:collapse">0</td>
                      <td style="border:1px solid black;border-collapse:collapse">'.$env->libras.'</td>
                      <td style="border:1px solid black;border-collapse:collapse">$'.number_format($env->precio,2,".",",").'</td>
                      <td style="border:1px solid black;border-collapse:collapse">$'.number_format($env->total,2,".",",").'</td>
                    </tr>';
                  }
                  $html .= '
                    <tr>
                      <td style="border:1px solid black;border-collapse:collapse">'.$sum_paq.'</td>
                      <td style="border:1px solid black;border-collapse:collapse"> </td>
                      <td style="border:1px solid black;border-collapse:collapse">'.$sum_sub.'</td>
                      <td style="border:1px solid black;border-collapse:collapse">0</td>
                      <td style="border:1px solid black;border-collapse:collapse">0</td>
                      <td style="border:1px solid black;border-collapse:collapse">'.$sum_lbs.'</td>
                      <td style="border:1px solid black;border-collapse:collapse">$'.number_format($sum_precio,2,".",",").'</td>
                      <td style="border:1px solid black;border-collapse:collapse">$'.number_format($sum_total,2,".",",").'</td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td colspan="4" style="font-size:8px">Nota: En caso de alguna perdida, se le devolverá el valor que usted declaro.
                  <span style="font-weight:bold">La empresa no se hace responsable por articulos no declarados</span>.
                  Es estrictamente prohibido meter dinero en los sobres o paquetes, esto es un delito federal.
                  <span style="font-weight:bold">La compañia no se hace responsable por paquetes no revisados por su familiar en
                    MEXICO.</span><br />
                  GRACIAS POR SU COMPRENSION.
              </td>
            </tr>
            <tr>
              <td style="font-size:5px"></td>
            </tr>
          </tbody>
        </table>';
$html.='<hr/>
        <table style="min-height:1500px;" class="table" width="100%" border="0">
          <tbody>
            <tr>
              <td style="font-size:5px"></td>
            </tr>
            <tr>
              <td colspan="4">Despachó: ' . $user_nom . '</td>
            </tr>
            <tr>
              <td colspan="1" width="20%">
                <img src="'.$logo.'" width="150">
              </td>
              <td colspan="1" width="30%">TULCINGO TRAVEL <br />
                '.$direc_tienda_org.'<br>Tel: '.$tel_org.'
              </td>
              <td colspan="1" width="30%"><span style="font-weight:bold">Paquete #:</span> <span style="font-weight:bold; font-size:16pt">'.$num_env.' </span> <br />
                Destino: '.$dest.' <br>'.$direc_tienda.'<br>Tel: '.$tel_dest.'
              </td>
              <td align="center" colspan="1" width="20%">Acepto Conformidad<br /><br />
                __________________________
              </td>
            </tr>
            <tr>
              <td colspan="2" style="font-weight:bold"><br>
              </td>
            </tr>
            <tr>
              <td colspan="2"></td>
              <td colspan="2" style="font-weight:bold">*SALIENDO DE LA SUCURSAL, NO SE ACEPTAN RECLAMOS
              </td>
            </tr>
            <tr>
              <td colspan="2" style="font-weight:bold"><br>
              </td>
            </tr>
            <tr>
              <td colspan="2"><span style="font-weight:bold;text-decoration:underline">REMITENTE</span> <br />
                NOMBRE: '.$envia.' <br />
                TEL: '.$tel_envia.' <br />
                DIRECCIÓN: '.$direccion_envia.'
              </td>
              <td colspan="2"><span style="font-weight:bold;text-decoration:underline">BENEFICIARIO</span> <br />
                NOMBRE: '.$recibe.' <br />
                TEL: '.$tel_recibe.' <br />
                DIRECCIÓN: '.$direccion_recibe.'
              </td>
            </tr>
            <tr>
              <td colspan="4" height="220px">
                <table width="100%" align="center" style="border-collapse:collapse">
                  <thead>
                  <tr><td><br></td></tr>
                    <tr>
                      <th style="border:1px solid black;border-collapse:collapse" width="5%">PAQ</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="50%">CONTENIDO</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="7.5%">SUBPAQ</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="7.5%">$ EST</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="7.5%">OTROS</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="7.5%">LBS</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="7.5%">PRECIO</th>
                      <th style="border:1px solid black;border-collapse:collapse" width="7.5%">TOTAL</th>
                    </tr>
                  </thead>
                  <tbody>';
                  $i=0;
                  foreach ($env_paq as $key => $env) {
                    $i++;
                    $html .= '
                    <tr>
                      <td style="border:1px solid black;border-collapse:collapse">1</td>
                      <td style="font-size:9px; border:1px solid black;border-collapse:collapse" align="left">'.$env->contenido.'</td>
                      <td style="border:1px solid black;border-collapse:collapse">'.$env->num_sub.'</td>
                      <td style="border:1px solid black;border-collapse:collapse">0</td>
                      <td style="border:1px solid black;border-collapse:collapse">0</td>
                      <td style="border:1px solid black;border-collapse:collapse">'.$env->libras.'</td>
                      <td style="border:1px solid black;border-collapse:collapse">$'.number_format($env->precio,2,".",",").'</td>
                      <td style="border:1px solid black;border-collapse:collapse">$'.number_format($env->total,2,".",",").'</td>
                    </tr>';
                  }
                  $html .= '
                    <tr>
                      <td style="border:1px solid black;border-collapse:collapse">'.$sum_paq.'</td>
                      <td style="border:1px solid black;border-collapse:collapse"> </td>
                      <td style="border:1px solid black;border-collapse:collapse">'.$sum_sub.'</td>
                      <td style="border:1px solid black;border-collapse:collapse">0</td>
                      <td style="border:1px solid black;border-collapse:collapse">0</td>
                      <td style="border:1px solid black;border-collapse:collapse">'.$sum_lbs.'</td>
                      <td style="border:1px solid black;border-collapse:collapse">$'.number_format($sum_precio,2,".",",").'</td>
                      <td style="border:1px solid black;border-collapse:collapse">$'.number_format($sum_total,2,".",",").'</td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td colspan="4" style="font-size:8px">Nota: En caso de alguna perdida, se le devolverá el valor que usted declaro.
                  <span style="font-weight:bold">La empresa no se hace responsable por articulos no declarados</span>.
                  Es estrictamente prohibido meter dinero en los sobres o paquetes, esto es un delito federal.
                  <span style="font-weight:bold">La compañia no se hace responsable por paquetes no revisados por su familiar en
                    MEXICO.</span><br />
                  GRACIAS POR SU COMPRENSION.
              </td>
            </tr>
          </tbody>
        </table>';
$pdf->writeHTML($html, true, false, true, false, 'center');

$pdf->IncludeJS('print(true);');
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
