<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Corte de Caja</div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Inicio</label>
                                        <input type="date" id="fi" class="form-control round">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Fin</label>
                                        <input type="date" id="ff" class="form-control round">
                                    </div>
                                </div>
                                <?php if($this->session->userdata("perfilid")=="1"){ ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Tienda</label>
                                            <select class="form-control round" id="id_tienda">
                                                <option value='0'>Todas</option>
                                            </select>
                                        </div>
                                    </div>
                                <?php }else { ?>
                                    <input type="hidden" id="id_tienda_user" value="<?php echo $this->session->userdata("id_tienda"); ?>">
                                <?php } ?>
                                <div class="col-md-3">
                                    <button id="export" style="margin-top: 25px;" type="button" class="btn btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar</button>
                                </div>
                            </div>
                            <div class="table-responsive m-t-40">
                                <table id="tabla_corte" class="display nowrap table table-hover table-striped table-bordered"
                                    cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th># Envío</th>
                                            <th>Semana</th>
                                            <th>Envía</th>
                                            <th>Paquete</th>
                                            <th>Libras</th>
                                            <th>Destino</th>
                                            <th>Método</th>
                                            <th>Monto</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <!--<td style="text-align: right;" colspan="5"><b>Total:</b></td>-->
                                            <td colspan="3"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <h5 class="modal-title">Montos totales</h5>
                                <div id="cont_metodos">
                                    <table class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <h5 id="cash" class="modal-title">Efectivo:</h5>
                                                </th>
                                                <th>
                                                    <h5 id="tcash" class="modal-title">$</h5>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <h5 id="tc" class="modal-title">Tarjeta de Crédito / Débito:</h5>
                                                </th>
                                                <th>
                                                    <h5 id="ttarjc" class="modal-title">$</h5>
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
