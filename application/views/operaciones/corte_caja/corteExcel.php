<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=corte_caja".date('Ymd Gis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
  <table border="1" id="tabla" class="display table table-hover table-striped table-bordered" width="100%">
    <thead>
      <!--<tr><td colspan="7">Tienda</td></tr>-->
      <tr>
        <th># Envío</th>
        <th>Envía</th>
        <th>Paquete</th>
        <th>Libras</th>
        <th>Destino</th>
        <th>Método</th>
        <th>Monto</th>
      </tr>
    </thead>
    <tbody>
    	<?php $tot_paqfin=0; $stot_libras=0; $stotal=0; $met=""; $total_elect=0;
        $stotalcash=0; $stotaltc=0; $stotaltd=0;
        foreach ($r as $key) {
          $tot_paqfin=$tot_paqfin+$key->paquetes;
          $stot_libras=$stot_libras+$key->slibras;
          $stotal=$stotal+$key->stotal;
          if($id_tienda!="0"){
            if($key->metodo==1){
              $stotalcash=$stotalcash+$key->stotal;
            }else if($key->metodo==2){
              $stotaltc=$stotaltc+$key->stotal;
            }else if($key->metodo==3){
              $stotaltd=$stotaltd+$key->stotal;
            }
          }

          $met='';
          if($key->metodo=="1")
            $met="Efectivo";
          else if($key->metodo=="2" || $key->metodo=="3")
            $met="T. Crédito/Débito";

        	echo '
            <tr>
              <td >'.$key->id.'</td>
              <td >'.$key->envia.'</td>
              <td >'.$key->paquetes.'</td>
              <td >'.$key->slibras.'</td>
              <td >'.$key->destino.'</td>
              <td >'.$met.'</td>
              <td >'.number_format($key->stotal,2,".",",").'</td>';
        }
        echo '</tr>
            <tfoot>
              <tr>
                <td colspan="2"></td>
                <td>'.$tot_paqfin.'</td>
                <td>'.$stot_libras.'</td>
                <td></td>
                <td></td>
                <td>'.number_format($stotal,2,".",",").'</td>
              </tr>
            </tfoot>';

        ?>
    </tbody>
  </table>
<?php if($id_tienda!="0"){ ?>
  <table class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th>
          <h5 id="cash" class="modal-title">Efectivo:</h5>
        </th>
        <th>
          <h5 id="tcash" class="modal-title">$<?php echo number_format($stotalcash,2,".",","); ?></h5>
        </th>
      </tr>
      <tr>
        <th>
          <h5 id="tc" class="modal-title">Tarjeta de Crédito/Débito:</h5>
        </th>
        <th>
          <h5 id="ttarjc" class="modal-title">$<?php echo number_format($stotaltc+$stotaltd,2,".",","); ?></h5>
        </th>
      </tr>
    </thead>
  </table>
<?php } else { 
  $txt_efec=""; $txt_tcd="";
  $txt_efec='<table class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>';
  $m=$this->ModeloEnvios->getTotalMetodo($params,'metodo=1');
  $conte=0; $contt=0;
  foreach($m as $m1){
    $conte++;
    $stotal=$m1->stotal;
    $txt_efec.='<tr><td>'.$m1->tienda.'</td></tr>
      <tr>
        <td>
          <strong>Efectivo:</strong>
        </td>
        <td>
          $ '.number_format($stotal,2,".",",").'
        </td>
      </tr>'; 
  }
  
  $txt_efec.='</thead>
    </table>';
  echo $txt_efec;

  echo '<table width="100%"><tr><td><br></td></tr></table>';
  $txt_tcd='<table class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>';
  //$getm=$this->ModeloEnvios->getTotalMetodoTienda($params,'metodo=2',array("id_destino"=>$m1->id_destino));
  $getm=$this->ModeloEnvios->getTotalMetodo($params,'metodo=2');
  foreach($getm as $m2){
    $contt++;
    $stotalt2=$m2->stotal;
    $txt_tcd.='<tr><td>'.$m2->tienda.'</td></tr>
      <tr>
        <td>
          <strong>Tarjeta de Crédito/Débito:</strong>
        </td>
        <td>
          $ '.number_format($stotalt2,2,".",",").'
        </td>
      </tr>'; 
  }
  $txt_tcd.='</thead>
    </table>';

  echo $txt_tcd;
?>
  <!--<table class="display table table-hover table-striped table-bordered" cellspacing="0" width="50%">
    <thead>
      <tr>
        <td width="50%"><?php echo $txt_efec; ?></td>
        <td width="50%"><?php echo $txt_tcd; ?></td>
      </tr>
    </thead>
  </table>-->

  <!--<table class="display table table-hover table-striped table-bordered" cellspacing="0" width="50%">
    <thead>
      <?php echo $txt_efec; ?>
    </thead>
  </table>
  <table class="display table table-hover table-striped table-bordered" cellspacing="0" width="50%">
    <thead>
      <?php echo $txt_tcd; ?>
    </thead>
  </table>-->
  
<?php } ?>
