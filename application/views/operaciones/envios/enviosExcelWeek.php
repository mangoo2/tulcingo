<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=week_list".date('Ymd Gis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
  <thead>
      <tr>
      	<th>No. de paquete</th>
        <th>Quién recibe</th>
        <th>Teléfono</th>
        <!-- <th>Remitente</th> -->
        <th>Contenido</th>
        <th>Sucursal destino</th>
        <th>Quién envía</th>
        <!-- <th>Paq</th>
        <th>LB</th>
        <th>Semana</th> -->
      </tr>
  </thead>
  <tbody>
  	<?php $tot_paqfin=0; $stot_libras=0; $id_ori=0;
      foreach ($r as $key) {
        $tot_paqfin=$tot_paqfin+$key->paquetes;
        $stot_libras=$stot_libras+$key->slibras;

        if($key->tipo=="1"){ //eu
          $num_env = $key->letra."".$key->folio;
        }else{
          if(is_numeric($key->letra)==true){
            $num_env = intVal($key->letra)+intVal($key->folio);
            //$num_env = intVal($key->folio);
          }else{
            $num_env = $key->letra."".$key->folio; 
          }
        }

        if($key->id_origen!=$id_ori){
        	echo '
            <thead>
              <tr><td style="text-align:center" colspan="7"><b>'.$key->origen.'</b></td></tr>
            </thead>';
        }
        //<td >'.$key->origen.'</td>
        echo '<tr>
            <td >'.$num_env.'</td>
            <td >'.$key->recibe.'</td>
            <td >'.$key->tel_recibe.'</td>
            <td >'.$key->contenido.'</td>
            <td >'.$key->destino.'</td>
            <td >'.$key->envia.'</td>
            ';

            /*<td >'.$key->paquetes.'</td>
            <td >'.$key->slibras.'</td>
            <td >'.$key->semana.'</td>*/

        $id_ori=$key->id_origen;
      }
      /*echo '</tr>
          <tfoot>
            <tr>
              <td colspan="6" style="text-align:right">Total:</td>
              <td style="text-align:center">'.$tot_paqfin.'</td>
              <td style="text-align:center">'.$stot_libras.'</td>
            </tr>
          </tfoot>';*/

      ?>
  </tbody>
</table>
