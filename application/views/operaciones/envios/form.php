<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Agregar nuevo envío</div>
                    <a href="<?php echo base_url();?>Envios"><button type="button" class="btn btn-secondary mr-1 mb-1">Regresar</button></a>
                </div>
            </div>

            <section id="basic-input">
                <div class="row">

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="mb-0 text-white">Persona que envía</h4>
                            </div>
                            <form id="form_cliente">
                                <div class="form-body">
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Cliente</label>
                                                <select class="form-control" name="id_cliente_envia" id="cliente">
                                                    <?php if(isset($env) && $env->id_cliente_envia>0){ echo "<option value=".$env->id_cliente_envia.">$env->nom_envia $env->apellido_envia</option>"; } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row pt-6">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="nom_envia">Nombre</label>
                                                    <input type="text" id="nom_envia" name="nom_envia" class="form-control round" placeholder="Nombre del cliente" <?php if(isset($env)){ echo "value='$env->nom_envia'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="apellido_envia">Apellido(s)</label>
                                                    <input type="text" id="apellido_envia" name="apellido_envia" class="form-control round" placeholder="Apellido(s) del cliente" <?php if(isset($env)){ echo "value='$env->apellido_envia'"; } ?>>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-6">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="calle_envia">Calle</label>
                                                    <input type="text" id="calle_envia" name="calle_envia" class="form-control round" placeholder="Calle del cliente" <?php if(isset($env)){ echo "value='$env->calle_envia'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="ciudad_envia">Ciudad</label>
                                                    <input type="text" id="ciudad_envia" name="ciudad_envia" class="form-control round" placeholder="Ciudad de recidencia del cliente" <?php if(isset($env)){ echo "value='$env->ciudad_envia'"; } ?>>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-6">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="edo_envia">State</label>
                                                    <input type="text" id="edo_envia" name="edo_envia" class="form-control round" placeholder="State del cliente" <?php if(isset($env)){ echo "value='$env->edo_envia'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="cp_envia">C.P</label>
                                                    <input type="number" id="cp_envia" name="cp_envia" class="form-control round" placeholder="CP de recidencia del cliente" <?php if(isset($env)){ echo "value='$env->cp_envia'"; } ?>>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-6">
                                            <div class="col-md-5">
                                                <fieldset class="form-group">
                                                    <label for="tel_envia">Teléfono</label>
                                                    <input type="text" id="tel_envia" name="tel_envia" class="form-control round" placeholder="Número teléfonico" <?php if(isset($env)){ echo "value='$env->tel_envia'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-3" style="text-align: end; margin-top: 20px;">
                                                <button type="button" class="btn btn-info" id="history"> <i class="fa fa-eye"></i> Historial</button>
                                            </div>
                                            <div class="col-md-4" style="text-align: end; margin-top: 20px;">
                                                <button title="Solo guardar si es su primer envío del cliente" type="button" class="btn btn-info" id="savec1"> <i class="fa fa-save"></i> Guardar Cliente</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="mb-0 text-white">Persona que recibe</h4>
                            </div>
                            <form id="form_cliente_recibe">
                                <div class="form-body">
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Cliente</label>
                                                <select class="form-control" name="id_cliente" id="recibe">
                                                    <?php if(isset($env) && $env->id_cliente>0){ echo "<option value=".$env->id_cliente.">$env->nom_recibe $env->apellido_recibe</option>"; } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row pt-6">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="nom_recibe">Nombre</label>
                                                    <input type="text" id="nom_recibe" name="nom_recibe" class="form-control round" placeholder="Nombre del cliente" <?php if(isset($env)){ echo "value='$env->nom_recibe'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="apellido_recibe">Apellido(s)</label>
                                                    <input type="text" id="apellido_recibe" name="apellido_recibe" class="form-control round" placeholder="Apellido(s) del cliente" <?php if(isset($env)){ echo "value='$env->apellido_recibe'"; } ?>>
                                                </fieldset>
                                            </div>

                                        </div>
                                        <div class="row pt-6">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="calle_recibe">Calle</label>
                                                    <input type="text" id="calle_recibe" name="calle_recibe" class="form-control round" placeholder="Calle del cliente" <?php if(isset($env)){ echo "value='$env->calle_recibe'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="ciudad_recibe">Ciudad</label>
                                                    <input type="text" id="ciudad_recibe" name="ciudad_recibe" class="form-control round" placeholder="Ciudad de recidencia del cliente" <?php if(isset($env)){ echo "value='$env->ciudad_recibe'"; } ?>>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-6">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="edo_recibe">State</label>
                                                    <input type="text" id="edo_recibe" name="edo_recibe" class="form-control round" placeholder="State del cliente" <?php if(isset($env)){ echo "value='$env->edo_recibe'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="cp_recibe">C.P</label>
                                                    <input type="number" id="cp_recibe" name="cp_recibe" class="form-control round" placeholder="CP de recidencia del cliente" <?php if(isset($env)){ echo "value='$env->cp_recibe'"; } ?>>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-6">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="tel_recibe">Teléfono</label>
                                                    <input type="text" id="tel_recibe" name="tel_recibe" class="form-control round" placeholder="Número teléfonico" <?php if(isset($env)){ echo "value='$env->tel_recibe'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6" style="text-align: end; margin-top: 20px;">
                                                <button title="Solo guardar si es primera vez que recibe" type="button" class="btn btn-info" id="savec2"> <i class="fa fa-save"></i> Guardar Cliente</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="mb-0 text-white">Tipo de envío</h4>
                            </div>
                            <form id="form_dest_orig">
                                <?php if(isset($env)){
                                    echo "<input name='id' id='id' value='$env->id' hidden />";
                                } ?>
                                <div class="form-body">
                                    <div class="card-body">
                                        
                                        <div class="row pt-6">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="folio"># de Envío</label>
                                                    <input type="text" id="folio" name="folio" class="form-control round" placeholder="Folio del envío" <?php if(isset($env)){ echo "value='$env->folio'"; } else { echo "value='$ultFol'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="tipo">Tipo de envío</label> 
                                                    <select class="form-control" id="tipo" name="tipo">
                                                        <?php if($this->session->userdata("tipot")==1){ ?>
                                                            <option <?php if(isset($env) && $env->tipo==1 || !isset($env) && $this->session->userdata("tipot")==1) echo "selected"; ?> value="1">EEUU -> MX</option>
                                                        <?php } ?>
                                                        <?php if($this->session->userdata("tipot")==2){ ?>
                                                            <option  <?php if(isset($env) && $env->tipo==2 || !isset($env) && $this->session->userdata("tipot")==2) echo "selected"; ?> value="2">MX -> EEUU</option>
                                                        <?php } ?>   
                                                    </select>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-3" id="cont_tiendas">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="id_origen">Tienda origen</label> <img src="<?php echo base_url(); ?>app-assets/img/flags/us.png">
                                                    <select class="form-control" id="id_origen" name="id_origen">
                                                        <option value=""></option>
                                                        <?php $sel=""; foreach ($tienda as $t) {
                                                            if(isset($env) && $env->id_origen==$t->id){
                                                                $sel="selected";
                                                            }else{
                                                                if($this->session->userdata("id_tienda")==$t->id){
                                                                    $sel="selected";
                                                                }else{
                                                                   $sel=""; 
                                                                }
                                                            }
                                                            //if($t->id==$this->session->userdata("id_tienda")){
                                                                echo "<option ".$sel." value='$t->id'>$t->nombre</option>";
                                                            //}
                                                        } ?>
                                                    </select>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="id_destino">Tienda destino</label> <img src="<?php echo base_url(); ?>public/img/mx.jpg" width="38px" height="20px">
                                                    <select class="form-control" id="id_destino" name="id_destino">
                                                        <option value=""></option>
                                                        <?php foreach ($tiendamx as $t) {
                                                            if(isset($env) && $env->id_destino==$t->id){
                                                                $sel="selected";
                                                            }else{
                                                                $sel="";
                                                            }
                                                            echo "<option ".$sel." value='$t->id'>$t->nombre</option>";
                                                        } ?>
                                                    </select>
                                                </fieldset>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="card" id="row_paq">
                            <div class="card-header bg-info">
                                <h4 class="mb-0 text-white">Datos del envío</h4>
                            </div>
                            <form id="form_paquete">
                                <div class="form-body padre_paq">
                                    <div class="card-body" id="cont_paqs">
                                    <table class="" id="tabla_paqts">
                                        <?php if($env_paq_row>0){ $cont_=0;
                                        foreach($env_paq as $enp) { $cont_++; ?>
                                        <tr>
                                            <td>
                                            <input name='id' id='id' value="<?php echo $enp->id; ?>" hidden />
                                            <div class="row pt-6">
                                                <div class="col-md-2" style="display:none">
                                                    <fieldset class="form-group">
                                                        <label for="num_sub"># Subpaquetes</label>
                                                        <input type="number" id="num_sub" name="num_sub" class="form-control round" placeholder="# de Subpaquetes" value="<?php echo $enp->num_sub; ?>">
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-3">
                                                    <fieldset class="form-group">
                                                        <label for="libras">Libras</label>
                                                        <input type="number" id="libras" name="libras" class="form-control round" placeholder="Libras" value="<?php echo $enp->libras; ?>">
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-3">
                                                    <fieldset class="form-group">
                                                        <label for="precio">Precio</label>
                                                        <input type="number" id="precio" name="precio" class="form-control round precio_e_<?php echo $cont_ ?>" oninput="calculaTot_e(<?php echo $cont_ ?>)" placeholder="Precioasdsad" value="<?php echo $enp->precio; ?>">
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-3">
                                                    <fieldset class="form-group">
                                                        <label for="valor_est">Valor estimado</label>
                                                        <input type="number" id="valor_est" name="valor_est" class="form-control round" placeholder="Valor estimado" value="<?php echo $enp->valor_est; ?>">
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-3">
                                                    <fieldset class="form-group">
                                                        <label for="impuesto">Impuesto</label>
                                                        <input type="number" id="impuesto" name="impuesto" class="form-control round impuesto_e_<?php echo $cont_ ?>" oninput="calculaTot_e(<?php echo $cont_ ?>)" placeholder="Impuesto" value="<?php echo $enp->impuesto; ?>">
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-2" style="display:none">
                                                    <fieldset class="form-group">
                                                        <label for="num_paquete">Paquetes</label>
                                                        <input type="number" id="num_paquete" name="num_paquete" class="form-control round" placeholder="Número de Paquetes" value="<?php echo $enp->num_paquete; ?>">
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row pt-6">
                                                <div class="col-md-10">
                                                    <fieldset class="form-group">
                                                        <label for="contenido">Contenido del paquete</label>
                                                        <textarea id="contenido" name="contenido" class="form-control round" placeholder="Contenido del Paquete" rows="3" value="<?php echo $enp->contenido; ?>"><?php echo $enp->contenido; ?></textarea> 
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-2">
                                                    <fieldset class="form-group">
                                                        <label for="total">Total: $</label>
                                                        <input readonly type="text" id="total" name="total" class="form-control round total_e_<?php echo $cont_ ?>" placeholder="Total del Paquete" value="<?php echo $enp->total; ?>">
                                                    </fieldset>
                                                    <?php if($cont_==1) { ?>
                                                        <!--<fieldset class="form-group">
                                                            <label for="sup_tot"><b>Super Total</b></label>
                                                            <input style="font-weight: bold;" readonly type="text" id="sup_tot" class="form-control round" placeholder="Total" value="0">
                                                        </fieldset>-->
                                                        <button type="button" id="addPaq" class="btn btn-primary mb-3"><i class="ft-plus mr-2"></i>Paquete</button> <?php } ?>
                                                    <?php if($enp === end($env_paq)) { ?>
                                                        <!--<fieldset class="form-group">
                                                            <label for="sup_tot"><b>Super Total</b></label>
                                                            <input style="font-weight: bold;" readonly type="text" id="sup_tot" class="form-control round" placeholder="Total" value="0">
                                                        </fieldset>--> <?php } ?>
                                                </div>
                                            </div>
                                            </td>
                                        </tr>
                                        <?php } 
                                        } else { ?>
                                            <tr>
                                                <td>
                                                    <input name='id' id='id' value="0" hidden />
                                                <div class="row pt-6">
                                                    <div class="col-md-2" style="display:none">
                                                        <fieldset class="form-group">
                                                            <label for="num_sub"># Subpaquetes</label>
                                                            <input type="number" id="num_sub" name="num_sub" class="form-control round" placeholder="# de Subpaquetes" value="1">
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <fieldset class="form-group">
                                                            <label for="libras">Libras</label>
                                                            <input type="number" id="libras" name="libras" class="form-control round" placeholder="Libras" value="">
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <fieldset class="form-group">
                                                            <label for="precio">Precio</label>
                                                            <input type="number"  oninput="calcular_paquete()" id="precio" name="precio" class="form-control round precio_p" placeholder="Precio" value="">
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <fieldset class="form-group">
                                                            <label for="valor_est">Valor estimado</label>
                                                            <input type="number" id="valor_est" name="valor_est" class="form-control round" placeholder="Valor estimado" value="">
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <fieldset class="form-group">
                                                            <label for="impuesto">Impuesto</label>
                                                            <input type="number" oninput="calcular_paquete()" id="impuesto" name="impuesto" class="form-control round impuesto_p" placeholder="Impuesto" value="" >
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-2" style="display:none">
                                                        <fieldset class="form-group">
                                                            <label for="num_paquete">Paquetes</label>
                                                            <input type="number" id="num_paquete" name="num_paquete" class="form-control round" placeholder="Número de Paquetes" value="1">
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row pt-6">
                                                    <div class="col-md-10">
                                                        <fieldset class="form-group">
                                                            <label for="contenido">Contenido del paquete</label>
                                                            <textarea id="contenido" name="contenido" class="form-control round" placeholder="Contenido del Paquete" rows="3" value=""></textarea> 
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <fieldset class="form-group">
                                                            <label for="total">Total: $</label>
                                                            <input readonly type="text" id="total" name="total" class="form-control round total_p" placeholder="Total del paquete" value="">
                                                        </fieldset>
                                                        
                                                        <button type="button" id="addPaq" class="btn btn-primary mb-3"><i class="ft-plus mr-2"></i>Paquete</button>
                                                    </div>
                                                </div>
                                                </td>
                                            </tr>
                                         <?php } ?>
                                        <tbody id="table_servicios">
                                        </tbody>
                                    </table>
                                    <!--<div class="row col-md-12">
                                        <div class="col-md-10"></div>-->
                                        <?php /*
                                        <label class="control-label">Método de pago</label>
                                                    <select class="form-control" id="metodo">
                                                        <option <?php if(isset($env) && $env->metodo=="1") echo "selected"; ?> value="1">Efectivo</option>
                                                        <option <?php if(isset($env) && $env->metodo=="2") echo "selected"; ?> value="2">Tarjeta de Crédito/Débito</option>
                                                        <!--<option <?php if(isset($env) && $env->metodo=="3") echo "selected"; ?> value="3">Tarjeta de Débito</option>-->
                                                    </select>
                                        */ ?>
                                        <div class="row pt-1"></div>
                                        <div class="row pt-1">
                                            <div class="col-md-7"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label">Método de pago</label>
                                                    <h6>Efectivo</h6>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="text-align: end;">
                                                <fieldset class="form-group">
                                                    <label for="efectivo_cantidad"><b>Cantidad: $</b></label>
                                                    <input style="text-align: end; font-weight: bold;" type="number" id="efectivo_cantidad" oninput="validar_cantida_total(1)" class="form-control round" placeholder="Efectivo" value="<?php if(isset($env)) echo $env->efectivo_cantidad ?>">
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-1">
                                            <div class="col-md-7"></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label">Método de pago</label>
                                                    <h6>Tarjeta de Crédito/Débito</h6>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="text-align: end;">
                                                <fieldset class="form-group">
                                                    <label for="tarjeta_cantidad"><b>Cantidad: $</b></label>
                                                    <input style="text-align: end; font-weight: bold;" type="number" id="tarjeta_cantidad" oninput="validar_cantida_total(2)" class="form-control round" placeholder="Tarjeta" value="<?php if(isset($env)) echo $env->tarjeta_cantidad ?>">
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-1">
                                            <div class="col-md-7"></div>
                                            <div class="col-md-3">
                                            </div>
                                            <div class="col-md-2" style="text-align: end;">
                                                <fieldset class="form-group">
                                                    <label for="sup_tot"><b>Super Total: $</b></label>
                                                    <input style="text-align: end; font-weight: bold;" readonly type="text" id="sup_tot" class="form-control round" placeholder="Cobro total de envío" value="0" disabled="">
                                                </fieldset>
                                            </div>
                                        </div>
                                    <!--</div>-->
                            </form>
                        </div>
                        <div class="form-actions">
                            <div class="card-body">
                                <button title="Click para confirmar envío" onclick="save()" type="button" class="btn btn-primary" id="btn_submit"> <i class="fa fa-check"></i> Guardar Envío</button>
                                <button onclick="location.href='<?php echo base_url();?>Envios';" type="button" class="btn btn-dark">Cancel</button>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="modal_envios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Historial de envíos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id_env" value="0">
                <div class="row col-md-12" id="cont_envios">
                    <div class="table-responsive m-t-40">
                        <h5 class="modal-title" id="myModalLabel17">Datos de cliente que envía</h5>
                        <span class="modal-title" id="nomb_envia_mod">Nombre:</span><br>
                        <span class="modal-title" id="tel_envia_mod">Teléfono:</span><br>
                        <span class="modal-title" id="dir_envia_mod">Dirección:</span><br>
                        <table id="tabla_envios" class="display table table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Estatus</th>
                                    <!--<th>Folio</th>-->
                                    <th>Recibe</th>
                                    <th>Direcc. Destino</th>
                                    <th>Tienda Destino</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="body_hist">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>