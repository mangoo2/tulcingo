<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=envios".date('Ymd Gis').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" id="tabla" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
        	<th>#</th>
          <th>Estatus</th>
          <!--<th>Folio</th>-->
          <th>Recibe</th>
          <th>Teléfono</th>
          <th>Envía</th>
          <th>Teléfono</th>
          <th>Origen</th>
          <th>Destino</th>
          <th>Contenido</th>
          <th># Paquetes</th>
          <th>Libras</th>
          <th>Fecha Registro</th>
          <th>Usuario</th>
        </tr>
    </thead>
    <tbody>
    	<?php $tot_paqfin=0; $stot_libras=0; $id_ori=0;
        foreach ($r as $key) {
          $tot_paqfin=$tot_paqfin+$key->tot_paq;
          $stot_libras=$stot_libras+$key->tot_libras;
          if($key->entregado==1)
            $estatus="Entregado $key->fecha_entregado";
          else if($key->entregado==0)
            $estatus="Sin entregar";
          else if($key->entregado==3)
            $estatus="Reportado";

          if($key->tipo=="1"){ //eu
            $num_env = $key->letra."".$key->folio;
          }else{
            if(is_numeric($key->letra)==true){
              $num_env = intVal($key->letra)+intVal($key->folio);
              //$num_env = intVal($key->folio);
            }else{
              $num_env = $key->letra."".$key->folio; 
            }
          }
          if($key->id_origen!=$id_ori){
          echo '
            <thead>
              <tr><td style="text-align:center" colspan="12"><b>'.$key->origen.'</b></td></tr>
            </thead>';
          }
        	echo '
            <tr>
              <!--<td >'.$key->id.'</td>-->
              <td >'.$num_env.'</td>
              <td >'.$estatus.'</td>
              <td >'.$key->recibe.'</td>
              <td >'.$key->tel_recibe.'</td>
              <td >'.$key->envia.'</td>
              <td >'.$key->tel_envia.'</td>
              <td >'.$key->origen.'</td>
              <td >'.$key->destino.'</td>
              <td >'.$key->contenido.'</td>
              <td >'.number_format($key->tot_paq,2).'</td>
              <td >'.number_format($key->tot_libras,2).'</td>
              <td >'.$key->fecha_reg.' <br> Semana:'.$key->semana.'</td>
              <td >'.$key->user.'</td>';
          $id_ori=$key->id_origen;
        }
        echo '</tr>
            <tfoot>
              <tr>
                <td colspan="9" style="text-align:right">Total:</td>
                <td style="text-align:center">'.number_format($tot_paqfin,2).'</td>
                <td style="text-align:center">'.number_format($stot_libras,2).'</td>
              </tr>
            </tfoot>';

        ?>
    </tbody>
</table>
