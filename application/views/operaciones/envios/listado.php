<style type="text/css">
    .tam_tab_env{
        /*font-size: 8pt;
        width: 10px !important;
        height: 35px !important;
        align-items: center !important;
        text-align: center !important;*/
        transform: scale(0.9);
        padding: 0.2rem 0.5rem;
    }
    .btn-trans{
        display: block;
        width: 100%;
        height: calc(1.5em + 0.75rem + 3px);
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #75787d;
        background-color: transparent;
        background-clip: padding-box;
        border: 1px solid #E0E0E0;
        border-radius: 1.5rem;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }
</style>
<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Listado de envíos</div>
                    <a href="<?php echo base_url();?>Envios/alta"><button type="button"  class="btn btn-secondary mr-1 mb-1">Agregar envio</button></a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="cursor-pointer">
                                    <div class="media d-flex align-items-center justify-content-between">
                                        <!---
                                        <div class="media-left">
                                            <div class="media-body">
                                                <h6 class="m-2">Cliente Registrado</h6>
                                            </div>
                                        </div>
                                        <div class="media-right">
                                            <div class="custom-control custom-switch">
                                                <input class="switchery" type="checkbox" checked id="tipo_cli" data-size="sm">
                                                <label for="tipo_cli"></label>
                                            </div>
                                        </div>
                                        --->
                                    </div>
                                </div>
                                <div class="col-md-4" id="cont_cliex" style="display:none">
                                    <div class="form-group">
                                        <label class="control-label">Cliente</label>
                                        <select class="form-control round" id="cliente">                        
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4" id="cont_nocliex">
                                    <div class="form-group">
                                        <label class="control-label">Cliente</label>
                                        <input class="form-control" type="text" placeholder="Buscar un cliente por nombre y/o apellido" id="clienteText">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Estatus</label>
                                        <select class="form-control round" id="entregado">
                                            <option value="2">Todos</option>
                                            <option value="1">Entregado</option>
                                            <option value="0">Sin entregar</option>
                                            <option value="3">Destino incorrecto</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <fieldset class="form-group">
                                        <label for="tipo">Tipo de envío</label> 
                                        <select class="form-control round" id="tipo">
                                            <?php if($this->session->userdata("tipot")==1 && $this->session->userdata("perfilid")!="1" || $this->session->userdata("perfilid")=="1"){ 
                                                $sel="";
                                                if($this->session->userdata("perfilid")=="1" && $this->session->userdata("tipot")==1) $sel="selected";
                                            ?>
                                                <option <?php echo $sel; ?> value="1">EEUU -> MX</option>
                                            <?php } ?>
                                            <?php if($this->session->userdata("tipot")==2 && $this->session->userdata("perfilid")!="1" || $this->session->userdata("perfilid")=="1" || $this->session->userdata("tipot")==1){ 
                                                $sel="";
                                                if($this->session->userdata("perfilid")=="1" && $this->session->userdata("tipot")==2) $sel="selected";
                                            ?>   
                                                <option <?php echo $sel; ?> value="2">MX -> EEUU</option>
                                            <?php } ?>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Tienda</label>
                                        <select class="form-control round" id="id_tienda">
                                            <option value='0'>Todas</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1" id="cont_btn_list" style="display:none">
                                    <div class="form-group" style="margin-top:25px">
                                        <button title='Ver Lista' class='btn btn-info' type='button' id="btn-list"><i class='fa fa-eye' aria-hidden='true'></i></button>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"># de Paquete</label>
                                        <input class="form-control" type="text" placeholder="Buscar por # de paquete" id="num_paq">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Inicio</label>
                                        <input type="date" id="fi" class="form-control round">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Fin</label>
                                        <input type="date" id="ff" class="form-control round">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Semana</label>
                                        <select class="form-control round" id="semana">
                                            <option value="0">Todas</option>
                                            <?php $sel=""; $anio=date("Y"); for($i=1; $i<=52; $i++){ 
                                                if($semana==$i) $sel="selected"; else $sel="";
                                                echo '<option '.$sel.' value="'.$i.'">'.$i.' - '.$anio.'</option>';
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button id="export" style="margin-top: 25px;" type="button" class="btn btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar</button>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Número telefonico</label>
                                        <input class="form-control" type="text" placeholder="Buscar por número de teléfono" id="search_tel">
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive m-t-40">
                                <table id="tabla_envios" class="display table table-striped table-bordered"
                                    cellspacing="0" style="font-size:10.0pt; font-weight: bold; vertical-align:middle" width="100%">
                                    <thead>
                                        <tr style="text-align:center">
                                            <th width="5%">#</th>
                                            <th width="18%">Estatus</th>
                                            <!--<th>Folio</th>-->
                                            <!--<th width="15%">Fecha</th>
                                            <th width="10%">Recibe</th>
                                            <th width="10%">Envía</th>
                                            <th width="10%">Origen</th>
                                            <th width="10%">Destino</th>-->
                                            <th width="60%">Datos de envío</th>
                                            <!--<th>Contenido</th>-->
                                            <!--<th width="10%">Usuario</th>-->
                                            <th width="17%">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="modal_conts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Contenido de paquetes</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id_env" value="0">
                <div class="col-md-12" id="conts_paqs">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade text-left" id="modal_coments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Comentarios de envio</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id_env_com" value="0">
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="comentarios">Comentarios</label>
                        <textarea id="comentarios" class="form-control round" placeholder="Ingrese comentarios al envío" rows="4"></textarea> 
                    </fieldset>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="save_comts"> <i class="fa fa-check"></i> Save</button>
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade text-left" id="modal_week_shop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Reporte semanal por tienda</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-3">
                    <button id="export_weeks" style="margin-top: 25px;" type="button" class="btn btn-success"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar</button>
                </div>
                <div class="table-responsive m-t-40">
                    <br>
                    <table id="tabla_list" class="display table table-hover table-striped table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Remitente</th>
                                <th>Destinatario</th>
                                <th>Teléfono</th>
                                <th>Paq</th>
                                <th>LB</th>
                                <th>Descripción</th>
                                <th>Destino</th>
                                <th>Origen</th>
                                <th>Semana</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <td style="text-align: right;" colspan="6"><b>Total:</b></td>
                                <td></td>
                                <td></td>
                                <td colspan="2"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

