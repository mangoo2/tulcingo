<div class="page-wrapper">  
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Formulario de Personal Operador</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url();?>Inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Personal Operador</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="mb-0 text-white">Datos de Personal Operador</h4>
                    </div>
                    <form id="form_pers">
                        <div class="card-body">
                            <!--<h4 class="card-title">Información General</h4>-->
                        </div>
                        <hr>
                        <?php if(isset($per)){
                            echo "<input name='id' id='id' value='$per->id' hidden />";
                        } ?>
                        <div class="form-body">
                            <div class="card-body">
                                <div class="row pt-3">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Nombre</label>
                                            <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre del personal operador" <?php if(isset($per)){ echo "value='$per->nombre'"; } ?>>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Apellido Paterno</label>
                                            <input type="text" id="app" name="app" class="form-control" placeholder="Apellido paterno del personal operador" <?php if(isset($per)){ echo "value='$per->app'"; } ?>>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Apellido Materno</label>
                                            <input type="text" id="apm" name="apm" class="form-control" placeholder="Apellido materno del personal operador" <?php if(isset($per)){ echo "value='$per->apm'"; } ?>>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Número de Licencia</label>
                                            <input type="text" id="num_licencia" name="num_licencia" class="form-control" placeholder="Número de licencia del personal operador" <?php if(isset($per)){ echo "value='$per->num_licencia'"; } ?>>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Tipo de Licencia</label>
                                            <select class="form-control custom-select" id="tipo_licencia" name="tipo_licencia">?>
                                                <option <?php if(isset($per) && $per->tipo_licencia==1) { echo "selected"; } ?> value="1">Particular</option>
                                                <option <?php if(isset($per) && $per->tipo_licencia==2) { echo "selected"; } ?> value="2">Mercantil</option>
                                                <option <?php if(isset($per) && $per->tipo_licencia==3) { echo "selected"; } ?> value="3">Moto</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Fecha de Ingreso</label>
                                            <input type="date" id="fecha_ingreso" name="fecha_ingreso" class="form-control" <?php if(isset($per)){ echo "value='$per->fecha_ingreso'"; } ?>>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                            </div>
                            <hr>

                            <div class="form-actions">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-success" id="btn_submit"> <i class="fa fa-check"></i> Save</button>
                                    <button type="button" class="btn btn-dark">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
