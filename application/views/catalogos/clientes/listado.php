<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Listado de clientes</div>
                    <a href="<?php echo base_url();?>Clientes/alta"><button type="button"  class="btn btn-secondary mr-1 mb-1">Agregar cliente</button></a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label for="lugar">Lugar
                                        <select class="form-control round" id="lugar">
                                            <option value="1">EEUU</option>
                                            <option value="2">México</option>
                                        </select>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="table-responsive m-t-40">
                                <table id="tabla_clientes" class="display table table-hover table-striped table-bordered"
                                    cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Dirección</th>
                                            <th>Tienda</th>
                                            <th>Teléfonos</th>
                                            <th>Ultima actualización</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                
            </div>
    </div>
</div>
