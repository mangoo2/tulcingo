<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Agregar nuevo cliente</div>
                    <a href="<?php echo base_url();?>Clientes"><button type="button" class="btn btn-secondary mr-1 mb-1">Regresar</button></a>
                </div>
            </div>

            <section id="basic-input">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="mb-0 text-white">Datos del cliente</h4>
                            </div>
                            <form id="form_cliente">

                                <?php if(isset($cliente)){
                                    echo "<input name='id' id='id' value='".$cliente->id."' hidden />";
                                    echo "<input id='idaux' value='".$cliente->id."' hidden>";
                                }else{
                                    echo "<input id='idaux' value='0' hidden>";   
                                } ?>
                                <div class="form-body">
                                    <div class="card-body">
                                        <div class="row pt-3">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="nombre">Nombre</label>
                                                    <input type="text" id="nombre" name="nombre" class="form-control round" placeholder="Nombre del cliente" <?php if(isset($cliente)){ echo "value='$cliente->nombre'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="apellidos">Apellido(s)</label>
                                                    <input type="text" id="apellidos" name="apellidos" class="form-control round" placeholder="Apellido(s) del cliente" <?php if(isset($cliente)){ echo "value='$cliente->apellidos'"; } ?>>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-3">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="tipo">Tipo de cliente</label> 
                                                    <select class="form-control round" id="tipo" name="tipo">
                                                        <option <?php if(isset($cliente) && $cliente->tipo=="1") echo "selected"; ?> value="1">EEUU</option>
                                                        <option <?php if(isset($cliente) && $cliente->tipo=="2") echo "selected"; ?> value="2">MX</option>
                                                    </select>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-3" id="cont_select">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="tienda">Tienda que recibe</label> <img src="<?php echo base_url(); ?>app-assets/img/flags/us.png">
                                                    <select class="form-control" id="tienda" name="tienda">
                                                        <option value=""></option>
                                                        <?php foreach ($tienda as $t) {
                                                            if(isset($cliente) && $cliente->tienda==$t->id){
                                                                $sel="selected";
                                                            }else{
                                                                $sel="";
                                                            }
                                                            echo "<option ".$sel." value='".$t->id."'>$t->nombre</option>";
                                                        } ?>
                                                    </select>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="tiendamx">Tienda que envía</label> <img src="<?php echo base_url(); ?>public/img/mx.jpg" width="38px" height="20px">
                                                    <select class="form-control" id="tiendamx" name="tiendamx">
                                                        <option value=""></option>
                                                        <?php foreach ($tiendamx as $t) {
                                                            if(isset($cliente) && $cliente->tiendamx==$t->id){
                                                                $sel="selected";
                                                            }else{
                                                                $sel="";
                                                            }
                                                            echo "<option ".$sel." value='".$t->id."'>$t->nombre</option>";
                                                        } ?>
                                                    </select>
                                                </fieldset>
                                            </div>
                                            
                                        </div>
                                        <div class="row pt-3">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="calle">Calle</label>
                                                    <input type="text" id="calle" name="calle" class="form-control round" placeholder="Calle del cliente" <?php if(isset($cliente)){ echo "value='$cliente->calle'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="ciudad">Ciudad</label>
                                                    <input type="text" id="ciudad" name="ciudad" class="form-control round" placeholder="Ciudad de recidencia del cliente" <?php if(isset($cliente)){ echo "value='$cliente->ciudad'"; } ?>>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-3">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="edo">State</label>
                                                    <input type="text" id="edo" name="edo" class="form-control round" placeholder="State del cliente" <?php if(isset($cliente)){ echo "value='$cliente->edo'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="cp">C.P</label>
                                                    <input type="number" id="cp" name="cp" class="form-control round" placeholder="CP de recidencia del cliente" <?php if(isset($cliente)){ echo "value='$cliente->cp'"; } ?>>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-3">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="telefono">Teléfono</label>
                                                    <input type="text" id="telefono" name="telefono" class="form-control round" placeholder="Número teléfonico" <?php if(isset($cliente)){ echo "value='$cliente->telefono'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="telefono2">Teléfono 2</label>
                                                    <input type="text" id="telefono2" name="telefono2" class="form-control round" placeholder="Número teléfonico 2" <?php if(isset($cliente)){ echo "value='$cliente->telefono2'"; } ?>>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row pt-3">
                                            <div class="col-md-12">
                                                <fieldset class="form-group">
                                                    <label for="comentarios">Comentarios</label>
                                                    <textarea id="comentarios" name="comentarios" class="form-control round" placeholder="Comentarios u observaciones" rows="3" <?php if(isset($cliente)){ echo "value='$cliente->comentarios'"; } ?>><?php if(isset($cliente)){ echo "$cliente->comentarios"; } ?></textarea> 
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-primary" id="btn_submit"> <i class="fa fa-check"></i> Save</button>
                                            <button onclick="location.href='<?php echo base_url();?>Clientes';" type="button" class="btn btn-dark">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
    </div>
</div>
