<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Listado de usuarios</div>
                    <a href="<?php echo base_url();?>Usuarios/alta"><button type="button"  class="btn btn-secondary mr-1 mb-1">Agregar usuario</button></a>
                </div>
            </div>
            <!-- Zero configuration table -->
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Usuarios activos</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <fieldset class="form-group">
                                                <label for="lugar">Lugar
                                                <select class="form-control round" id="lugar">
                                                    <option value="1">EEUU</option>
                                                    <option value="2">México</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="tabla_usus" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Nombre</th>
                                                    <th>Usuario</th>
                                                    <th>Tienda</th>
                                                    <!--<th>Lugar</th>-->
                                                    <th>Acciones</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->

        </div>
    </div>
    <!-- END : End Main Content-->
