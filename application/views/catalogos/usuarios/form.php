<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Agregar nuevo usuario</div>
                    <a href="<?php echo base_url();?>Usuarios"><button type="button" class="btn btn-secondary mr-1 mb-1">Regresar</button></a>
                </div>
            </div>

            <section id="basic-input">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="mb-0 text-white">Datos del Usuario</h4>
                            </div>
                            <form id="form_usus">
                                <?php if(isset($usu)){
                                    echo "<input name='id' id='id' value='$usu->id' hidden />";
                                } ?>
                                <div class="form-body">
                                    <div class="card-body">
                                        <div class="row pt-6">
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="nombre">Nombre</label>
                                                    <input type="text" id="nombre" name="nombre" class="form-control round" placeholder="Nombre del usuario" <?php if(isset($usu)){ echo "value='$usu->nombre'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset class="form-group">
                                                    <label for="apellidos">Apellido(s)</label>
                                                    <input type="text" id="apellidos" name="apellidos" class="form-control round" placeholder="Apellido(s) del usuario" <?php if(isset($usu)){ echo "value='$usu->apellidos'"; } ?>>
                                                </fieldset>
                                            </div>

                                        </div>
                                        <div class="row pt-6">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Tipo de Perfil</label>
                                                    <select class="form-control round" id="id_perfil" name="id_perfil">
                                                        <?php $selected='';
                                                        foreach ($perf as $p) {
                                                            if (isset($usu)) {
                                                                if ($p->id==$usu->id_perfil) {
                                                                    $selected='selected';
                                                                }else{
                                                                    $selected='';  
                                                                }
                                                            }
                                                            echo "<option value='$p->id' $selected>$p->perfil</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Tienda</label>
                                                    <select class="form-control round" id="id_tienda" name="id_tienda">
                                                        <?php $selected='';
                                                        foreach ($tienda as $t) {
                                                            if (isset($usu)) {
                                                                if ($t->id==$usu->id_tienda) {
                                                                    $selected='selected';
                                                                }else{
                                                                    $selected='';  
                                                                }
                                                            }
                                                            echo "<option value='$t->id' $selected>$t->nombre</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <h5>Datos de Acceso</h5>
                                        <hr>
                                        <div class="row">
                                            <div class="col-6">
                                                <fieldset class="form-group">
                                                  <label >Nombre de Usuario</label>
                                                  <input type="text" id="usuario" name="usuario" class="form-control round" placeholder="Usuario" <?php if(isset($usu)){ echo "value='$usu->usuario'"; } ?>>
                                                </fieldset>
                                                <fieldset class="form-group">
                                                  <label>Password</label>
                                                  <input type="password" id="contrasenia" name="contrasenia" class="form-control round" placeholder="Password" <?php if(isset($usu)){ echo "value='XXXXXX'"; } ?>>
                                                </fieldset>
                                                <fieldset class="form-group">
                                                  <label>Confirme Password</label>
                                                  <input type="password" name="contrasenia2" class="form-control round" placeholder="Password" <?php if(isset($usu)){ echo "value='XXXXXX'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-6">
                                                <?php 
                                                    $temp=array();
                                                    if(isset($permisos)){
                                                      foreach ($permisos as $p) {
                                                        array_push($temp, $p->MenusubId);
                                                      }
                                                    }
                                                ?>
                                                <!--<div class="col-3">
                                                    <label >Permisos Disponibles:</label>
                                                    <?php
                                                    /*
                                                    $temp=array();
                                                    if(isset($permisos)){
                                                      foreach ($permisos as $p) {
                                                        array_push($temp, $p->MenusubId);
                                                      }
                                                    }
                                                    */
                                                    ?>
                                                    <br>
                                                </div><hr>-->
                                                <div class="col-md-6 col-12">
                                                    <h6>Permisos Otorgados</h6>
                                                    <!--<p>Permisos asignados al usuario</p>-->
                                                    <fieldset class="form-group">
                                                        <select id="optgroup" name="permisos[]" class="form-control" multiple style="height: 150px;">
                                                            <!--<option value="3" <?php // if(in_array(3, $temp)){echo "selected";} ?>>Categorías</option>-->
                                                          <option value="1" <?php if(in_array(1, $temp)){echo "selected";} ?>>Clientes</option>
                                                          <option value="4" <?php if(in_array(4, $temp)){echo "selected";} ?>>Tiendas</option>
                                                          <option value="2" <?php if(in_array(2, $temp)){echo "selected";} ?>>Usuarios</option>
                                                          <option value="5" <?php if(in_array(5, $temp)){echo "selected";} ?>>Envíos</option>
                                                          <option value="6" <?php if(in_array(6, $temp)){echo "selected";} ?>>Corte de Caja</option>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="card-body">
                                                <button type="submit" class="btn btn-primary" id="btn_submit"> <i class="fa fa-check"></i> Save</button>
                                                <button onclick="location.href='<?php echo base_url();?>Usuarios';" type="button" class="btn btn-dark">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
    </div>
</div>
