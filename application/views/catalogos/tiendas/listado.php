<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Listado de tiendas</div>
                    <a href="<?php echo base_url();?>Tiendas/alta"><button type="button"  class="btn btn-secondary mr-1 mb-1">Agregar tienda</button></a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <fieldset class="form-group">
                                        <label for="lugar">Lugar de tienda
                                        <select class="form-control round" id="lugar">
                                            <option value="1">EEUU</option>
                                            <option value="2">México</option>
                                        </select>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="table-responsive m-t-40">
                                <table id="tabla_tiendas" class="display nowrap table table-hover table-striped table-bordered"
                                    cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Teléfono</th>
                                            <th>Lugar</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                
            </div>
    </div>
</div>
