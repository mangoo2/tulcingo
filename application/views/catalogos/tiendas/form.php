<div class="main-panel">
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="content-header">Agregar nueva tienda</div>
                    <a href="<?php echo base_url();?>Tiendas"><button type="button" class="btn btn-secondary mr-1 mb-1">Regresar</button></a>
                </div>
            </div>

            <section id="basic-input">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="mb-0 text-white">Datos de la tienda</h4>
                            </div>
                            <form id="form_tienda">
                                <?php if(isset($tienda)){
                                    echo "<input name='id' id='id' value='$tienda->id' hidden />";
                                } ?>
                                <div class="form-body">
                                    <div class="card-body">
                                        <div class="row pt-3">
                                            <div class="col-md-3">
                                                <fieldset class="form-group">
                                                    <label for="nombre">Nombre</label>
                                                    <input type="text" id="nombre" name="nombre" class="form-control round" placeholder="Nombre de la tienda" <?php if(isset($tienda)){ echo "value='$tienda->nombre'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-2">
                                                <fieldset class="form-group">
                                                    <label for="lugar">Tipo - Lugar</label>
                                                    <select class="form-control" id="lugar" name="lugar">
                                                        <option <?php if(isset($tienda) && $tienda->lugar==1){ echo"selected"; } ?> value="1">EEUU</option>
                                                        <option <?php if(isset($tienda) && $tienda->lugar==2){ echo"selected"; } ?> value="2">MX</option>
                                                    </select>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                                <fieldset class="form-group">
                                                    <label for="telefono">Teléfono</label>
                                                    <input type="text" id="telefono" name="telefono" class="form-control round" placeholder="Teléfono de la tienda" <?php if(isset($tienda)){ echo "value='$tienda->telefono'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-2">
                                                <fieldset class="form-group">
                                                    <label for="telefono">Letra</label>
                                                    <input type="text" id="letra" name="letra" class="form-control round" placeholder="Letra de la tienda" <?php if(isset($tienda)){ echo "value='$tienda->letra'"; } ?>>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="media-left">
                                                    <label for="caja">Tipo Caja</label>
                                                    <div class="custom-control">
                                                        <input class="switchery" type="checkbox" id="caja" data-size="sm" <?php if(isset($tienda) && $tienda->caja=="1"){ echo "checked"; } ?>>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row pt-6">
                                            <div class="col-md-12">
                                                <fieldset class="form-group">
                                                    <label for="direccion">Dirección</label>
                                                    <textarea id="direccion" name="direccion" class="form-control round" placeholder="Dirección y/o descripción de la ubicación de la tienda" rows="4" value="<?php if(isset($tienda)) echo $tienda->direccion; ?>"><?php if(isset($tienda)) echo $tienda->direccion; ?></textarea> 
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-primary" id="btn_submit"> <i class="fa fa-check"></i> Save</button>
                                            <button onclick="location.href='<?php echo base_url();?>Tiendas';" type="button" class="btn btn-dark">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
    </div>
</div>
