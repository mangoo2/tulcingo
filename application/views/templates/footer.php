 <!-- BEGIN : Footer-->
            <footer class="footer undefined undefined">
                <p class="clearfix text-muted m-0"><span>Copyright &copy; <?php echo date("Y") ?> &nbsp;</span><a href="https://mangoo.mx" id="pixinventLink" target="_blank">MANGOO SOFTWARE</a><span class="d-none d-sm-inline-block">, All rights reserved.</span></p>
            </footer>
            <!-- End : Footer-->
            <!-- Scroll to top button -->
            <button class="btn btn-primary scroll-top" type="button"><i class="ft-arrow-up"></i></button>

        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/vendors.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/switchery.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/toastr.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/sweetalert2.all.min.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN APEX JS-->
    <script src="<?php echo base_url(); ?>app-assets/js/core/app-menu.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/core/app.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/notification-sidebar.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/customizer.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/scroll-top.js"></script>
    <!-- END APEX JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?php echo base_url(); ?>app-assets/js/data-tables/dt-basic-initialization.js"></script>
    <script src="<?php echo base_url(); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>/public/select2/select2.full.min.js" type="text/javascript"></script>

    <!-- END PAGE LEVEL JS-->
    <!-- BEGIN: Custom CSS-->
    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>

    <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.12.1/b-2.2.3/b-html5-2.2.3/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.12.1/b-2.2.3/b-html5-2.2.3/datatables.min.js"></script>-->
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>


    <!-- END: Custom CSS-->
</body>
<!-- END : Body-->

</html>