<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Envios extends CI_Controller {

	public function __construct() {
    parent::__construct();
    $this->load->model('ModeloEnvios');
    $this->load->model('ModeloClientes');
    $this->load->model('General_model');
    if (!$this->session->userdata('logeado')){
      redirect('/Login');
  	}else{
      $this->perfilid=$this->session->userdata('perfilid');
      $this->id_usuario=$this->session->userdata('usuarioid');
      $this->id_tienda=$this->session->userdata('id_tienda');
      $this->tipot=$this->session->userdata('tipot');
      //ira el permiso del modulo
      $this->load->model('Login_model');
      $permiso=$this->Login_model->getviewpermiso($this->perfilid,5);// 5 es el id del submenu
      if ($permiso==0) {
      	redirect('/Sistema');
      }
  	}
  	date_default_timezone_set('America/Mexico_City');
  	$this->fecha=date("Y-m-d H:i:s");
  	$this->fecha2=date("d-m-Y");
   }

	public function index(){
		$data2["activo"]=1;
		$data2["mn"]=2;
		$fecha = strtotime($this->fecha2);
		$semana = date('W', $fecha);	// Obtenemos el número de semana con el parametro W y la fecha en Unix
		//log_message('error', 'semana: '.$semana);
		$data["semana"]=$semana;
		$this->load->view('templates/header');
	    $this->load->view('templates/navbar',$data2);
	    //$this->load->view('notification');
		$this->load->view('operaciones/envios/listado',$data);
		$this->load->view('templates/footer');
    	$this->load->view('operaciones/envios/envioslistajs');
	}

  	public function getTiendasAll(){
	  	$tipo=$this->input->post("tipo");
	  	$tienda=$this->General_model->getselectwhereall("tienda",array("lugar"=>1,"estatus"=>1));//EU
			$tiendamx=$this->General_model->getselectwhereall("tienda",array("lugar"=>2,"estatus"=>1));//MX
			$html="";
			$fecha = strtotime($this->fecha2);
			$semana = date('W', $fecha);
			$semana=$this->input->post("semana");
			$anio=date("Y");
			$semana=$this->input->post("semana");
			$f1=$this->input->post("f1");
            $f2=$this->input->post("f2");
			

			if($this->session->userdata("perfilid")==1){
				$html.="<option value='0'>Todas</option>";
			}
			if($tipo==1){ //eu a mx
				$html.='<option value="0">Seleccionar opción</option>';
		      foreach ($tienda as $t) {
		      	$getnum=$this->ModeloEnvios->getTotListEnvios($t->id,$semana,$anio,$f1,$f2);
		      	$sel="";
		      	if($this->session->userdata("perfilid")!=1 && $t->id==$this->session->userdata("id_tienda")){
		        	$html.='<option value="'.$t->id.'">'.$t->nombre.'</option>';
		        }else if($this->session->userdata("perfilid")==1){
		        	if($t->id==$this->session->userdata("id_tienda")){
		        		$sel="selected";
		        	}
		        	$html.='<option '.$sel.' value="'.$t->id.'">'.$t->nombre.' ('.$getnum->tot_env.')</option>';
		        	//$html.='<option '.$sel.' value="'.$t->id.'">'.$t->nombre.'</option>';
		        }
		      }
			}else{ //mx a eu
				$html.='<option value="0">Seleccionar opción</option>';
		      foreach ($tiendamx as $t){
		      	$getnum=$this->ModeloEnvios->getTotListEnvios($t->id,$semana,$anio,$f1,$f2);
		      	$sel="";
		      	if($this->session->userdata("perfilid")!=1 && $t->id==$this->session->userdata("id_tienda")){// || $this->session->userdata("tipot")==1
		        	$html.='<option value="'.$t->id.'">'.$t->nombre.'</option>';
		        }else if($this->session->userdata("perfilid")==1){
		        	if($t->id==$this->session->userdata("id_tienda")){
		        		$sel="selected";
		        	}
		        	$html.='<option '.$sel.' value="'.$t->id.'">'.$t->nombre.' ('.$getnum->tot_env.')</option>';
		        	//$html.='<option '.$sel.' value="'.$t->id.'">'.$t->nombre.'</option>';
		        }
		      }
			}
			echo $html;
  	}

	public function alta($id=0){
		$data2["activo"]=1;
		$data2["mn"]=2;
		$data['env_paq_row']=0;
		$ultFol=0;
		$fecha = new DateTime(strtotime("d/m/Y"));
		$semana = $fecha->format('W');
		$dia = $fecha->format('N');
		//log_message('error', 'semana: '.$semana);
		//log_message('error', 'dia: '.$dia);

		if($id>0){
			$data['env'] = $this->General_model->get_record("id",$id,"envios");
			$data['env_paq'] = $this->General_model->getselectwhereall("envio_paquete",array("id_envio"=>$id));
			foreach($data['env_paq'] as $t){
				$data['env_paq_row']++;
			}
		}
		else{
			$fol_ult=0;
			$data=[];
			$data['env_paq_row']=0;
			$ultFol=$this->General_model->getUltimoFolio($this->tipot,$semana,$this->id_tienda);
			if(isset($ultFol->folio)){ // si existen registros de envios de esta semana
				//folios para sucursales de mexico
				if($this->tipot==2){
					if($dia>5){ //jueves o mas, apartir del viernes se reincia a 1 el folio (consecutivo)
						$data['ultFol'] = $fol_ult+1;
					}else{ //aun pertenece a la semana activa
						$data['ultFol'] = intval($ultFol->folio)+1;
					}
				}
				//folios para sucursales de EEUU
				if($this->tipot==1){
					$ult_date=date("Y-m-t", strtotime($this->fecha2));
					/*$date = strtotime($this->fecha2);
					$lastdate = strtotime(date("Y-m-t", $date ));   
					$day = date("d", $lastdate);
					log_message('error', 'day: '.$day);*/
					/*log_message('error', 'ult_date: '.$ult_date);
					log_message('error', 'fecha: '.date("Y-m-d"));*/
					if($this->fecha2 == $ult_date){ //fin de mes
					//if($day){ //fin de mes
						$data['ultFol'] = $fol_ult+1;
						//log_message('error', 'ultima fecha del mes: '.$ult_date);
					}else{ //aun pertenece a la semana activa
						$data['ultFol'] = intval($ultFol->folio)+1;
						//log_message('error', 'aun dentro de mes: '.$ult_date);
					}
				}
			}else{
				$data['ultFol'] = $ultFol+1;
			}
		}
		
		$data["tienda"]=$this->General_model->getselectwhereall("tienda",array("lugar"=>1,"estatus"=>1));//EU
		$data["tiendamx"]=$this->General_model->getselectwhereall("tienda",array("lugar"=>2,"estatus"=>1));//MX
		$this->load->view('templates/header');
	    $this->load->view('templates/navbar',$data2);
	    $this->load->view('notification');
		$this->load->view('operaciones/envios/form',$data);
		$this->load->view('templates/footer');
		$this->load->view('operaciones/envios/enviosaddjs');
	}

	public function getEnvios() {
	    $params = $this->input->post();
	    if($params["num_paq"]=="") $params["num_paq"]="SNAPLI";
	    if($params["sea_tel"]=="") $params["sea_tel"]="SNAPLI";
	    $clt = $this->ModeloEnvios->getEnvios($params); //falta comparar por semana
	    $totalRecords=$this->ModeloEnvios->total_envios($params); 
	    $json_data = array(
	      "draw"            => intval( $params['draw'] ),   
	      "recordsTotal"    => intval($totalRecords),  
	      "recordsFiltered" => intval($totalRecords),
	      "data"            => $clt->result(),
	      "query"           =>$this->db->last_query()   
	    );
	    echo json_encode($json_data);
	} 

	public function editaDireccion() {
		$id = $this->input->post("id");
		$element = $this->input->post("element");
		$field = $this->input->post("field");
		$field2 = $this->input->post("field2");
		$type = $this->input->post("type");
		$id_envio=$this->input->post("id_envio");
		$id_cliente = $this->input->post("id_cliente");

		if($type == "id_cliente"){
			$dataRecibe = $this->ModeloEnvios->getDataClienteEnvio("nom_recibe,apellido_recibe,tel_recibe",$id_cliente,$type);

			if($id_cliente > 0){
				$id_query = $this->General_model->edit_record("id",$id_cliente,array('nombre' => $dataRecibe->nom_recibe, 'apellidos' => $dataRecibe->apellido_recibe,'telefono' => $dataRecibe->tel_recibe, $field2 => $element),"clientes");
				$this->ModeloEnvios->editaDireccion($id_envio,$type,$element,$field,$id_cliente);
				echo $id_cliente . " Existe";
			}else{
				$id_query = $this->General_model->add_record("clientes",array('nombre' => $dataRecibe->nom_recibe, 'apellidos' => $dataRecibe->apellido_recibe,'telefono' => $dataRecibe->tel_recibe, $field2 => $element));
				$this->ModeloEnvios->editaDireccion($id_envio,$type,$element,$field,$id_query);
				echo $id_cliente." ".$id_query . "No existe";
			}

		}else if($type == "id_cliente_envia"){
			$dataEnvia = $this->ModeloEnvios->getDataClienteEnvio("nom_envia,apellido_envia,tel_envia",$id_cliente,$type);

			if($id_cliente > 0){
				$id_query = $this->General_model->edit_record("id",$id_cliente,array('nombre' => $dataEnvia->nom_envia, 'apellidos' => $dataEnvia->apellido_envia,'telefono' => $dataEnvia->tel_envia, $field2 => $element),"clientes");
				$this->ModeloEnvios->editaDireccion($id_envio,$type,$element,$field,$id_cliente);
				echo $id_cliente . " Existe";
			}else{
				$id_query = $this->General_model->add_record("clientes",array('nombre' => $dataEnvia->nom_envia, 'apellidos' => $dataEnvia->apellido_envia,'telefono' => $dataEnvia->tel_envia, $field2 => $element));
				$this->ModeloEnvios->editaDireccion($id_envio,$type,$element,$field,$id_query);
				echo $id_cliente." ".$id_query . "No existe";
			}
		}
	}

  	public function getTiendaTipo(){ //solo pintar la tienda origen - tienda a la que pertenece el usuario
  	$tipo=$this->input->post("tipo");
  	$id=$this->input->post("id");
  	$id_org=0;
  	$id_dest=0;
  	if($id>0){
			$env = $this->General_model->get_record("id",$id,"envios");
			$id_org=$env->id_origen;
  		$id_dest=$env->id_destino;
		}
		//log_message('error', 'id_org: '.$id_org);
		//log_message('error', 'id_dest: '.$id_dest);
  		$tienda=$this->General_model->getselectwhereall("tienda",array("lugar"=>1,"estatus"=>1));//EU
		$tiendamx=$this->General_model->getselectwhereall("tienda",array("lugar"=>2,"estatus"=>1));//MX
		$htlm="";
		if($tipo==1){ //eu a mx
			$html='<div class="col-md-6">
        	<fieldset class="form-group">
            <label for="id_origen">Tienda origen</label> <img src="'.base_url().'app-assets/img/flags/us.png">
            <select class="form-control" id="id_origen" name="id_origen">
                <!--<option value=""></option>-->';
                foreach ($tienda as $t) {
                	/*if($id_org==$t->id){
                    $sel="selected";
                  }else{
                    $sel="";
                  }*/
                  if($t->id==$this->session->userdata("id_tienda")){
                  	$html.='<option value="'.$t->id.'">'.$t->nombre.'</option>';
                  }
                }
            $html.='</select>
	        </fieldset>
	    	</div>';

	    	$html.='<div class="col-md-6">
	        <fieldset class="form-group">
	            <label for="id_destino">Tienda destino</label> <img src="'.base_url().'public/img/mx.jpg" width="38px" height="20px">
	            <select class="form-control" id="id_destino" name="id_destino">
	                <option value=""></option>';
	                foreach ($tiendamx as $t) {
	                	if($id_dest==$t->id){
	                    $sel="selected";
	                  }else{
	                    $sel="";
	                  }
	                  $html.='<option '.$sel.' value="'.$t->id.'">'.$t->nombre.'</option>';
	                }
	            $html.='</select>
	        </fieldset>
	    	</div>';
		}else{ //mx a eu
			$tienda=$this->General_model->getselectwhereall("tienda",array("lugar"=>1,"caja"=>0,"estatus"=>1));//EU - sin las tipo caja
			$html='<div class="col-md-6">
        	<fieldset class="form-group">
            <label for="id_origen">Tienda origen</label> <img src="'.base_url().'public/img/mx.jpg" width="38px" height="20px">
            <select class="form-control" id="id_origen" name="id_origen">
                <!--<option value=""></option>-->';
                foreach ($tiendamx as $t) {
                	/*if($id_org==$t->id){
                    $sel="selected";
                  }else{
                    $sel="";
                  }*/
                  if($t->id==$this->session->userdata("id_tienda")){
                  	$html.='<option value="'.$t->id.'">'.$t->nombre.'</option>';
                  }
                }
            $html.='</select>
	        </fieldset>
	    	</div>';

	    	$html.='<div class="col-md-6">
	       		<fieldset class="form-group">
	            <label for="id_destino">Tienda destino</label> <img src="'.base_url().'app-assets/img/flags/us.png">
	            <select class="form-control" id="id_destino" name="id_destino">
	                <option value=""></option>';
	                foreach ($tienda as $t) {
	                	if($id_dest==$t->id){
	                    $sel="selected";
	                  }else{
	                    $sel="";
	                  }
	                  $html.='<option '.$sel.' value="'.$t->id.'">'.$t->nombre.'</option>';
	                }
	            $html.='</select>
	        </fieldset>
	    	</div>';
		}
		echo $html;
  	}

  public function getContenidos(){
  	$id=$this->input->post("id");
  	$html=""; $i=0; $stotal=0;
		$env_paq=$this->General_model->getselectwhereall("envio_paquete",array("id_envio"=>$id));
		foreach($env_paq as $p){
			$i++;
			$stotal=$stotal+($p->impuesto+$p->precio);
			$html.='
						<div class="row pt-10">
	            <div class="col-md-12">
	            	<h4>Paquete '.$i.'</h4>
	            </div>
	            <div class="col-md-3">
                <fieldset class="form-group">
                  <label for="libras">Libras</label>
                  <input type="number" id="libras" class="form-control round" readonly value="'.$p->libras.'">
                </fieldset>
            	</div>
            	<div class="col-md-3">
                <fieldset class="form-group">
                  <label for="precio">Precio</label>
                  <input type="text" id="precio" class="form-control round" readonly value="$'.number_format($p->precio,2,".",".").'">
                </fieldset>
            	</div>
            	<div class="col-md-3">
                <fieldset class="form-group">
                  <label for="impuesto">Impuesto</label>
                  <input type="text" id="impuesto" class="form-control round" readonly value="$'.number_format($p->impuesto,2,".",".").'">
                </fieldset>
            	</div>
            	<div class="col-md-3">
                <fieldset class="form-group">
                  <label for="total">Total</label>
                  <input type="text" id="total" class="form-control round" readonly value="$'.number_format($p->impuesto+$p->precio,2,".",".").'">
                </fieldset>
            	</div>
	            <div class="col-md-12">
                <fieldset class="form-group">
                  <label for="contenido">Contenido del paquete:</label>
                  <textarea id="contenido" readonly class="form-control round" rows="3">'.$p->contenido.'</textarea> 
                </fieldset>
	            </div>
	        </div>';
		}
			$html.='
				<div class="row pt-10">
					<div class="col-md-9">
          </div>
					<div class="col-md-3" style="text-align: end;">
            <fieldset class="form-group">
              <label for="total"><b>Super Total</b></label>
              <input style="text-align:right;font-weight:bold" type="text" id="stotal" class="form-control round" readonly value="$'.number_format($stotal,2,".",".").'">
            </fieldset>
        	</div>
        </div>';
       echo $html;
	}

  public function etiquetas($id){
		$data['env'] = $this->General_model->getselectwhereall("envios",array("id"=>$id));
		$data['env_paq'] = $this->General_model->getselectwhereall("envio_paquete",array("id_envio"=>$id));
		$this->load->view('reportes/etiqueta',$data);      
	}

	public function etiquetaEntrega($id){
		$data['env'] = $this->General_model->getselectwhereall("envios",array("id"=>$id));
		$data['env_paq'] = $this->General_model->getselectwhereall("envio_paquete",array("id_envio"=>$id));
		$data["imprime"] = $this->session->userdata("usuario");
		$this->load->view('reportes/etiquetaEntrega',$data);      
	}

	public function entregado(){
		$id=$this->input->post("id");
		$estatus=$this->input->post("estatus");
		if($estatus==1) //entregado
			$this->General_model->edit_recordn("envios",array("entregado"=>$estatus,"fecha_entregado"=>$this->fecha),array("id"=>$id));
		else if($estatus==3) //reportado incorrecto
			$this->General_model->edit_recordn("envios",array("entregado"=>$estatus),array("id"=>$id));
	}

	public function submitCliente(){
		$data=$this->input->post();
		$data2["tienda"] = $this->id_tienda;
		if(isset($data["id_destino"]))
			$data2["tiendamx"] = $data["tiendamx"];
		else
			$data2["tiendamx"] = 0;
         
		//$data2["tiendamx"] = $data["tiendamx"];
		$nombre_v='';
		$app_v='';
		if($data["tipoc"]==1){
			//$data2["id"] = $data["cliente"];
			$nombre_v=$data["nom_envia"];
		    $app_v=$data["apellido_envia"];

			$data2["nombre"] = $data["nom_envia"];
			$data2["apellidos"] = $data["apellido_envia"];
			$data2["calle"] = $data["calle_envia"];
			$data2["ciudad"] = $data["ciudad_envia"];
			$data2["edo"] = $data["edo_envia"];
			$data2["cp"] = $data["cp_envia"];
			$data2["telefono"] = $data["tel_envia"];
		}else{
			//$data2["id"] = $data["recibe"];
			$nombre_v=$data["nom_recibe"];
		    $app_v=$data["apellido_recibe"];
		    
			$data2["nombre"] = $data["nom_recibe"];
			$data2["apellidos"] = $data["apellido_recibe"];
			$data2["calle"] = $data["calle_recibe"];
			$data2["ciudad"] = $data["ciudad_recibe"];
			$data2["edo"] = $data["edo_recibe"];
			$data2["cp"] = $data["cp_recibe"];
			$data2["telefono"] = $data["tel_recibe"];
		}
		$data2["tipo"] = $data["tipo"];
		//log_message('error', 'tipo: '.$data2["tipo"]);
		//log_message('error', 'tiendamx: '.$data2["tiendamx"]);
		$validar_cliente=0;
		if($data['id_cli']==0){ //insert
			$val_cli=0;
			$resultcli=$this->ModeloClientes->get_validar_cliente($nombre_v,$app_v,$data["tipo"]);
			foreach ($resultcli as $x){
				$val_cli=1;
			}
			if($val_cli==1){
                $validar_cliente=1;
            }else{
            	$validar_cliente=0;
				$data2["reg"]=$this->fecha;
				$data2["update"]=$this->fecha;
				$id=$this->General_model->add_record("clientes",$data2);
			}
		}
		else{ //update
        	$id=$data["id_cli"]; unset($data["id_cli"]);
		    $data2["update"]=$this->fecha;
		    $this->General_model->edit_recordn("clientes",$data2,array("id"=>$id));	
		}
		echo $validar_cliente;
	}

	public function validarCliente(){
		$data=$this->input->post();
		$validar_cliente=0;
		$nombre_v='';
		$app_v='';
		if($data["tipo"]==1){
			$nombre_v=$data["nom_envia"];
		    $app_v=$data["apellido_envia"];
		}else{
			$nombre_v=$data["nom_recibe"];
		    $app_v=$data["apellido_recibe"];
		}
		if($data['id_cli']==0){ //insert
			$val_cli=0;
			$resultcli=$this->ModeloClientes->get_validar_cliente($nombre_v,$app_v,$data["tipo"]);
			foreach ($resultcli as $x){
				$val_cli=1;
			}
			if($val_cli==1){
                $validar_cliente=1;
            }else{
            	$validar_cliente=0;
			}
		}
		echo $validar_cliente;
	}

	public function submit(){
		$data=$this->input->post();

		if(!isset($data['id'])){ //insert

			if(!isset($data["id_cliente_envia"])){
                if(isset($data["nom_envia"])){
                     $dataenvia["nombre"]=$data["nom_envia"];
                }
                if(isset($data["apellido_envia"])){
                     $dataenvia["apellidos"]=$data["apellido_envia"];
                }
                if(isset($data["calle_envia"])){
                     $dataenvia["calle"]=$data["calle_envia"];
                }
                if(isset($data["ciudad_envia"])){
                     $dataenvia["ciudad"]=$data["ciudad_envia"];
                }
                if(isset($data["edo_envia"])){
                     $dataenvia["edo"]=$data["edo_envia"];
                }
                if(isset($data["cp_envia"])){
                     $dataenvia["cp"]=$data["cp_envia"];
                }
                if(isset($data["tel_envia"])){
                     $dataenvia["telefono"]=$data["tel_envia"];
                }
                if(isset($data["tipo"])){
                     $dataenvia["tipo"]=$data["tipo"];
                }
                if(isset($data["id_origen"])){
                     $dataenvia["tienda"]=$data["id_origen"];
                }
                $id=$this->General_model->add_record("clientes",$dataenvia);
                $data["id_cliente_envia"]=$id;
			}

            if(!isset($data["id_cliente"])){
            	if(isset($data["nom_recibe"])){
                     $datarecibe["nombre"]=$data["nom_recibe"];
                }
                if(isset($data["apellido_recibe"])){
                     $datarecibe["apellidos"]=$data["apellido_recibe"];
                }
                if(isset($data["calle_recibe"])){
                     $datarecibe["calle"]=$data["calle_recibe"];
                }
                if(isset($data["ciudad_recibe"])){
                     $datarecibe["ciudad"]=$data["ciudad_recibe"];
                }
                if(isset($data["edo_recibe"])){
                     $datarecibe["edo"]=$data["edo_recibe"];
                }
                if(isset($data["cp_recibe"])){
                     $datarecibe["cp"]=$data["cp_recibe"];
                }
                if(isset($data["tel_recibe"])){
                     $datarecibe["telefono"]=$data["tel_recibe"];
                }
                if(isset($data["tipo"])){
                	if($data["tipo"]==1){
                        $datarecibe["tipo"]=2;
                	}else{
                		$datarecibe["tipo"]=1;
                	}
                }
                if(isset($data["id_destino"])){
                     $dataenvia["tienda"]=$data["id_destino"];
                }
                $id=$this->General_model->add_record("clientes",$datarecibe);
                $data["id_cliente"]=$id;
			}

			$data["reg"]=$this->fecha;
			$data["id_usuario"]=$this->id_usuario;
			$fecha = new DateTime(strtotime("d/m/Y"));
			$data["semana"] = $fecha->format('W');
			$id=$this->General_model->add_record("envios",$data);
		}
		else{ //update
			$id=$data["id"]; unset($data["id"]);
			$this->General_model->edit_recordn("envios",$data,array("id"=>$id));
		}
		echo $id;
	}

	/*public function submitDatosPaquete(){
		$data=$this->input->post();
		if(!isset($data['id'])){ //insert
			$id=$this->General_model->add_record("envio_paquete",$data);
		}
		else{ //update
			$id=$data["id"]; unset($data["id"]);
			$this->General_model->edit_recordn("envio_paquete",$data,array("id"=>$id));
		}
	}*/

	function Export($cliente,$clitxt,$tipocli,$estatus,$tipo,$id_tienda,$fi,$ff,$semana,$num_paq="SNAPLI",$sea_tel="SNAPLI"){
		$cliente = str_replace("%20", " ", $cliente);
		$clitxt = str_replace("%20", " ", $clitxt);
	    $data["cliente"] = $cliente;
	    $data["clientetxt"] = $clitxt;
	    $data["estatus"] = $estatus;
	    $data["tipo"] = $tipo;
	    $data["id_tienda"] = $id_tienda;
	    $data["fi"] = $fi;
	    $data["ff"] = $ff;
	    $data["semana"] = $semana;
	    /*$data["tipo_list"] = 1;
	    $data["num_paq"] = $num_paq;
	    $data["sea_tel"] = $sea_tel;
	    $data["tipocli"] = $tipocli;*/

	    //$data["r"] = $this->ModeloEnvios->getEnvios($data); 
	    $data["r"] = $this->ModeloEnvios->getDatosEnvios($cliente,$clitxt,$tipocli,$estatus,$tipo,$id_tienda,$fi,$ff,$semana,$num_paq,$sea_tel);
	    $this->load->view('operaciones/envios/enviosExcel',$data);
	}

	function ExportWeekShop($cliente,$clitxt,$tipocli,$estatus,$tipo,$id_tienda,$fi,$ff,$tipolist,$semana,$num_paq="SNAPLI",$sea_tel="SNAPLI"){
		$cliente = str_replace("%20", " ", $cliente);
		$clitxt = str_replace("%20", " ", $clitxt);
	    $data["cliente"] = $cliente;
	    $data["clientetxt"] = $clitxt;
	    $data["estatus"] = $estatus;
	    $data["tipo"] = $tipo;
	    $data["tipocli"] = $tipocli;
	    $data["id_tienda"] = $id_tienda;
	    $data["fi"] = $fi;
	    $data["ff"] = $ff;
	    $data["tipo_list"] = $tipolist;
	    $data["semana"] = $semana;

	    /*log_message('error', 'cliente: '.$cliente);
	    log_message('error', 'estatus: '.$estatus);
	    log_message('error', 'tipo: '.$tipo);
	    log_message('error', 'id_tienda: '.$id_tienda);
	    log_message('error', 'tipolist: '.$tipolist);*/

	    $data["r"] = $this->ModeloEnvios->getEnviosWeek($data);
	    $this->load->view('operaciones/envios/enviosExcelWeek',$data);
	}

	public function submitDatosPaquete(){ 
	    $data = $this->input->post('data');
	    $DATA = json_decode($data);

	    for ($i=0; $i<count($DATA); $i++) {
	      $array=array(
	          "num_sub"=>$DATA[$i]->num_sub,
	          "libras"=>$DATA[$i]->libras,
	          "precio" =>$DATA[$i]->precio,
	          "valor_est"=>$DATA[$i]->valor_est,
	          "impuesto"=>$DATA[$i]->impuesto,
	          "num_paquete"=>$DATA[$i]->num_paquete,
	          "contenido" =>$DATA[$i]->contenido,
	          "total"=>$DATA[$i]->total,
	          "id_envio"=>$DATA[$i]->id_envio
	      );
	      //log_message('error', 'id: '.$DATA[$i]->id);
	      if($DATA[$i]->id==0){
	      	if($DATA[$i]->num_sub!="" /*&& $DATA[$i]->libras!=""*/){
	        	$id=$this->General_model->add_record("envio_paquete",$array);
	        }
	      }else{
	        $id=$DATA[$i]->id;
					$this->General_model->edit_recordn("envio_paquete",$array,array("id"=>$id));
	      }
	    }

  	}

	public function delete($id){
		$this->General_model->edit_recordn("envios",array("estatus"=>0),array("id"=>$id));
	}

	function searchclientes(){
	    $cli = $this->input->get('search');
	    $tipo = $this->input->get('tipo');
	    //log_message('error', 'tipo: '.$tipo);
	    $results=$this->ModeloClientes->cliente_search($cli,$tipo);
	    echo json_encode($results->result());
  	}

	public function formatorecibo($id){
		$data['env'] = $this->General_model->getselectwhereall("envios",array("id"=>$id));
		$data['env_paq'] = $this->General_model->getselectwhereall("envio_paquete",array("id_envio"=>$id));
		$this->load->view('reportes/format',$data);
	}

	public function formatohoja($id){
		$data['env'] = $this->General_model->getselectwhereall("envios",array("id"=>$id));
		$data['env_paq'] = $this->General_model->getselectwhereall("envio_paquete",array("id_envio"=>$id));
		$this->load->view('reportes/hoja',$data);
	}

	public function getHistorialEnvios(){
		$id_cli = $this->input->post('id_cli');
		$nombre_em = $this->input->post('nombre_em');
		$html=""; $estatus="";
		$envs = $this->ModeloEnvios->getEnviosHistorial($id_cli,$nombre_em);
		foreach($envs as $e){
			if($e->entregado==0){
		        $estatus= "<div class='row justify-content-center'><div class='col-md-12 text-center'>Sin entregar</div> <div class='col-md-6'>
		        <i style='color: #FFC90E !important;' class='fa fa-spinner fa-spin fa-fw'></i>
		        <span class='sr-only'>Loading...</span>
		        <i style='color: #FFC90E !important;' class='fa fa-circle' aria-hidden='true'></i></div></div>";
			    }else if($e->entregado==1){
			      $estatus= "<div class='row justify-content-center'><div class='col-md-12 text-center'>Entregado</div> <div class='col-md-6'>
			        <i style='color: green !important;' class='fa fa-check animated infinite tada' aria-hidden='true'></i>
			        <i style='color: green !important;' class='fa fa-circle' aria-hidden='true'></i></div></div>";
			    }else if($e->entregado==3){
			      $estatus="<div class='row justify-content-center'><div class='col-md-12 text-center'>Reportado</div> <div class='col-md-6'>
			        <i style='color: red !important;' class='fa fa-spinner fa-spin fa-fw'></i>
			        <span class='sr-only'>Loading...</span>
			        <i style='color: red !important;' class='fa fa-circle' aria-hidden='true'></i></div></div>";
			    }
			    if($e->tipo=="1"){ //eu
		        $num_env = $e->letra.''.$e->folio;
		    }else{
		        if(is_numeric($e->letra)==true){
		          //$num_env = intVal($e->letra)+intVal($e->folio);
		          $num_env = intVal($e->folio);
		        }else{
		          $num_env = $e->letra."".$e->folio; 
		        }
		    }
			$html.="<tr>
							<td>".$num_env."</td>
							<td>".$estatus."</td>
							<!--<td>".$e->folio."</td>-->
							<td>".$e->nom_recibe." ".$e->apellido_recibe."</td>
							<td>".$e->calle_recibe." ".$e->ciudad_recibe." ".$e->edo_recibe." ".$e->cp_recibe."</td>
							<td>".$e->destino."</td>
							<td><button title='Utilizar Destinatario' class='btn btn-info' type='button' onclick='selectDest(".$e->id.")'><i class='fa fa-check' aria-hidden='true'></i></button></td>
						</tr>";
		}//foreach
		echo $html;
	}

	public function getEnvioId(){
    $id = $this->input->post("id");
    $result= $this->General_model->getselectwhereall('envios',array('id'=>$id));
    echo json_encode($result);  
  }

  public function getLista() {
    $params = $this->input->post();
    $datas = $this->ModeloEnvios->getCorteCaja($params);
    $json_data = array("data" => $datas);
    echo json_encode($json_data);
  }

  public function saveComents(){
		$id=$this->input->post("id");
		$coments=$this->input->post("coments");
		$this->General_model->add_record("comentarios_envio",array("id_envio"=>$id,"comentarios"=>$coments,"reg"=>$this->fecha,"id_usuario"=>$this->id_usuario));
		//$this->General_model->edit_recordn("envios",array("comentarios"=>$coments),array("id"=>$id));
  }
  
  public function get_destino_select()
  { 
  	$id=$this->input->post("id");
    $id_destino=$this->input->post("id_destino");
    $lugar=$this->input->post("lugar");
    $id_origen=$this->input->post("id_origen");
  	$html='<select class="form-control round" id="id_tienda_edit" onchange="editar_direccion('.$id.','.$id_origen.','.$lugar.')">';
        $tienda=$this->General_model->getselectwhereall("tienda",array("estatus"=>1,'lugar'=>$lugar));//todas las tiendas
        foreach ($tienda as $t) {
  	        $html.='<option value="'.$t->id.'"'; if($t->id==$id_destino){$html.='selected'; } $html.='>'.$t->nombre.'</option>';
        }
  	$html.='</select>';
  	echo $html;
  }

  public function get_destino_editar()
  { 
    $id=$this->input->post("id");
    $id_destino=$this->input->post("tienda");
    $tipot=$this->input->post("lugar");
    $id_origen=$this->input->post("id_origen"); 
    $fecha = new DateTime(strtotime("d/m/Y"));
    $semana = $fecha->format('W'); 
    $dia = $fecha->format('N');
  	$fol_ult=0;

	$ultFol=0;
	$ultFol=$this->General_model->getUltimoFolio(0,$semana,$id_origen);
	if(isset($ultFol->folio)){ // si existen registros de envios de esta semana
		//folios para sucursales de mexico
		if($this->tipot==2){
			if($dia>5){ //jueves o mas, apartir del viernes se reincia a 1 el folio (consecutivo)
				$ultFol = $fol_ult+1;
			}else{ //aun pertenece a la semana activa
				$ultFol = intval($ultFol->folio)+1;
			}
		}
		//folios para sucursales de EEUU
		if($this->tipot==1){
			$ult_date=date("Y-m-t", strtotime($this->fecha2));
			/*$date = strtotime($this->fecha2);
			$lastdate = strtotime(date("Y-m-t", $date ));   
			$day = date("d", $lastdate);
			log_message('error', 'day: '.$day);*/
			/*log_message('error', 'ult_date: '.$ult_date);
			log_message('error', 'fecha: '.date("Y-m-d"));*/
			if($this->fecha2 == $ult_date){ //fin de mes
			//if($day){ //fin de mes
				$ultFol = $fol_ult+1;
				//log_message('error', 'ultima fecha del mes: '.$ult_date);
			}else{ //aun pertenece a la semana activa
				$ultFol = intval($ultFol->folio)+1;
				//log_message('error', 'aun dentro de mes: '.$ult_date);
			}
		}
	}else{
		$ultFol = $ultFol+1;
	}
    
    $data = array('folio'=>$ultFol,'id_destino'=>$id_destino);
	$this->General_model->edit_recordn("envios",$data,array("id"=>$id));

  }


}
