<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require_once APPPATH.'/third_party/Spout/Autoloader/autoload.php';

//use Box\Spout\Reader\ReaderFactory;  
//use Box\Spout\Common\Type;

class Clientes extends CI_Controller {

	public function __construct() {
    parent::__construct();
    $this->load->model('ModeloClientes');
    $this->load->model('General_model');
    if (!$this->session->userdata('logeado')){
      redirect('/Login');
  	}else{
      $this->perfilid=$this->session->userdata('perfilid');
      //ira el permiso del modulo
      $this->load->model('Login_model');
      $permiso=$this->Login_model->getviewpermiso($this->perfilid,1);// 1 es el id del submenu
      if ($permiso==0) {
          redirect('/Sistema');
      }
  	}
  	date_default_timezone_set('America/Mexico_City');
  	$this->fecha=date("Y-m-d H:i:s");
  	//$this->load->library('excel');
  }

	public function index(){
		$data2["activo"]=1;
		$data2["mn"]=1;
		$this->load->view('templates/header');
    $this->load->view('templates/navbar',$data2);
    //$this->load->view('notification');
		$this->load->view('catalogos/clientes/listado');
		$this->load->view('templates/footer');
    $this->load->view('catalogos/clientes/clienteslistajs');
	}

	public function alta($id=0){
		$data2["activo"]=1;
		$data2["mn"]=1;
		if($id>0){
			$data['cliente'] = $this->General_model->get_record("id",$id,"clientes");
		}
		else{
			$data=array();
		}
		$data["tienda"]=$this->General_model->getselectwhereall("tienda",array("lugar"=>1,"estatus"=>1));//EU
		$data["tiendamx"]=$this->General_model->getselectwhereall("tienda",array("lugar"=>2,"estatus"=>1));//MX
		$this->load->view('templates/header');
    $this->load->view('templates/navbar',$data2);
    //$this->load->view('notification');
		$this->load->view('catalogos/clientes/form',$data);
		$this->load->view('templates/footer');
		$this->load->view('catalogos/clientes/clientesaddjs');
	}

	public function clientesall() {
    $params = $this->input->post();
    $clt = $this->ModeloClientes->getclientes($params);
    $totalRecords=$this->ModeloClientes->total_clientes($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totalRecords),  
        "recordsFiltered" => intval($totalRecords),
        "data"            => $clt->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
   } 

	public function submit(){
		$data=$this->input->post();
		if(!isset($data['id'])){ //insert
			$data["reg"]=$this->fecha;
			$data["update"]=$this->fecha;
			$id=$this->General_model->add_record("clientes",$data);
		}
		else{ //update
			$id=$data["id"]; unset($data["id"]);
			$data["update"]=$this->fecha;
			$this->General_model->edit_recordn("clientes",$data,array("id"=>$id));
		}
	}

	public function delete($id){
		$this->General_model->edit_recordn("clientes",array("estatus"=>0),array("id"=>$id));
	}

	function searchclientes(){
    $cli = $this->input->get('search');
    $results=$this->ModeloClientes->cliente_search($cli);
    echo json_encode($results->result());
  }

  public function getSelectTipo(){
    $tipo = $this->input->post('tipo');
    $id = $this->input->post('id_cli');
    $getcli = $this->General_model->get_record("id",$id,"clientes");
    $html="";

    $tienda=$this->General_model->getselectwhereall("tienda",array("lugar"=>1,"estatus"=>1));//EU
		$tiendamx=$this->General_model->getselectwhereall("tienda",array("lugar"=>2,"estatus"=>1));//MX
    
    if($tipo==1){ //EEUU
    	$html='<div class="col-md-6">
	      <fieldset class="form-group">
	        <label for="tienda">Tienda que recibe</label> <img src="'.base_url().'app-assets/img/flags/us.png">
	        <select class="form-control" id="tienda" name="tienda">
            <option value=""></option>';
            foreach ($tienda as $t) {
            	if($id>0 && $getcli->tienda==$t->id){
            		$sel="selected";
            	}else{
            		$sel="";
            	}
              $html.="<option ".$sel." value='".$t->id."'>".$t->nombre."</option>";
            }
	        $html.='</select>
	      </fieldset>
	  	</div>
	  	<div class="col-md-6">
        <fieldset class="form-group">
          <label for="tiendamx">Tienda que envía</label> <img src="'.base_url().'public/img/mx.jpg" width="38px" height="20px">
          <select class="form-control" id="tiendamx" name="tiendamx">
            <option value=""></option>';
            foreach ($tiendamx as $t) {
            	if($id>0 && $getcli->tiendamx==$t->id){
            		$sel="selected";
            	}else{
            		$sel="";
            	}
              $html.="<option ".$sel." value='".$t->id."'>".$t->nombre."</option>";
            }
          $html.='</select>
        </fieldset>
    	</div>';
    }else{
    	$html='<div class="col-md-6">
	      <fieldset class="form-group">
	        <label for="tiendamx">Tienda que recibe</label> <img src="'.base_url().'public/img/mx.jpg" width="38px" height="20px">
	        <select class="form-control" id="tiendamx" name="tiendamx">
            <option value=""></option>';
            foreach ($tiendamx as $t) {
            	if($id>0 && $getcli->tiendamx==$t->id){
            		$sel="selected";
            	}else{
            		$sel="";
            	}
              $html.="<option ".$sel." value='".$t->id."'>".$t->nombre."</option>";
            }
	        $html.='</select>
	      </fieldset>
	  	</div>
	  	<div class="col-md-6">
        <fieldset class="form-group">
          <label for="tienda">Tienda que envía</label> <img src="'.base_url().'app-assets/img/flags/us.png">
          <select class="form-control" id="tienda" name="tienda">
            <option value=""></option>';
            foreach ($tienda as $t) {
            	if($id>0 && $getcli->tienda==$t->id){
            		$sel="selected";
            	}else{
            		$sel="";
            	}
              $html.="<option ".$sel." value='".$t->id."'>".$t->nombre."</option>";
            }
          $html.='</select>
        </fieldset>
    	</div>';
    }
    echo $html;
    
  }

  public function cargaDatosMaisivos(){
  	$data2["activo"]=1;
		$data2["mn"]=1;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar',$data2);
		$this->load->view('catalogos/clientes/cargaExcel');
		$this->load->view('templates/footer');
  }

  function guardar_clientes(){
    $configUpload['upload_path'] = FCPATH.'mallasDatos/';
    $configUpload['allowed_types'] = 'xls|xlsx|csv';

		$this->load->library('upload', $configUpload);
		$this->upload->do_upload('inputFile');  
		$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
		$file_name = $upload_data['file_name']; //uploded file name
		$extension=$upload_data['file_ext'];    // uploded file extension
		//===========================================
		  $objReader= PHPExcel_IOFactory::createReader('csv'); // For cvs delimitado por comas  
		  $objReader->setReadDataOnly(true);          
		  //Load excel file
		  $objPHPExcel=$objReader->load(FCPATH.'mallasDatos/'.$file_name);      
		  $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow(); //Count Numbe of rows avalable in excel         
		  $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                
		  //loop from first data untill last data

		  for($i=2;$i<=$totalrows;$i++){
		    $nombre = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
		    $apellido = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
		    $apellido2 = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
		    $apellido3 = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
		    $apellido4 = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
		    $tel = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
		    $tienda = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
		    $reg = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();

		    $data = array(
		      'nombre' => $nombre,
		      'apellidos' => mb_strtoupper($apellido." ".$apellido2." ".$apellido3." ".$apellido4),
		      'telefono' => $tel,
		      'tienda' => $tienda,
		      'reg' => $reg
		    );               
		    $this->General_model->add_record("clientes",$data);
		  }
		  unlink('fileExcel/'.$file_name); //File Deleted After uploading in database . 

		//===========================================
		$output = [];
		echo json_encode($output);
		redirect('Clientes');
   }

}
