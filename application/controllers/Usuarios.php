<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function __construct() {
    parent::__construct();
    $this->load->model('General_model');
    if (!$this->session->userdata('logeado')){
      redirect('/Login');
    }else{
      $this->perfilid=$this->session->userdata('perfilid');
      //ira el permiso del modulo
      $this->load->model('Login_model');
      $permiso=$this->Login_model->getviewpermiso($this->perfilid,2);// 2 es el id del submenu
      if ($permiso==0) {
          redirect('/Sistema');
      }
    }
  }

	public function index(){
		$data2["activo"]=1;
		$data2["mn"]=1;
		$this->load->view('templates/header');
    $this->load->view('templates/navbar',$data2);
    //$this->load->view('notification');
		$this->load->view('catalogos/usuarios/listado');
		$this->load->view('templates/footer');
    $this->load->view('catalogos/usuarios/usu_listajs');
	}

	public function alta($id=0){
		$data2["activo"]=1;
		$data2["mn"]=1;
		if($id>0){
			$data['usu'] = $this->General_model->get_record("id",$id,"usuarios");
		}
		else{
			$data=array();
		}
		$data["tienda"]=$this->General_model->getselectwhereall("tienda",array("estatus"=>1));//todas las tiendas
		$data["perf"]=$this->General_model->get_table_active2("perfiles");
		$data['permisos']=$this->General_model->get_records_condition("id_usuario=$id","perfiles_detalles");
		$this->load->view('templates/header');
    $this->load->view('templates/navbar',$data2);
    //$this->load->view('notification');
		$this->load->view('catalogos/usuarios/form',$data);
		$this->load->view('templates/footer');
    $this->load->view('catalogos/usuarios/usu_addjs');
	}

	public function datatable_records(){
		$params = $this->input->post();
		//$datas = $this->General_model->get_table_active2("usuarios");
		$datas = $this->General_model->get_table_users($params["lugar"]);
    $json_data = array("data" => $datas);
    echo json_encode($json_data);
	}

	public function submit(){
		$data=$this->input->post();

		if($data["contrasenia"]!="XXXXXX"){
			$data["contrasenia"] = password_hash($data["contrasenia"],PASSWORD_DEFAULT);
		}else{
			unset($data["contrasenia"]);	
		}
		unset($data["contrasenia2"]);
		if(isset($data['permisos'])){
			$permisos=$data["permisos"]; unset($data["permisos"]);
		}	
		$this->db->trans_start();
		if(!isset($data['id'])){ //insert
			$id=$this->General_model->add_record("usuarios",$data);
		}
		else{ //update
			$id=$data["id"]; unset($data["id"]);
			$this->General_model->edit_recordn("usuarios",$data,array("id"=>$id));
		}

		if(isset($permisos)){
			$this->General_model->delete_records("id_usuario=$id","perfiles_detalles");
			foreach ($permisos as $p) {
				$this->General_model->add_record("perfiles_detalles",array("id_perfil"=>$data['id_perfil'],"id_usuario"=>$id,"MenusubId"=>$p));
			}
		}
		$this->db->trans_complete();
		$this->db->trans_status();
	}

	public function delete($id){
		$this->General_model->edit_recordn("usuarios",array("status"=>0),array("id"=>$id));
	}

	public function verificaUser(){
		$user = $this->input->post("user");
		$data = $this->General_model->getselectwhereall("usuarios",array("usuario"=>$user,"status"=>1));
		$userVal="";
		foreach($data as $u){
			$userVal = $u->usuario;
		}
    echo $userVal;
	}

}
