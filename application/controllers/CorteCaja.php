<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CorteCaja extends CI_Controller {
	
    function __construct()    {
        parent::__construct();
        $this->load->model('ModeloEnvios');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->tipot=$this->session->userdata('tipot');
            $this->load->model('Login_model');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,6);// 6 es el id del submenu
            if ($permiso==0) {
                 redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }

	public function index()
	{
        $data2["activo"]=1;
        $data2["mn"]=2;
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar',$data2);
		//$this->load->view('notification');
        $this->load->view('operaciones/corte_caja/corte');
        $this->load->view('templates/footer');
        $this->load->view('operaciones/corte_caja/cortejs');
	}

    public function getCorte() {
        $params = $this->input->post();
        $params["tipo"]=$this->tipot;
        $datas = $this->ModeloEnvios->getCorteCaja($params);
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
    }

    public function getTotalesCorte() {
        $params = $this->input->post();
        $params["tipo"]=$this->tipot;
        $stotal=0; $stotaltc=0; $stotaltd=0;
        if($params["id_tienda"]!="0"){
            $datas = $this->ModeloEnvios->getTotalesCorteCaja($params);
            foreach ($datas as $k) {
                $stotal = $stotal+$k->stotal;
                $stotaltc = $stotaltc+$k->stotaltc;
                //$stotaltd = $stotaltd+$k->stotaltd;
            }
            echo json_encode(array("unica"=>1,"stotal"=>$stotal,"stotaltc"=>$stotaltc,"stotaltd"=>$stotaltd));
        }else{
            $datasEfec = $this->ModeloEnvios->getTotalMetodo($params,'metodo=1'); 
            $datasTarj = $this->ModeloEnvios->getTotalMetodo($params,'metodo=2');
            echo json_encode(array("unica"=>2,"datasEfec"=>$datasEfec,"datasTarj"=>$datasTarj)); 
        }    
    }

    function Export($fi,$ff,$tienda){
        $data["fi"] = $fi;
        $data["ff"] = $ff;
        $data["id_tienda"] = $tienda;
        $params["tipo"]=$this->tipot;
        $params = array("fi"=>$fi,"ff"=>$ff,"id_tienda"=>$tienda);
        $data["params"] = $params;
        $data["r"] = $this->ModeloEnvios->getCorteCaja($params);
        $this->load->view('operaciones/corte_caja/corteExcel',$data);
    } 
}
