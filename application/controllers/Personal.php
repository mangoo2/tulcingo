<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('General_model');
        if (!$this->session->userdata('logeado')){
          redirect('/Login');
	    }else{
	        $this->perfilid=$this->session->userdata('perfilid');
	        //ira el permiso del modulo
	        $this->load->model('Login_model');
	        $permiso=$this->Login_model->getviewpermiso($this->perfilid,8);// 8 es el id del submenu
	        if ($permiso==0) {
	            redirect('/Sistema');
	        }
	    }
    }

	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('catalogos/personal/listado');
		$this->load->view('templates/footer');
        $this->load->view('catalogos/personal/personal_listajs');
	}

	public function alta($id=0){
		if($id>0){
			$data['per'] = $this->General_model->get_record("id",$id,"personal_operador");
		}
		else{
			$data=array();
		}
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('catalogos/personal/form',$data);
		$this->load->view('templates/footer');
        $this->load->view('catalogos/personal/personal_addjs');
	}

	public function datatable_records(){
		$params = $this->input->post();
		$datas = $this->General_model->get_table_active("personal_operador");
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
	}

	public function submit(){
		$data=$this->input->post();
		if(!isset($data['id'])){ //insert
			$id=$this->General_model->add_record("personal_operador",$data);
		}
		else{ //update
			$id=$data["id"]; unset($data["id"]);
			$this->General_model->edit_recordn("personal_operador",$data,array("id"=>$id));
		}
	}

	public function delete($id){
		$this->General_model->edit_recordn("personal_operador",array("estatus"=>0),array("id"=>$id));
	}

}
