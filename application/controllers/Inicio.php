<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('ModeloEnvios');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
    }
	public function index()
	{
        $data2["activo"]=0;
        $data2["mn"]=0;
        $data["env"]=$this->ModeloEnvios->getDatosInicio($this->fechahoy);
        $data["tenv"]=$this->ModeloEnvios->getDatosInicioTotal($this->fechahoy);

    	$this->load->view('templates/header');
        $this->load->view('templates/navbar',$data2);
		$this->load->view('inicio',$data);
        //$this->load->view('notification');
		$this->load->view('templates/footer');
        $this->load->view('iniciojs');
	}

    public function formato1()
    {
        $this->load->view('templates/header');
        //$this->load->view('templates/navbar');
        //$this->load->view('inicio');
        $this->load->view('format');
        //$this->load->view('notification');
        //$this->load->view('templates/footer');
        $this->load->view('iniciojs');
    }
}
