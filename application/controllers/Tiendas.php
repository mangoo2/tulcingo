<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiendas extends CI_Controller {

	public function __construct() {
    parent::__construct();
    $this->load->model('ModeloClientes');
    $this->load->model('General_model');
    if (!$this->session->userdata('logeado')){
      redirect('/Login');
  	}else{
      $this->perfilid=$this->session->userdata('perfilid');
      //ira el permiso del modulo
      $this->load->model('Login_model');
      $permiso=$this->Login_model->getviewpermiso($this->perfilid,4);// 4 es el id del submenu
      if ($permiso==0) {
          redirect('/Sistema');
      }
  	}
  	date_default_timezone_set('America/Mexico_City');
  	$this->fecha=date("Y-m-d H:i:s");
   }

	public function index(){
		$data2["activo"]=1;
		$data2["mn"]=1;
		$this->load->view('templates/header');
    $this->load->view('templates/navbar',$data2);
    //$this->load->view('notification');
		$this->load->view('catalogos/tiendas/listado');
		$this->load->view('templates/footer');
    $this->load->view('catalogos/tiendas/tiendalistajs');
	}

	public function alta($id=0){
		$data2["activo"]=1;
		$data2["mn"]=1;
		if($id>0){
			$data['tienda'] = $this->General_model->get_record("id",$id,"tienda");
		}
		else{
			$data=array();
		}
		$this->load->view('templates/header');
    $this->load->view('templates/navbar',$data2);
    //$this->load->view('notification');
		$this->load->view('catalogos/tiendas/form',$data);
		$this->load->view('templates/footer');
		$this->load->view('catalogos/tiendas/tiendaaddjs');
	}

	public function getTiendas() {
   	$params = $this->input->post();
		$datas = $this->General_model->getselectwhereall("tienda",array("lugar"=>$params["lugar"]));
    $json_data = array("data" => $datas);
    echo json_encode($json_data);
  } 

	public function submit(){
		$data=$this->input->post();
		if(!isset($data['id'])){ //insert
			$id=$this->General_model->add_record("tienda",$data);
		}
		else{ //update
			$id=$data["id"]; unset($data["id"]);
			$this->General_model->edit_recordn("tienda",$data,array("id"=>$id));
		}
	}

	public function delete($id){
		$this->General_model->edit_recordn("tienda",array("estatus"=>0),array("id"=>$id));
	}


}
