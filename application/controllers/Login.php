<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
    }
	public function index()
	{
        $this->load->view('login');
	}
	public function login(){
		$username = $this->input->post('usuario');
        $password = $this->input->post('password');
        // Inicializamos la variable de respuesta en 0;
        $count = 0;
        // Obtenemos los datos del usuario ingresado mediante su usuario
        //log_message('error', 'usuario: '.$username);
        $respuesta = $this->Login_model->login($username);
        $contrasena='';
        foreach ($respuesta as $item) {
            $contrasena =$item->contrasenia;
        }
        // Verificamos si las contraseñas son iguales
        /*log_message('error', 'contra1: '.$password);
        log_message('error', 'contra2: '.$contrasena);*/
        $verificar = password_verify($password,$contrasena);

        // En caso afirmativo, inicializamos datos de sesión
        if ($verificar) 
        {
            $data = array(
                        'logeado' => true,
                        'usuarioid' => $respuesta[0]->id,
                        'usuario' => $respuesta[0]->usuario,
                        'perfilid'=>$respuesta[0]->id_perfil,
                        'id_tienda'=>$respuesta[0]->id_tienda,
                        'nom_tie'=>$respuesta[0]->nom_tie,
                        'tipot'=>$respuesta[0]->tipot,
                        'menu' => $this->get_menu($respuesta[0]->id_perfil),
                    );
            $this->session->set_userdata($data);
            $count=1;
        }
        // Devolvemos la respuesta
        echo $count;
	}

	public function logout(){
		$this->session->sess_destroy();
        redirect(base_url(), 'refresh');
	}

    private function get_menu($id){
        //log_message('error', 'id: '.$id);
        $id_perf = $id;
        $menu=$this->Login_model->getMenus($id_perf);
        foreach ($menu as $m) {
            $m->submenu=$this->Login_model->submenus($id_perf,$m->MenuId);
        }
        return $menu;
    }
}
