<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
	public function __construct(){
        parent::__construct();
        $this->fechaactual=date('Y-m-d');
    }
	function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function totalguiashoy($cliente){
        $sql = "SELECT COUNT( * ) AS total FROM `ordenes` WHERE cliente=$cliente AND fecha_reg_sys BETWEEN '$this->fechaactual 00:00:00' AND '$this->fechaactual 23:59:59'";
        $query = $this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function totalguiasciclo($cliente,$inicio,$fin){
        $sql = "SELECT COUNT( * ) AS total FROM `ordenes` WHERE cliente=$cliente AND fecha_reg_sys BETWEEN '$inicio 00:00:00' AND '$fin 23:59:59'";
        $query = $this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function guiasstatusg($cliente){
        $sql = "SELECT env.estatus, ord.num_guia
                FROM envios AS env
                INNER JOIN ordenes AS ord ON ord.id = env.id_orden
                WHERE ord.cliente =$cliente";
        log_message('error', 'sql: '.$sql);
        $query = $this->db->query($sql);
        return $query; 
    }
    function guiasstatus($cliente,$guia){
        $sql = "SELECT env.estatus, ord.num_guia
                FROM envios AS env
                INNER JOIN ordenes AS ord ON ord.id = env.id_orden
                WHERE ord.cliente =$cliente and ord.num_guia='$guia'";
                log_message('error', 'sql: '.$sql);
        $query = $this->db->query($sql);
        return $query; 
    }
}

