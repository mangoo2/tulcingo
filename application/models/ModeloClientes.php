<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloClientes extends CI_Model {
    public function __construct(){
        parent::__construct();
    }

    function getclientes($params){
        $columns = array( 
            0=>'cli.id', 
            1=>"cli.nombre",
            2=>'cli.apellidos',
            3=>'cli.tiendamx',
            4=>'cli.tienda',
            5=>'cli.calle',
            6=>'cli.ciudad',
            7=>'cli.edo',
            8=>'cli.cp',
            9=>'cli.telefono',
            10=>'cli.telefono2',
            11=>'cli.comentarios',
            12=>'ti1.nombre as tienda1',
            13=>'ti2.nombre as tienda2',
            14=>'cli.update',
            15=>'cli.reg',
            16=>'concat(cli.nombre," ",cli.apellidos) as nombre_ap'
        );
        $columns_seach = array( 
            0=>'cli.id', 
            1=>"cli.nombre",
            2=>'cli.apellidos',
            3=>'cli.tiendamx',
            4=>'cli.tienda',
            5=>'cli.calle',
            6=>'cli.ciudad',
            7=>'cli.edo',
            8=>'cli.cp',
            9=>'cli.telefono',
            10=>'cli.telefono2',
            11=>'cli.comentarios',
            12=>'ti1.nombre',
            13=>'ti2.nombre',
            14=>'cli.update',
            15=>'cli.reg',
            16=>'concat(cli.nombre," ",cli.apellidos)'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('clientes cli');
        $this->db->join('tienda ti1', 'ti1.id=cli.tienda',"left"); //EU
        $this->db->join('tienda ti2', 'ti2.id=cli.tiendamx',"left"); //MX

        $this->db->where("tipo",$params["lugar"]);

        $this->db->where("cli.estatus",1);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns_seach as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns_seach[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function total_clientes($params){
        $columns = array( 
            0=>'cli.id', 
            1=>"cli.nombre",
            2=>'cli.apellidos',
            3=>'cli.tiendamx',
            4=>'cli.tienda',
            5=>'cli.calle',
            6=>'cli.ciudad',
            7=>'cli.edo',
            8=>'cli.cp',
            9=>'cli.telefono',
            10=>'cli.telefono2',
            11=>'cli.comentarios',
            12=>'ti1.nombre as tienda1',
            13=>'ti2.nombre as tienda2',
            14=>'cli.update',
            15=>'cli.reg',
            16=>'concat(cli.nombre," ",cli.apellidos) as nombre_ap'
        );
        $columns_seach = array( 
            0=>'cli.id', 
            1=>"cli.nombre",
            2=>'cli.apellidos',
            3=>'cli.tiendamx',
            4=>'cli.tienda',
            5=>'cli.calle',
            6=>'cli.ciudad',
            7=>'cli.edo',
            8=>'cli.cp',
            9=>'cli.telefono',
            10=>'cli.telefono2',
            11=>'cli.comentarios',
            12=>'ti1.nombre',
            13=>'ti2.nombre',
            14=>'cli.update',
            15=>'cli.reg'
        );
        
        $this->db->select('COUNT(*) as total');
        $this->db->from('clientes cli');
        $this->db->join('tienda ti1', 'ti1.id=cli.tienda',"left"); //EU
        $this->db->join('tienda ti2', 'ti2.id=cli.tiendamx',"left"); //MX
        $this->db->where("cli.estatus",1);

        $this->db->where("tipo",$params["lugar"]);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns_seach as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        ///$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }

    /*function cliente_search($cli,$tipo){
        $strq = "SELECT cli.*, cli.nombre as cliente 
        FROM clientes as cli
        where tipo=".$tipo." and cli.estatus=1 and cli.nombre like '%".$cli."%'"."
        or tipo=".$tipo." and cli.estatus=1 and concat(cli.nombre,' ',cli.apellidos) like '%".$cli."%'"." 
        or tipo=".$tipo." and cli.estatus=1 and cli.apellidos like '%".$cli."%'";
        $query = $this->db->query($strq);
        return $query;
    }*/

    function cliente_search($cli,$tipo){
        $sel=""; $where=""; $group="";
        if($tipo==1){ //envia -- EEU a MX
            $sel="e.id_cliente_envia as id, e.nom_envia as nombre, e.apellido_envia as apellidos, e.calle_envia as calle, ciudad_envia as ciudad, edo_envia as edo, cp_envia as cp, tel_envia as telefono, e.nom_envia as cliente";
            //$join="join envios e on e.tipo=".$tipo." and e.estatus=1 AND concat(e.nom_envia,' ',e.apellido_envia) like '%CLIENTE Q%'";
            //$where="where e.tipo=".$tipo." and e.estatus=1 AND concat(e.nom_envia,' ',e.apellido_envia) like '%".$cli."%' ";
            $where="where e.estatus=1 AND concat(e.nom_envia,' ',e.apellido_envia) like '%".$cli."%' ";
            $group="group by nom_envia";
        }else{
            $sel="e.id_cliente as id, e.nom_recibe as nombre, e.apellido_recibe as apellidos, e.calle_recibe as calle, ciudad_recibe as ciudad, edo_recibe as edo, cp_recibe as cp, tel_recibe as telefono, e.nom_recibe as cliente";
            
            //$where="where e.tipo=".$tipo." and e.estatus=1 AND concat(e.nom_recibe,' ',e.apellido_recibe) like '%".$cli."%' ";
            $where="where e.estatus=1 AND concat(e.nom_recibe,' ',e.apellido_recibe) like '%".$cli."%' ";
            $group="group by nom_recibe";
        }
        $strq = "SELECT cli.id, cli.nombre, cli.apellidos, cli.calle,cli.ciudad,cli.edo,cli.cp,cli.telefono, cli.nombre as cliente
        FROM clientes as cli
        where cli.estatus=1 and cli.nombre like '%".$cli."%'"."
        or cli.estatus=1 and concat(cli.nombre,' ',cli.apellidos) like '%".$cli."%'"." 
        or cli.estatus=1 and cli.apellidos like '%".$cli."%'"."
        /*group by concat(nombre,' ',apellidos)*/

        UNION 

        SELECT ".$sel."
        FROM envios as e
        ".$where." 
        group by concat(nom_envia,' ',apellido_envia)";

        $query = $this->db->query($strq);
        return $query;
    }
     
    function get_validar_cliente($nombre,$app,$tipo)
    {
        $sql="SELECT * FROM clientes WHERE UPPER(nombre) = UPPER('$nombre') AND UPPER(apellidos) = UPPER('$app') AND tipo=$tipo";
        $query = $this->db->query($sql);
        return $query->result();
    } 
    

}  