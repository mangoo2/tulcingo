<?php
class General_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    public function get_table($table){
    	$sql = "SELECT * FROM $table";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_table_active($table){
    	$sql = "SELECT * FROM $table WHERE estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_table_active2($table){
        $sql = "SELECT * FROM $table WHERE status=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_record($col,$id,$table){
    	$sql = "SELECT * FROM $table WHERE $col=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    public function get_records_condition($condition,$table){
    	$sql = "SELECT * FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function add_record($table,$data){
    	$this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function edit_record($cos,$id,$data,$table){
    	$this->db->set($data);
        $this->db->where($cos, $id);
        return $this->db->update($table);
    }
    public function edit_recordn($table,$data,$wherecols){
        $this->db->set($data);
        $this->db->where($wherecols);
        return $this->db->update($table);
    }

    public function delete_records($condition,$table){
    	$sql = "DELETE FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query;
    }
    public function getselectwhere($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    public function getselectwhereall($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);
        $query=$this->db->get();
        return $query->result();
    }
    public function getselectlike($tables,$values,$search){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);
        $this->db->or_like($search);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    /*public function getUltimoFolio(){
        $this->db->select("id");
        $this->db->from("envios");
        $this->db->order_by("id","desc");
        $this->db->limit(1);
        $query=$this->db->get();
        return $query->row();
    }*/

    public function getUltimoFolio($tipo,$semana,$id_org){
        $this->db->select("id, IFNULL(folio,0) as folio");
        $this->db->from("envios");
        $this->db->where("semana",$semana);
        //if($tipo==2){ //mx -> eu
        $this->db->where("id_origen",$id_org);
        //}
        $this->db->order_by("id","desc");
        $this->db->limit(1);
        $query=$this->db->get();
        return $query->row();
    }

    public function get_table_users($lugar){
        $this->db->select("u.*, t.nombre as tienda, t.lugar");
        $this->db->from("usuarios u");
        $this->db->join("tienda t","t.id=u.id_tienda");
        $this->db->where("t.lugar",$lugar);
        $this->db->where("u.status",1);
        $query=$this->db->get();
        return $query->result();
    }
}