<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloEnvios extends CI_Model {
    public function __construct(){
        parent::__construct();
    }

    function getEnvios($params){ //falta comparar por # de semana
        $columns = array( 
            0=>'e.id', 
            1=>"e.nom_recibe",
            2=>'e.apellido_recibe',
            3=>'e.calle_recibe',
            4=>'e.ciudad_recibe',
            5=>'e.edo_recibe',
            6=>'e.cp_recibe',
            7=>'e.tel_recibe',
            8=>'e.nom_envia',
            9=>'e.apellido_envia',
            10=>'e.calle_envia',
            11=>'e.ciudad_envia',
            12=>'e.edo_envia',
            13=>'e.cp_envia',
            14=>'e.tel_envia',
            15=>'t1.nombre as origen',
            16=>'t2.nombre as destino',
            17=>'e.folio',
            18=>'e.entregado',
            19=>'e.reg',
            20=>'concat(e.nom_recibe," ",e.apellido_recibe) as recibe',
            21=>'concat(e.nom_envia," ",e.apellido_envia) as envia',
            //22=>'ep.contenido',
            23=>'u.usuario',
            24=>'concat(u.nombre," ",u.apellidos) as user',
            25=>"(select count(*) from envio_paquete ep where ep.id_envio=e.id) as paquetes",
            26=>"(select GROUP_CONCAT(contenido SEPARATOR '<br>') as contenido from envio_paquete ep2 where ep2.id_envio=e.id) as contenido, ",
            27=>"FORMAT((select sum(libras) from envio_paquete ep3 where ep3.id_envio=e.id),2) as slibras",
            28=>",DATE_FORMAT(reg,  '%d / %m / %Y %r') AS fecha_reg",
            29=>",DATE_FORMAT(fecha_entregado,  '%d / %m / %Y %r') AS fecha_entrega",
            //30=>"e.comentarios",
            31=>"(select sum(total) from envio_paquete ep4 where ep4.id_envio=e.id) as stotal",
            32=>"(select sum(valor_est) from envio_paquete ep5 where ep5.id_envio=e.id) as stotalve",
            33=>"IFNULL((select GROUP_CONCAT(concat(u.nombre, ': ', DATE_FORMAT(reg, '%d/%m/%Y %r'), ' - ', ce.comentarios) SEPARATOR '<br>') as coments from comentarios_envio ce join usuarios u on u.id=ce.id_usuario where ce.id_envio=e.id),'') as coments",
            34=>'e.tipo',
            35=>'t2.letra',
            36=>'e.semana',
            37=>'e.id_cliente',
            38=>'e.id_cliente_envia',
            39=>'concat(t2.letra,"",e.folio) as mi_folio',
            40=>'concat(t2.letra,"",e.folio) as mi_folio2',
            41=>'IF(t1.letra REGEXP "^[0-9]+$","isNumeric","noNumeric") as band_number',
            42=>'t2.lugar AS lugar2',
            43=>'e.id_destino',
            44=>'e.id_origen'
        );
        $columns_seach = array( 
            0=>'e.id', 
            1=>"e.nom_recibe",
            2=>'e.apellido_recibe',
            3=>'e.calle_recibe',
            4=>'e.ciudad_recibe',
            5=>'e.edo_recibe',
            6=>'e.cp_recibe',
            7=>'e.tel_recibe',
            8=>'e.nom_envia',
            9=>'e.apellido_envia',
            10=>'e.calle_envia',
            11=>'e.ciudad_envia',
            12=>'e.edo_envia',
            13=>'e.cp_envia',
            14=>'e.tel_envia',
            15=>'t1.nombre',
            16=>'t2.nombre',
            17=>'e.folio',
            18=>'e.entregado',
            19=>'e.reg',
            //20=>'ep.contenido',
            21=>'u.apellidos',
            22=>'u.nombre',
            23=>"DATE_FORMAT(reg,  '%d / %m / %Y %r')",
            24=>"DATE_FORMAT(fecha_entregado,  '%d / %m / %Y %r')",
            //25=>"e.comentarios",
            26=>"(select sum(total) from envio_paquete ep4 where ep4.id_envio=e.id)",
            27=>"(select sum(valor_est) from envio_paquete ep5 where ep5.id_envio=e.id)",
            28=>"IFNULL((select GROUP_CONCAT(concat(u.nombre, ': ', DATE_FORMAT(reg, '%d/%m/%Y %r'), ' - ', ce.comentarios) SEPARATOR '<br>') as coments from comentarios_envio ce join usuarios u on u.id=ce.id_usuario where ce.id_envio=e.id),'')",
            36=>'e.semana',
            37=>'e.id_cliente',
            38=>'e.id_cliente_envia',
            39=>'concat(t1.letra,"",e.folio)',
            40=>'concat(t2.letra,"",e.folio)',
            41=>'IF(t1.letra REGEXP "^[0-9]+$",(t1.letra+e.folio),concat(t1.letra,"",e.folio))',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $sel2="";
        //if($params["cliente"]!="" && $params["cliente"]!="0"){ //cliente registrado
            $sel2="IF(id_cliente>0 && id_cliente!='',(select c.telefono from clientes c where c.id=e.id_cliente),'') as telcli";
            $sel2.=", IF(id_cliente>0 && id_cliente!='',(select c.telefono2 from clientes c where c.id=e.id_cliente),'') as telcli2";
        //}
        $this->db->select($select.", ".$sel2);
        $this->db->from('envios e');
        $this->db->join('tienda t1', 't1.id=e.id_origen');
        $this->db->join('tienda t2', 't2.id=e.id_destino');
        //$this->db->join('envio_paquete ep', 'ep.id_envio=e.id');
        $this->db->join('usuarios u', 'u.id=e.id_usuario');
        if($params["estatus"]!=2){
            $this->db->where("entregado",$params["estatus"]);
        }
        if($params["tipo"]!="" && $params["tipo_list"]==1 /*&& $params["cliente"]==""*/ && $params["num_paq"]=="SNAPLI"){ //listado normal - principal
            $this->db->where("tipo",$params["tipo"]);
        }
        //log_message('error', 'tipo_list: '.$params["tipo_list"]); //2=modal
        //log_message('error', 'tipo: '.$params["tipo"]); // 2=tipo de envio

        if($params["id_tienda"]!="0" && $params["tipo_list"]==1 && $params["cliente"]=="" && $params["num_paq"]=="SNAPLI"){ //listado normal - principal
            $this->db->where("id_origen",$params["id_tienda"]);
        }else if($params["id_tienda"]!="0" && $params["tipo_list"]==2){ //listado por sucursal de modal 
            $this->db->where("id_destino",$params["id_tienda"]);
        }
        else if($params["id_tienda"]=="0" && $params["tipo_list"]==2 && $params["tipo"]==1){ //listado de modal en todas las tiendas, envios de eu a mx
            $this->db->where("e.tipo",2);
        }else if($params["id_tienda"]!="0" && $params["tipo_list"]==2 && $params["tipo"]==1){ //listado de modal en todas las tiendas, envios de eu a mx
            $this->db->where("id_origen",$params["id_tienda"]);
            $this->db->where("e.tipo",2);
        }
        else if($params["id_tienda"]=="0" && $params["tipo_list"]==2 && $params["tipo"]==2){ //listado de modal en todas las tiendas, envios de mx a eu
            $this->db->where("e.tipo",1);
        }else if($params["id_tienda"]!="0" && $params["tipo_list"]==2 && $params["tipo"]==2){ //listado de modal en todas las tiendas, envios de mx a eu
            $this->db->where("id_origen",$params["id_tienda"]);
            $this->db->where("e.tipo",1);
        }

        if($params["fi"]!="" && $params["ff"]!=""){
            $this->db->where("e.reg between '{$params['fi']} 00:00:00' AND '{$params['ff']} 23:59:59' ");
        }
        if($params["semana"]!=0 && $params["clientetxt"]=="" && $params["tipo_list"]==1 && $params["fi"]=="" && $params["ff"]=="" && $params["num_paq"]!="SNAPLI" || 
            $params["semana"]!=0 && $params["clientetxt"]=="" && $params["tipo_list"]==1  && $params["fi"]=="" && $params["ff"]=="" && $params["num_paq"]=="SNAPLI" && $params["sea_tel"]=="SNAPLI" ||
            $params["semana"]!=0 && $params["clientetxt"]=="0" && $params["tipo_list"]==2 && $params["fi"]=="" && $params["ff"]==""&& $params["num_paq"]!="SNAPLI" || 
            $params["num_paq"]!="SNAPLI" && $params["sea_tel"]=="SNAPLI" && $params["semana"]!=0 
            || $params["tipo_list"]==2 && $params["fi"]=="" && $params["ff"]=="" && $params["semana"]!=0){ //cliente que no existe como registro 
            $this->db->where("e.semana",$params["semana"]);
        }
        $this->db->where("DATE_FORMAT(e.reg,'%Y')",date("Y"));
        $this->db->where("e.estatus",1);

        $var_tipo="";
        if($params["tipo"]!=""){ //listado normal - principal
            $var_tipo="and tipo=".$params["tipo"];
        }
        if($params["cliente"]!="" && $params["cliente"]!="0" && $params["tipocli"]==0){ //cliente registrado
            /*$this->db->where("e.id_cliente",$params['cliente']);
            $this->db->or_where("e.id_cliente_envia",$params['cliente']);*/
            //$this->db->join('clientes c', 'c.id=e.id_cliente'); //join al cliente que recibe para verificar numero telefonico
            $this->db->where("(e.id_cliente_envia=".$params['cliente']." or e.id_cliente = ".$params['cliente'].")");
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("concat(nom_recibe,' ',apellido_recibe)",trim($params["clientetxt"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("concat(nom_envia,' ',apellido_envia)",trim($params["clientetxt"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
        }else if($params["cliente"]!="" && $params["tipocli"]==1){ //cliente que no existe como registro 
            $this->db->like("concat(nom_recibe,' ',apellido_recibe)",$params["clientetxt"]);
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("concat(nom_envia,' ',apellido_envia)",$params["clientetxt"]);
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("nom_recibe",trim($params["clientetxt"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("apellido_recibe",trim($params["clientetxt"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }

            $explodeApe = explode(" ", $params["clientetxt"]);
            /*if(isset($explodeApe[0])){
                $this->db->or_like("apellido_recibe",$explodeApe[0]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }*/
            if(isset($explodeApe[1]) && !isset($explodeApe[2])){
                $this->db->or_like("apellido_recibe",$explodeApe[1]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
            if(isset($explodeApe[2]) && !isset($explodeApe[3])){
                $this->db->or_like("apellido_recibe",$explodeApe[1]." ".$explodeApe[2]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
            if(isset($explodeApe[3])){
                $this->db->or_like("apellido_recibe",$explodeApe[1]." ".$explodeApe[2]." ".$explodeApe[3]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }

            $this->db->or_like("nom_envia",trim($params["clientetxt"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("apellido_envia",trim($params["clientetxt"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }

            $explodeApe = explode(" ", $params["clientetxt"]);
            /*if(isset($explodeApe[0])){
                $this->db->or_like("apellido_envia",$explodeApe[0]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }*/
            if(isset($explodeApe[1]) && !isset($explodeApe[2])){
                $this->db->or_like("apellido_envia",$explodeApe[1]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
            if(isset($explodeApe[2]) && !isset($explodeApe[3])){
                $this->db->or_like("apellido_envia",$explodeApe[1]." ".$explodeApe[2]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
            if(isset($explodeApe[3])){
                $this->db->or_like("apellido_envia",$explodeApe[1]." ".$explodeApe[2]." ".$explodeApe[3]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }

            if($params["sea_tel"]!="SNAPLI"){
                $this->db->or_like("tel_recibe",trim($params["sea_tel"]));
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
                $this->db->or_like("tel_envia",trim($params["sea_tel"]));
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
        }

        if($params["tipo"]=="1" && $params["num_paq"]!="SNAPLI"){ //listado normal - principal -- de EU a MX --ok
            $this->db->like("concat(t1.letra,'',e.folio)",$params["num_paq"]);
            $this->db->where("tipo",$params["tipo"]);
        }

        if($params["num_paq"]!="SNAPLI" && $params["tipo_list"]==1){ //numero de paquete
            
            if($params["tipo"]=="1"){ //listado normal - principal -- de EU a MX --ok
                $this->db->like("concat(t1.letra,'',e.folio)",$params["num_paq"]);
                $this->db->where("tipo",$params["tipo"]);
            }
            if($params["tipo"]=="2"){ //listado normal - principal -- de MX a EU
                //$this->db->like("concat(t1.letra,'',e.folio)",$params["num_paq"]);
                //$this->db->like('IF(t1.letra REGEXP "^[0-9]+$",(t1.letra+e.folio),concat(t1.letra,"",e.folio))', $params["num_paq"]);
                $this->db->where('IF(t1.letra REGEXP "^[0-9]+$",(t1.letra+e.folio),concat(t1.letra,"",e.folio))="'.$params['num_paq'].'"');
                $this->db->where("tipo",$params["tipo"]);
            }
            //$this->db->or_like("e.tel_envia",trim($num_paq_tel));
            //$this->db->or_like("e.tel_recibe",trim($num_paq_tel));
        }
        if($params["sea_tel"]!="SNAPLI" && $params["num_paq"]=="SNAPLI" && $params["tipo_list"]==1){ //numero de paquete o numero telefonico
            $this->db->like("e.tel_envia",trim($params["sea_tel"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("e.tel_recibe",trim($params["sea_tel"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
        }else if($params["sea_tel"]!="SNAPLI" && $params["num_paq"]!="SNAPLI" && $params["tipo_list"]==1){ //numero de paquete o numero telefonico
            $this->db->or_like("e.tel_envia",trim($params["sea_tel"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("e.tel_recibe",trim($params["sea_tel"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
        }

        $this->db->group_by("e.id");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns_seach as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns_seach[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function total_envios($params){
        $columns = array( 
            0=>'e.id', 
            1=>"e.nom_recibe",
            2=>'e.apellido_recibe',
            3=>'e.calle_recibe',
            4=>'e.ciudad_recibe',
            5=>'e.edo_recibe',
            6=>'e.cp_recibe',
            7=>'e.tel_recibe',
            8=>'e.nom_envia',
            9=>'e.apellido_envia',
            10=>'e.calle_envia',
            11=>'e.ciudad_envia',
            12=>'e.edo_envia',
            13=>'e.cp_envia',
            14=>'e.tel_envia',
            15=>'t1.nombre as origen',
            16=>'t2.nombre as destino',
            17=>'e.folio',
            18=>'e.entregado',
            19=>'e.reg',
            20=>'concat(e.nom_recibe," ",e.apellido_recibe) as recibe',
            21=>'concat(e.nom_envia," ",e.apellido_envia) as envia',
            //22=>'ep.contenido',
            24=>'concat(u.nombre," ",u.apellidos) as user',
            25=>'e.tipo',
            36=>'e.semana',
            37=>'concat(t1.letra,"",e.folio) as mi_folio',
            38=>'concat(t2.letra,"",e.folio) as mi_folio2'
        );
        $columns_seach = array( 
            0=>'e.id', 
            1=>"e.nom_recibe",
            2=>'e.apellido_recibe',
            3=>'e.calle_recibe',
            4=>'e.ciudad_recibe',
            5=>'e.edo_recibe',
            6=>'e.cp_recibe',
            7=>'e.tel_recibe',
            8=>'e.nom_envia',
            9=>'e.apellido_envia',
            10=>'e.calle_envia',
            11=>'e.ciudad_envia',
            12=>'e.edo_envia',
            13=>'e.cp_envia',
            14=>'e.tel_envia',
            15=>'t1.nombre',
            16=>'t2.nombre',
            17=>'e.folio',
            18=>'e.entregado',
            19=>'e.reg',
            //20=>'ep.contenido',
            21=>'u.apellidos',
            22=>'u.nombre',
            23=>"DATE_FORMAT(reg,  '%d / %m / %Y %r')",
            24=>"DATE_FORMAT(fecha_entregado,  '%d / %m / %Y %r')",
            //25=>"e.comentarios",
            26=>"(select sum(total) from envio_paquete ep4 where ep4.id_envio=e.id)",
            27=>"(select sum(valor_est) from envio_paquete ep5 where ep5.id_envio=e.id)",
            28=>"IFNULL((select GROUP_CONCAT(concat(u.nombre, ': ', DATE_FORMAT(reg, '%d/%m/%Y %r'), ' - ', ce.comentarios) SEPARATOR '<br>') as coments from comentarios_envio ce join usuarios u on u.id=ce.id_usuario where ce.id_envio=e.id),'') ",
            36=>'e.semana',
            37=>'concat(t1.letra,"",e.folio)',
            40=>'concat(t2.letra,"",e.folio)',
            41=>'IF(t1.letra REGEXP "^[0-9]+$",(t1.letra+e.folio),concat(t1.letra,"",e.folio))',
        );
        
        $this->db->select('COUNT(*) as total');
        $this->db->from('envios e');
        $this->db->join('tienda t1', 't1.id=e.id_origen');
        $this->db->join('tienda t2', 't2.id=e.id_destino');
        //$this->db->join('envio_paquete ep', 'ep.id_envio=e.id');
        $this->db->join('usuarios u', 'u.id=e.id_usuario');
        if($params["estatus"]!=2){
            $this->db->where("entregado",$params["estatus"]);
        }
        if($params["tipo"]!="" && $params["tipo_list"]==1 /*&& $params["cliente"]==""*/ && $params["num_paq"]=="SNAPLI"){ //listado normal - principal
            $this->db->where("tipo",$params["tipo"]);
        }

        //log_message('error', 'id_tienda: '.$params["id_tienda"]);
        //log_message('error', 'tipo_list: '.$params["tipo_list"]);

        if($params["id_tienda"]!="0" && $params["tipo_list"]==1 && $params["cliente"]=="" && $params["num_paq"]=="SNAPLI"){ //listado normal - principal
            $this->db->where("id_origen",$params["id_tienda"]);
        }else if($params["id_tienda"]!="0" && $params["tipo_list"]==2){ //listado por sucursal de modal 
            $this->db->where("id_destino",$params["id_tienda"]);
        }
        else if($params["id_tienda"]=="0" && $params["tipo_list"]==2 && $params["tipo"]==1){ //listado de modal en todas las tiendas, envios de eu a mx
            $this->db->where("e.tipo",2);
        }else if($params["id_tienda"]!="0" && $params["tipo_list"]==2 && $params["tipo"]==1){ //listado de modal en todas las tiendas, envios de eu a mx
            $this->db->where("id_origen",$params["id_tienda"]);
            $this->db->where("e.tipo",2);
        }else if($params["id_tienda"]=="0" && $params["tipo_list"]==2 && $params["tipo"]==2){ // listado de modal en todas las tiendas, envios de mx a eu
            $this->db->where("e.tipo",1);
        }else if($params["id_tienda"]!="0" && $params["tipo_list"]==2 && $params["tipo"]==2){ // listado de modal en todas las tiendas, envios de mx a eu
            $this->db->where("id_origen",$params["id_tienda"]);
            $this->db->where("e.tipo",1);
        }
        if($params["fi"]!="" && $params["ff"]!=""){
            $this->db->where("e.reg between '{$params['fi']} 00:00:00' AND '{$params['ff']} 23:59:59' ");
        }
        //log_message('error', 'clientetxt: '.$params["clientetxt"]);
        /*log_message('error', 'tipo_list: '.$params["tipo_list"]);
        log_message('error', 'num_paq: '.$params["num_paq"]);
        log_message('error', 'sea_tel: '.$params["sea_tel"]);*/
        if($params["semana"]!=0 && $params["clientetxt"]=="" && $params["tipo_list"]==1 && $params["fi"]=="" && $params["ff"]=="" && $params["num_paq"]!="SNAPLI" || 
            $params["semana"]!=0 && $params["clientetxt"]=="" && $params["tipo_list"]==1  && $params["fi"]=="" && $params["ff"]=="" && $params["num_paq"]=="SNAPLI" && $params["sea_tel"]=="SNAPLI" ||
            $params["semana"]!=0 && $params["clientetxt"]=="0" && $params["tipo_list"]==2 && $params["fi"]=="" && $params["ff"]==""&& $params["num_paq"]!="SNAPLI" || 
            $params["num_paq"]!="SNAPLI" && $params["sea_tel"]=="SNAPLI" && $params["semana"]!=0 
            || $params["tipo_list"]==2 && $params["fi"]=="" && $params["ff"]=="" && $params["semana"]!=0){ //cliente que no existe como registro 
            $this->db->where("e.semana",$params["semana"]);
        }

        $this->db->where("DATE_FORMAT(e.reg,'%Y')",date("Y"));
        $this->db->where("e.estatus",1);

        /*if($params["cliente"]!=""){
            $this->db->like("concat(nom_recibe,' ',apellido_recibe)",$params["cliente"]);
            $this->db->or_like("concat(nom_envia,' ',apellido_envia)",$params["cliente"]);
        }*/

        if($params["tipo"]=="1" && $params["num_paq"]!="SNAPLI"){ //listado normal - principal -- de EU a MX --ok
            $this->db->like("concat(t1.letra,'',e.folio)",$params["num_paq"]);
            $this->db->where("tipo",$params["tipo"]);
        }

        if($params["cliente"]!="" && $params["cliente"]!="0" && $params["tipocli"]==0){ //cliente registrado
            /*$this->db->where("e.id_cliente",$params['cliente']);
            $this->db->or_where("e.id_cliente_envia",$params['cliente']);*/
            $this->db->where("(e.id_cliente_envia=".$params['cliente']." or e.id_cliente = ".$params['cliente'].")");
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("concat(nom_recibe,' ',apellido_recibe)",trim($params["clientetxt"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("concat(nom_envia,' ',apellido_envia)",trim($params["clientetxt"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
        }else if($params["cliente"]!="" && $params["tipocli"]==1){ //cliente que no existe como registro 
            $this->db->like("concat(nom_recibe,' ',apellido_recibe)",$params["clientetxt"]);
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("concat(nom_envia,' ',apellido_envia)",$params["clientetxt"]);
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("nom_recibe",trim($params["clientetxt"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("apellido_recibe",trim($params["clientetxt"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }

            $explodeApe = explode(" ", $params["clientetxt"]);
            /*if(isset($explodeApe[0])){
                $this->db->or_like("apellido_recibe",$explodeApe[0]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }*/
            if(isset($explodeApe[1]) && !isset($explodeApe[2])){
                $this->db->or_like("apellido_recibe",$explodeApe[1]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
            if(isset($explodeApe[2]) && !isset($explodeApe[3])){
                $this->db->or_like("apellido_recibe",$explodeApe[1]." ".$explodeApe[2]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
            if(isset($explodeApe[3])){
                $this->db->or_like("apellido_recibe",$explodeApe[1]." ".$explodeApe[2]." ".$explodeApe[3]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }

            $this->db->or_like("nom_envia",trim($params["clientetxt"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("apellido_envia",trim($params["clientetxt"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }

            $explodeApe = explode(" ", $params["clientetxt"]);
            /*if(isset($explodeApe[0])){
                $this->db->or_like("apellido_envia",$explodeApe[0]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }*/
            if(isset($explodeApe[1]) && !isset($explodeApe[2])){
                $this->db->or_like("apellido_envia",$explodeApe[1]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
            if(isset($explodeApe[2]) && !isset($explodeApe[3])){
                $this->db->or_like("apellido_envia",$explodeApe[1]." ".$explodeApe[2]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
            if(isset($explodeApe[3])){
                $this->db->or_like("apellido_envia",$explodeApe[1]." ".$explodeApe[2]." ".$explodeApe[3]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }

            if($params["sea_tel"]!="SNAPLI"){
                $this->db->or_like("tel_recibe",trim($params["sea_tel"]));
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
                $this->db->or_like("tel_envia",trim($params["sea_tel"]));
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
        }

        if($params["num_paq"]!="SNAPLI" && $params["tipo_list"]==1){ //numero de paquete
            if($params["tipo"]=="1"){ //listado normal - principal -- de EU a MX
                $this->db->like("concat(t1.letra,'',e.folio)",$params["num_paq"]);
                $this->db->where("tipo",$params["tipo"]);
            }
            if($params["tipo"]=="2"){ //listado normal - principal -- de MX a EU
                //$this->db->like("concat(t1.letra,'',e.folio)",$params["num_paq"]);
                //$this->db->like('IF(t1.letra REGEXP "^[0-9]+$",(t1.letra+e.folio),concat(t1.letra,"",e.folio))', $params["num_paq"]);
                $this->db->where('IF(t1.letra REGEXP "^[0-9]+$",(t1.letra+e.folio),concat(t1.letra,"",e.folio))="'.$params['num_paq'].'"');
                $this->db->where("tipo",$params["tipo"]);
            }
            //$this->db->or_like("e.tel_envia",trim($num_paq_tel));
            //$this->db->or_like("e.tel_recibe",trim($num_paq_tel));
        }
        if($params["sea_tel"]!="SNAPLI" && $params["num_paq"]=="SNAPLI" && $params["tipo_list"]==1){ //numero de paquete o numero telefonico
            $this->db->like("e.tel_envia",trim($params["sea_tel"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("e.tel_recibe",trim($params["sea_tel"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
        }else if($params["sea_tel"]!="SNAPLI" && $params["sea_tel"]!="SNAPLI" && $params["tipo_list"]==1){ //numero de paquete o numero telefonico
            $this->db->or_like("e.tel_envia",trim($params["sea_tel"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
            $this->db->or_like("e.tel_recibe",trim($params["sea_tel"]));
            if($params["tipo"]!=""){ //listado normal - principal
               $this->db->where("tipo",$params["tipo"]);
            }
        }

        $this->db->group_by("e.id");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns_seach as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();
        //return $this->db->count_all_results();

        /*if(isset($query->row()->total)){
            //return $query->row()->total;
            return $this->db->count_all_results();
        }
        else{
            return 0;
        }*/
        $tot=0;
        foreach($query->result() as $t){
            $tot++;
        }
        return $tot;
    }

    function getDatosEnvios($cliente,$clitxt,$tipocli,$estatus,$tipo,$id_tienda,$fi,$ff,$semana,$num_paq,$sea_tel){
        $columns = array( 
            0=>'e.id', 
            1=>"e.nom_recibe",
            2=>'e.apellido_recibe',
            3=>'e.calle_recibe',
            4=>'e.ciudad_recibe',
            5=>'e.edo_recibe',
            6=>'e.cp_recibe',
            7=>'e.tel_recibe',
            8=>'e.nom_envia',
            9=>'e.apellido_envia',
            10=>'e.calle_envia',
            11=>'e.ciudad_envia',
            12=>'e.edo_envia',
            13=>'e.cp_envia',
            14=>'e.tel_envia',
            15=>'t1.nombre as origen',
            16=>'t2.nombre as destino',
            17=>'e.folio',
            18=>'e.entregado',
            19=>'e.reg',
            20=>'concat(e.nom_recibe," ",e.apellido_recibe) as recibe',
            21=>'concat(e.nom_envia," ",e.apellido_envia) as envia',
            //22=>'ep.contenido',
            23=>'u.usuario',
            24=>'concat(u.nombre," ",u.apellidos) as user',
            25=>"(SELECT GROUP_CONCAT('', contenido SEPARATOR '<br>') from envio_paquete where id_envio=e.id) as contenido",
            26=>"(SELECT count(id) from envio_paquete where id_envio=e.id) as tot_paq",
            27=>"(SELECT sum(libras) from envio_paquete where id_envio=e.id) as tot_libras",
            //28=>"(SELECT libras from envio_paquete where id_envio=e.id) as libras",
            29=>"DATE_FORMAT(fecha_entregado,  '%d/%m/%Y %r') AS fecha_entregado",
            30=>"DATE_FORMAT(e.reg,  '%d/%m/%Y %r') AS fecha_reg",
            31=>"e.semana",
            32=>"e.tipo",
            33=>"e.id_origen",
            34=>'t1.letra as letra',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('envios e');
        $this->db->join('tienda t1', 't1.id=e.id_origen');
        $this->db->join('tienda t2', 't2.id=e.id_destino');
        //$this->db->join('envio_paquete ep', 'ep.id_envio=e.id');
        $this->db->join('usuarios u', 'u.id=e.id_usuario');

        if ($cliente > 0){ 
            $this->db->join("clientes c", "c.id = $cliente"); 
        } 

        if(/*$clitxt!="0" &&*/ $estatus!=2){ //diferente de todos
            $this->db->where("entregado",$estatus);
        }
        /*log_message('error', 'clitxt: '.$clitxt);
        log_message('error', 'tipo: '.$tipo);
        log_message('error', 'num_paq: '.$num_paq);*/

        //FALTA VALIDAR TIPO, SEMANAS, BUSQUEDAS 
        if(/*$clitxt!="0" && */$tipo!="" && $num_paq=="SNAPLI"){
            $this->db->where("tipo",$tipo);
        }

        if($clitxt=="0" && $id_tienda!="0" && $num_paq=="SNAPLI"){
            $this->db->where("id_origen",$id_tienda);
        }
        if($fi!="0" && $ff!="0"){
            $this->db->where("e.reg between '{$fi} 00:00:00' AND '{$ff} 23:59:59' ");
        }
        /*log_message('error', 'clitxt: '.$clitxt);
        log_message('error', 'id_tienda: '.$id_tienda);
        log_message('error', 'num_paq: '.$num_paq);*/

        /*if($clitxt=="0" && $fi=="0" && $ff=="0" && $num_paq!="SNAPLI" && $semana!="0" && $sea_tel=="SNAPLI" || $clitxt=="0" && $fi=="0" && $ff=="0" && $num_paq!="SNAPLI" && $semana!="0" && $sea_tel!="SNAPLI"
            || $clitxt=="0" && $num_paq=="SNAPLI" && $semana!="0" && $sea_tel=="SNAPLI"){ //cliente que no existe como registro 
            $this->db->where("e.semana",$semana);
        }*/
        if($semana!=0 && $clitxt=="0" && $fi=="0" && $ff=="0" && $num_paq!="SNAPLI" || 
            $semana!=0 && $clitxt=="0"  && $fi=="0" && $ff=="0" && $num_paq=="SNAPLI" && $sea_tel=="SNAPLI" ||
            $semana!=0 && $clitxt=="0" && $fi=="0" && $ff=="0" && $num_paq!="SNAPLI" || 
            $num_paq!="SNAPLI" && $sea_tel=="SNAPLI" && $semana!=0 
            || $tipo==2 && $fi=="" && $ff=="" && $semana!=0){ //cliente que no existe como registro 
            $this->db->where("e.semana",$semana);
        }

        $this->db->where("DATE_FORMAT(e.reg,'%Y')",date("Y"));
        $this->db->where("e.estatus",1);

        /*if($cliente!="0"){
            $this->db->like("concat(nom_recibe,' ',apellido_recibe)",$cliente);
            $this->db->or_like("concat(nom_envia,' ',apellido_envia)",$cliente);
        }*/
        if($cliente!="0" && $tipocli==0){ //cliente registrado
            /*$this->db->where("e.id_cliente",$params['cliente']);
            $this->db->or_where("e.id_cliente_envia",$params['cliente']);*/
            $this->db->where("(e.id_cliente_envia=".$cliente." or e.id_cliente = ".$cliente.")");
            if($clitxt!="0"){
                $this->db->or_like("concat(nom_recibe,' ',apellido_recibe)",$clitxt);
                if($tipo!=""){ //listado normal - principal
                   $this->db->where("tipo",$tipo);
                }
                $this->db->or_like("concat(nom_envia,' ',apellido_envia)",$clitxt);
                if($tipo!=""){ //listado normal - principal
                   $this->db->where("tipo",$tipo);
                }
            }
        }else if($cliente!="0" && $tipocli==1){ //cliente que no existe como registro 
            $this->db->like("concat(nom_recibe,' ',apellido_recibe)",$cliente);
            if($tipo!=""){ //listado normal - principal
               $this->db->where("tipo",$tipo);
            }
            $this->db->or_like("concat(nom_envia,' ',apellido_envia)",$cliente);
            if($tipo!=""){ //listado normal - principal
               $this->db->where("tipo",$tipo);
            }
            if($clitxt!="0"){
                $this->db->or_like("nom_recibe",$clitxt);
                if($tipo!=""){ //listado normal - principal
                   $this->db->where("tipo",$tipo);
                }
                $this->db->or_like("apellido_recibe",$clitxt);
                if($tipo!=""){ //listado normal - principal
                   $this->db->where("tipo",$tipo);
                }

                $explodeApe = explode(" ", $clitxt);
                /*if(isset($explodeApe[0])){
                    $this->db->or_like("apellido_recibe",$explodeApe[0]);
                    if($tipo!=""){ //listado normal - principal
                       $this->db->where("tipo",$tipo);
                    }
                }*/
                if(isset($explodeApe[1]) && !isset($explodeApe[2])){
                    $this->db->or_like("apellido_recibe",$explodeApe[1]);
                    if($tipo!=""){ //listado normal - principal
                       $this->db->where("tipo",$tipo);
                    }
                }
                if(isset($explodeApe[2]) && !isset($explodeApe[3])){
                    $this->db->or_like("apellido_recibe",$explodeApe[1]." ".$explodeApe[2]);
                    if($tipo!=""){ //listado normal - principal
                       $this->db->where("tipo",$tipo);
                    }
                }
                if(isset($explodeApe[3])){
                    $this->db->or_like("apellido_recibe",$explodeApe[1]." ".$explodeApe[2]." ".$explodeApe[3]);
                    if($tipo!=""){ //listado normal - principal
                       $this->db->where("tipo",$tipo);
                    }
                }

                $this->db->or_like("nom_envia",$clitxt);
                if($tipo!=""){ //listado normal - principal
                   $this->db->where("tipo",$tipo);
                }
                $this->db->or_like("apellido_envia",$clitxt);
                if($tipo!=""){ //listado normal - principal
                   $this->db->where("tipo",$tipo);
                }

                $explodeApe = explode(" ", $clitxt);
                /*if(isset($explodeApe[0])){
                    $this->db->or_like("apellido_envia",$explodeApe[0]);
                    if($tipo!=""){ //listado normal - principal
                       $this->db->where("tipo",$tipo);
                    }
                }*/
                if(isset($explodeApe[1]) && !isset($explodeApe[2])){
                    $this->db->or_like("apellido_envia",$explodeApe[1]);
                    if($tipo!=""){ //listado normal - principal
                       $this->db->where("tipo",$tipo);
                    }
                }
                if(isset($explodeApe[2]) && !isset($explodeApe[3])){
                    $this->db->or_like("apellido_envia",$explodeApe[1]." ".$explodeApe[2]);
                    if($tipo!=""){ //listado normal - principal
                       $this->db->where("tipo",$tipo);
                    }
                }
                if(isset($explodeApe[3])){
                    $this->db->or_like("apellido_envia",$explodeApe[1]." ".$explodeApe[2]." ".$explodeApe[3]);
                    if($tipo!=""){ //listado normal - principal
                       $this->db->where("tipo",$tipo);
                    }
                }

                if($sea_tel!="SNAPLI"){
                    $this->db->or_like("tel_recibe",trim($sea_tel));
                    if($tipo!=""){ //listado normal - principal
                       $this->db->where("tipo",$tipo);
                    }
                    $this->db->or_like("tel_envia",trim($sea_tel));
                    if($tipo!=""){ //listado normal - principal
                       $this->db->where("tipo",$tipo);
                    }
                }
            }
        }

        if($num_paq!="SNAPLI"){ //numero de paquete
            /*$this->db->like("concat(t1.letra,'',e.folio)",$num_paq);
            if($tipo!=""){ //listado normal - principal
               $this->db->where("tipo",$tipo);
            }*/
            if($tipo=="1"){ //listado normal - principal -- de EU a MX
                $this->db->like("concat(t1.letra,'',e.folio)",$num_paq);
                $this->db->where("tipo",$tipo);
            }
            if($tipo=="2"){ //listado normal - principal -- de MX a EU
                //$this->db->like('IF(t1.letra REGEXP "^[0-9]+$",(t1.letra+e.folio),concat(t1.letra,"",e.folio))', $num_paq);
                $this->db->where('IF(t1.letra REGEXP "^[0-9]+$",(t1.letra+e.folio),concat(t1.letra,"",e.folio))="'.$num_paq.'"');
                $this->db->where("tipo",$tipo);
            }
            //$this->db->or_like("e.tel_envia",trim($num_paq_tel));
            //$this->db->or_like("e.tel_recibe",trim($num_paq_tel));
        }
        if($sea_tel!="SNAPLI" && $num_paq=="SNAPLI"){ //numero de paquete o numero telefonico
            $this->db->like("e.tel_envia",trim($sea_tel));
            if($tipo!=""){ //listado normal - principal
               $this->db->where("tipo",$tipo);
            }
            $this->db->or_like("e.tel_recibe",trim($sea_tel));
            if($tipo!=""){ //listado normal - principal
               $this->db->where("tipo",$tipo);
            }
        }else if($sea_tel!="SNAPLI" && $num_paq!="SNAPLI"){ //numero de paquete o numero telefonico
            $this->db->or_like("e.tel_envia",trim($sea_tel));
            if($tipo!=""){ //listado normal - principal
               $this->db->where("tipo",$tipo);
            }
            $this->db->or_like("e.tel_recibe",trim($sea_tel));
            if($tipo!=""){ //listado normal - principal
               $this->db->where("tipo",$tipo);
            }
        }

        $this->db->order_by("t1.nombre","ASC");
        $this->db->group_by("e.id");
        $query=$this->db->get();
        return $query->result();
    }

    public function getEnviosHistorial($id_cli,$txt){
        $this->db->select("e.*, t.nombre as destino, t2.letra");
        $this->db->from("envios e");
        $this->db->join('tienda t', 't.id=e.id_destino');
        $this->db->join('tienda t2', 't2.id=e.id_origen');
        //$this->db->where($where);
        $this->db->where("id_cliente_envia=$id_cli and e.id IN (SELECT MAX(id) FROM envios GROUP BY id_cliente) ");
        $this->db->or_where("concat(e.nom_envia,' ',e.apellido_envia) like '%".$txt."%' ");
        $this->db->where("e.estatus",1);
        $this->db->order_by("e.id","desc");
        $query=$this->db->get();
        return $query->result();
    }

    public function getCorteCaja($params){
        $this->db->select("concat(t1.letra,'',e.folio) as mi_folio, e.tipo, e.folio, t1.letra, e.semana,
                        e.id, e.metodo, concat(e.nom_envia,' ',e.apellido_envia) as envia, t.nombre as destino, 
                        (select count(*) from envio_paquete ep where ep.id_envio=e.id) as paquetes,
                        (select sum(total) from envio_paquete ep2 where ep2.id_envio=e.id) as stotal,
                        (select sum(libras) from envio_paquete ep3 where ep3.id_envio=e.id) as slibras,e.efectivo_cantidad,e.tarjeta_cantidad");
        $this->db->from("envios e");
        $this->db->join('tienda t1', 't1.id=e.id_origen');
        $this->db->join('tienda t', 't.id=e.id_destino');
        $this->db->where("e.estatus",1);

        if($params["id_tienda"]!="0" && $params["tipo"]==1){ //EU - MX
            $this->db->where("e.id_origen",$params["id_tienda"]); 
        }if($params["id_tienda"]=="0" && $params["tipo"]==1){
           $this->db->order_by("e.id_origen","DESC");  
        }

        if($params["id_tienda"]!="0" && $params["tipo"]==2){ //MX - EU
            $this->db->where("e.id_destino",$params["id_tienda"]); 
        }if($params["id_tienda"]=="0" && $params["tipo"]==2){
           $this->db->order_by("e.id_destino","DESC");  
        }
        $this->db->where("e.reg between '{$params['fi']} 00:00:00' AND '{$params['ff']} 23:59:59' ");
        $query=$this->db->get();
        return $query->result();
    }

    public function getTotalesCorteCaja($params){
        /*$this->db->select("e.id_destino,IFNULL((select sum(total) from envio_paquete ep where ep.id_envio=e.id and metodo=1),0) as stotal,
                        IFNULL((select sum(total) from envio_paquete ep2 where ep2.id_envio=e.id and metodo=2),0) as stotaltc,
                        IFNULL((select sum(total) from envio_paquete ep3 where ep3.id_envio=e.id and metodo=3),0) as stotaltd");*/
        $this->db->select("e.id_destino,SUM(e.efectivo_cantidad) as stotal,
                        SUM(e.tarjeta_cantidad) as stotaltc,
                        IFNULL((select sum(total) from envio_paquete ep3 where ep3.id_envio=e.id and metodo=3),0) as stotaltd");
        $this->db->from("envios e");

        if($params["id_tienda"]!="0" && $params["tipo"]==1){ //EU - MX
            $this->db->where("e.id_origen",$params["id_tienda"]); 
        }if($params["id_tienda"]=="0" && $params["tipo"]==1){
           $this->db->order_by("e.id_origen","DESC");  
        }

        if($params["id_tienda"]!="0" && $params["tipo"]==2){ //MX - EU
            $this->db->where("e.id_destino",$params["id_tienda"]); 
        }if($params["id_tienda"]=="0" && $params["tipo"]==2){
           $this->db->order_by("e.id_destino","DESC");  
        }

        $this->db->where("e.estatus",1);
        $this->db->where("e.reg between '{$params['fi']} 00:00:00' AND '{$params['ff']} 23:59:59' ");
        $query=$this->db->get();
        return $query->result();
    }

    public function getTotalMetodo($params,$metodo){
        //$this->db->select("e.id_destino, IFNULL(sum(total), 0) as stotal, t.nombre as tienda");
        $this->db->select("e.id_destino, SUM(e.efectivo_cantidad) as stotal,SUM(e.tarjeta_cantidad) as ttotal, t.nombre as tienda");
        $this->db->from("envios e");
        //$this->db->join("envio_paquete ep","ep.id_envio=e.id");

        if($params["tipo"]==1){ //EU - MX
            $this->db->join('tienda t', 't.id=e.id_origen');
            $this->db->group_by("e.id_origen");
        }

        if($params["tipo"]==2){ //MX - EU
            $this->db->join('tienda t', 't.id=e.id_destino');
            $this->db->group_by("e.id_destino");  
        }
       
        $this->db->where("e.estatus",1);
        $this->db->where("e.reg between '{$params['fi']} 00:00:00' AND '{$params['ff']} 23:59:59' ");
        //$this->db->where($metodo);
        
        $this->db->order_by("t.nombre","ASC");
        $query=$this->db->get();
        return $query->result();
    }
    public function getTotalMetodoTienda($params,$metodo,$tienda){
        $this->db->select("e.id_destino, IFNULL(sum(total), 0) as stotaltc, t.nombre as tienda");
        $this->db->from("envios e");
        $this->db->join("envio_paquete ep","ep.id_envio=e.id");
        $this->db->join('tienda t', 't.id=e.id_destino');
        $this->db->where("e.estatus",1);
        $this->db->where($tienda);
        $this->db->where("e.reg between '{$params['fi']} 00:00:00' AND '{$params['ff']} 23:59:59' ");
        $this->db->where($metodo);

        $this->db->order_by("t.nombre","ASC");
        $query=$this->db->get();
        return $query->result();
    }

    /*public function getDatosInicio($fecha){
        $this->db->select("count(e.id) as total_envios, 
            IFNULL((select sum(total) from envio_paquete ep where ep.id=e.id group by ep.id_envio),0) as total_dinero,
            IFNULL((select count(e.id) from envios en where en.entregado=1 and en.id=e.id),0) as total_ent, 
            IFNULL((select count(e.id) from envios en where en.entregado=0 and en.id=e.id),0) as total_no_ent");
        $this->db->from("envios e");
        //$this->db->join('envio_paquete ep', 'ep.id_envio=e.id');
        $this->db->where("e.estatus",1);
        $this->db->where("e.reg between '{$fecha} 00:00:00' AND '{$fecha} 23:59:59' ");
        //$this->db->group_by("e.id");
        $query=$this->db->get();
        return $query->row();
    }*/

    public function getDatosInicio($fecha){
        $this->db->select("sum(total) as total_dinero,
            IFNULL((select count(e.id) from envios en where en.entregado=1 and en.id=e.id),0) as total_ent/*, 
            IFNULL((select count(e.id) from envios en where en.entregado=0 and en.id=e.id),0) as total_no_ent*/");
        $this->db->from("envios e");
        $this->db->join('envio_paquete ep', 'ep.id_envio=e.id');
        $this->db->where("e.estatus",1);
        $this->db->where("e.reg between '{$fecha} 00:00:00' AND '{$fecha} 23:59:59' ");
        $query=$this->db->get();
        return $query->row();
    }

    public function getDatosInicioTotal($fecha){
        $this->db->select("count(e.id) as total_envios,
            IFNULL((select count(e2.id) from envios e2 where e2.entregado=1 and e2.estatus=1 and e2.reg between '{$fecha} 00:00:00' AND '{$fecha} 23:59:59'),0) as total_ent, 
            IFNULL((select count(e3.id) from envios e3 where e3.entregado=0 and e3.estatus=1 and e3.reg between '{$fecha} 00:00:00' AND '{$fecha} 23:59:59'),0) as total_no_ent");
        $this->db->from("envios e");
        $this->db->where("e.estatus",1);
        $this->db->where("e.reg between '{$fecha} 00:00:00' AND '{$fecha} 23:59:59' ");
        $query=$this->db->get();
        return $query->row();
    }

    function getEnviosWeek($params){
        $columns = array( 
            0=>'e.id', 
            1=>"e.nom_recibe",
            2=>'e.apellido_recibe',
            3=>'e.calle_recibe',
            4=>'e.ciudad_recibe',
            5=>'e.edo_recibe',
            6=>'e.cp_recibe',
            7=>'e.tel_recibe',
            8=>'e.nom_envia',
            9=>'e.apellido_envia',
            10=>'e.calle_envia',
            11=>'e.ciudad_envia',
            12=>'e.edo_envia',
            13=>'e.cp_envia',
            14=>'e.tel_envia',
            15=>'t1.nombre as origen',
            16=>'t2.nombre as destino',
            17=>'e.folio',
            18=>'e.entregado',
            19=>'e.reg',
            20=>'concat(e.nom_recibe," ",e.apellido_recibe) as recibe',
            21=>'concat(e.nom_envia," ",e.apellido_envia) as envia',
            //22=>'ep.contenido',
            23=>'u.usuario',
            24=>'concat(u.nombre," ",u.apellidos) as user',
            25=>"(select count(*) from envio_paquete ep where ep.id_envio=e.id) as paquetes",
            26=>"(select GROUP_CONCAT(contenido SEPARATOR '<br>') as contenido from envio_paquete ep2 where ep2.id_envio=e.id) as contenido, ",
            27=>"FORMAT((select sum(libras) from envio_paquete ep3 where ep3.id_envio=e.id),2) as slibras",
            28=>",DATE_FORMAT(e.reg,  '%d / %m / %Y %r') AS fecha_reg",
            29=>",DATE_FORMAT(fecha_entregado,  '%d / %m / %Y %r') AS fecha_entrega",
            30=>"e.comentarios",
            31=>"(select sum(total) from envio_paquete ep4 where ep4.id_envio=e.id) as stotal",
            32=>"(select sum(valor_est) from envio_paquete ep5 where ep5.id_envio=e.id) as stotalve",
            33=>"e.id_origen",
            34=>'t1.letra as letra',
            35=>'e.tipo',
            36=>"e.semana"
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('envios e');
        $this->db->join('tienda t1', 't1.id=e.id_origen');
        $this->db->join('tienda t2', 't2.id=e.id_destino');
        //$this->db->join('envio_paquete ep', 'ep.id_envio=e.id');
        $this->db->join('usuarios u', 'u.id=e.id_usuario');
        if($params["estatus"]!=2){
            $this->db->where("entregado",$params["estatus"]);
        }
        
        if($params["tipo"]!="" && $params["tipo_list"]==1 /*&& $params["cliente"]==""*/){ //listado normal - principal
            $this->db->where("tipo",$params["tipo"]);
        }

        if($params["id_tienda"]!="0" && $params["tipo_list"]==1 /*&& $params["cliente"]==""*/){ //listado normal - principal
            $this->db->where("id_origen",$params["id_tienda"]);
        }else if($params["id_tienda"]!="0" && $params["tipo_list"]==2){ //listado por sucursal de modal 
            $this->db->where("id_destino",$params["id_tienda"]);
        }
        else if($params["id_tienda"]=="0" && $params["tipo_list"]==2 && $params["tipo"]==1){ //listado de modal en todas las tiendas, envios de eu a mx
            $this->db->where("e.tipo",2);
        }else if($params["id_tienda"]!="0" && $params["tipo_list"]==2 && $params["tipo"]==1){ //listado de modal en todas las tiendas, envios de eu a mx
            $this->db->where("id_origen",$params["id_tienda"]);
            $this->db->where("e.tipo",2);
        }else if($params["id_tienda"]=="0" && $params["tipo_list"]==2 && $params["tipo"]==2){ // listado de modal en todas las tiendas, envios de mx a eu
            $this->db->where("e.tipo",2);
        }else if($params["id_tienda"]!="0" && $params["tipo_list"]==2 && $params["tipo"]==2){ // listado de modal en todas las tiendas, envios de mx a eu
            $this->db->where("id_origen",$params["id_tienda"]);
            $this->db->where("e.tipo",2);
        }

        if($params["fi"]!="0" && $params["ff"]!="0"){
            $this->db->where("e.reg between '{$params['fi']} 00:00:00' AND '{$params['ff']} 23:59:59' ");
        }
        //log_message('error', 'clientetxt: '.$params["clientetxt"]);
        //log_message('error', 'id: '.$params["tipo_list"]);
        $this->db->where("DATE_FORMAT(e.reg,'%Y')",date("Y"));
        if($params["clientetxt"]=="0" && $params["tipo_list"]==2 && $params["semana"]!="0" || $params["clientetxt"]=="0" && $params["tipo_list"]==2 && $params["semana"]!="0"){ //cliente que no existe como registro 
            $this->db->where("e.semana",$params["semana"]);
        }
        $this->db->where("e.estatus",1);

        if($params["cliente"]!="" && $params["cliente"]!="0" && $params["tipocli"]==0){ //cliente registrado
            /*$this->db->where("e.id_cliente",$params['cliente']);
            $this->db->or_where("e.id_cliente_envia",$params['cliente']);*/
            //$this->db->join('clientes c', 'c.id=e.id_cliente'); //join al cliente que recibe para verificar numero telefonico
            $this->db->where("(e.id_cliente_envia=".$params['cliente']." or e.id_cliente = ".$params['cliente'].")");
            if($params["clientetxt"]!="0"){
                $this->db->or_like("concat(nom_recibe,' ',apellido_recibe)",trim($params["clientetxt"]));
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
                $this->db->or_like("concat(nom_envia,' ',apellido_envia)",trim($params["clientetxt"]));
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
        }else if($params["cliente"]!="" && $params["tipocli"]==1){ //cliente que no existe como registro 
            if($params["clientetxt"]!="0"){
                $this->db->like("concat(nom_recibe,' ',apellido_recibe)",$params["cliente"]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
                $this->db->or_like("concat(nom_envia,' ',apellido_envia)",$params["cliente"]);
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
            if($params["clientetxt"]!="0"){
                $this->db->or_like("nom_recibe",trim($params["clientetxt"]));
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
                $this->db->or_like("apellido_recibe",trim($params["clientetxt"]));
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
                $this->db->or_like("nom_envia",trim($params["clientetxt"]));
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
                $this->db->or_like("apellido_envia",trim($params["clientetxt"]));
                if($params["tipo"]!=""){ //listado normal - principal
                   $this->db->where("tipo",$params["tipo"]);
                }
            }
        }

        $this->db->group_by("e.id");
        //$this->db->order_by("e.id_origen");
        $this->db->order_by("e.id");
        $query=$this->db->get();
        return $query->result();
    }

    function editaDireccion($id,$type,$element,$field,$id_cliente){
        $this->db->set(array($type => $id_cliente, $field => $element));
        $this->db->where('id', $id);
        $this->db->update('envios');
    }

    public function getDataClienteEnvio($fields,$id_cliente,$type){
        $this->db->select($fields);
        $this->db->from("envios");
        $this->db->where($type,$id_cliente);
        $this->db->order_by("id","desc");
        $this->db->limit(1);
        $query=$this->db->get();
        return $query->row();
    }

    public function getTotListEnvios($id,$sem,$anio,$f1,$f2){
        $this->db->select("count(1) as tot_env");
        $this->db->from("envios");
        $this->db->where("id_destino",$id); //cuenta los envios que llegarán a la sucursal elegida (propia por ejemplo)
        //$this->db->where("id_origen",$id);//
        $this->db->where("estatus",1);
        
        if($f1!="" && $f2!=""){
            $this->db->where("reg between '{$f1} 00:00:00' AND '{$f2} 23:59:59' ");
        }else{
            if($sem>0){
                $this->db->where("semana",$sem);
            }
        }
        
        //$this->db->where("DATE_FORMAT(reg,'%Y')",$anio);
        $query=$this->db->get();
        return $query->row();
    }
}  