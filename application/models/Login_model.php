<?php
class Login_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function login($usuario){
        $strq = "SELECT usu.id,usu.usuario, usu.id_perfil, usu.contrasenia, usu.id_tienda, IFNULL(t.lugar,'0') as tipot, t.nombre as nom_tie
                FROM usuarios as usu 
                left join tienda t on t.id=usu.id_tienda
                where usu.Usuario ='".$usuario."'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getMenus($perfil)
    {
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon 
                from menu as men, menu_sub as mens, perfiles_detalles as perfd 
                where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.id_perfil='$perfil'";
        $query = $this->db->query($strq);
        return $query->result();
    } 

    /*function submenus($perfil,$menu){
        $strq ="SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.id_perfil='$perfil' and menus.MenuId='$menu' 
            group by MenusubId
            order by menus.Nombre asc";
        $query = $this->db->query($strq);
        return $query->result();
    }*/

    function submenus($user,$menu){
        $strq ="SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
            from menu_sub as menus, perfiles_detalles as perfd 

            WHERE perfd.MenusubId=menus.MenusubId and perfd.id_usuario='$user' and menus.MenuId='$menu'
            group by MenusubId
            order by menus.Nombre asc";
        $query = $this->db->query($strq);
        return $query->result();
    }
    function get_record($table,$col,$id){
        $sql = "SELECT * FROM $table WHERE $col=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE id_perfil=$perfil AND MenusubId=$modulo";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }

}