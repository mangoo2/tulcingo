$(document).ready(function () {
    //$('#optgroup').multiSelect({ selectableOptgroup: true });

    $("#id_tienda").select2({ sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)) });
    
    $('#form_usus').validate({
        rules: {
            nombre: "required",
            apellidos: "required",
            id_tienda: "required",
            Usuario: "required",
            id_perfil: "required",
            contrasenia: {
                required: true,
                minlength: 6
            },
            contrasenia2: {
              equalTo: "#contrasenia"
            }
        },
        messages: {
            nombre: "Ingrese nombre ",
            apellidos: "Ingrese apellido(s)",
            id_tienda: "Seleccione a que tienda pertenece",
            Usuario: "Ingrese un usuario",
            id_perfil: "Ingrese un correo electrónico",
            contrasenia: {
                required: "Ingrese la contraseña de usuario",
                minlength: "La contraseña debe tener al menos 6 caracteres"
            },
            contrasenia2: {
              equalTo: "Las contraseñas deben de coincidir"
            }
        },
        submitHandler: function (form) {
            $.ajax({
                 type: "POST",
                 url: base_url+"Usuarios/submit",
                 data: $(form).serialize(),
                 beforeSend: function(){
                    $("#btn_submit").attr("disabled",true);
                 },
                 success: function (result) {
                    //console.log(result);
                    toastr.success('Se han realizado los cambios correctamente', 'Éxito');
                    setTimeout(function () { window.location.href = base_url+"Usuarios/" }, 1500);
                 }
            });
            return false; // required to block normal submit for ajax
         },
        errorPlacement: function(label, element) {
          label.addClass('mt-2 text-danger');
          label.insertAfter(element);
        },
        highlight: function(element, errorClass) {
          $(element).parent().addClass('has-danger')
          $(element).addClass('form-control-danger')
        }
    });

    $("#usuario").on("change",function(){
        if($("#usuario").val()!=""){
            $.ajax({
                type: "POST",
                url: base_url+"Usuarios/verificaUser",
                data: { user:$("#usuario").val() },
                success: function (result) {
                    //console.log(result);
                    if(result!=""){
                        toastr.warning('Utilice otro usuario', 'Álerta');
                        $("#usuario").val("");
                    }
                }
            });
        }
    });
});