
$(document).ready(function () {
    $("#lugar").on("change",function(){
        loadTable();
    });
    loadTable(); 
});

function loadTable(){
    table = $('#tabla_usus').DataTable({
        destroy:true,
        "ajax": {
           "url": base_url+"Usuarios/datatable_records",
           type: "post",
           data: { lugar:  $("#lugar option:selected").val()},
            error: function(){
               $("#tabla_usus").css("display","none");
            }
        },
        "columns": [   
            {"data": "id"},
            {"data": null,
                render:function(data,type,row){
                    html= row.nombre+" "+row.apellidos;
                    return html;
                }
            },
            {"data": "usuario"},
            {"data": "tienda"},
            {"data": null,
                render:function(data,type,row){
                html= "<button id='editar' onclick=editar("+row.id+") title='Editar Usuario' class='btn btn-info edit'><i class='fa fa-edit'></i></button>\
                    <button id='remove' onclick=delete_record("+row.id+") title='Eliminar Usuario' class='btn btn-danger delete'><i class='fa fa-trash'></i></button>";
                    return html;
                }
            }
        ],
        "pageLength": 50
    });

    /*$('#tabla_usus').on('click', 'button.edit', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = base_url+'Usuarios/alta/'+data.id;
    });
    $('#tabla_usus').on('click', 'button.delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        delete_record(data.id);
    });*/ 
}

function editar(id){
    window.location = base_url+'Usuarios/alta/'+id;
}

function delete_record(id) {
    Swal.fire({
        title: 'Está seguro de eliminar este usuario?',
        text: "Se eliminará definitivamente del listado!",
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar!',
        confirmButtonColor: '#2F8BE6',
        cancelButtonColor: '#F55252',
        confirmButtonText: 'Si, eliminar!',
        confirmButtonClass: 'btn btn-warning',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: base_url+"Usuarios/delete/"+id,
                    success: function (result) {
                        //console.log(result);
                        table.ajax.reload();
                        Swal.fire({
                          type: "success",
                          title: 'Borrado!',
                          text: 'Se ha eliminado correctamente.',
                          confirmButtonClass: 'btn btn-success',
                        })
                    }
                });   
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire({
            title: 'Cancelado',
            text: '',
            type: 'error',
            confirmButtonClass: 'btn btn-success',
            })
        }
    });

}

