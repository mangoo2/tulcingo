
$(document).ready(function () {
    $("#lugar").on("change",function(){
        loadTable();
    });
    loadTable();
});

function loadTable() {
    table = $('#tabla_clientes').DataTable({
        "bProcessing": true,
        "serverSide": true,
        destroy:true,
        "ajax": {
           "url": base_url+"Clientes/clientesall",
           type: "post",
           data: { lugar:  $("#lugar option:selected").val() },
            error: function(){
               $("#tabla_clientes").css("display","none");
            }
        },
        "columns": [

            {"data": "id"},
            {"data": "nombre_ap"},
            {"data": "calle"},
            {"data": "tienda1"},
            {"data": null,
                render:function(data,type,row){
                    var htmlt= row.telefono+"<br>"+row.telefono2;
                    return htmlt;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var htmlu;
                    if(row.update=="0000-00-00 00:00:00"){
                        htmlu=row.reg;
                    }else{
                        htmlu=row.update;
                    }
                    return htmlu;
                }
            },
            //{"data": "update"},
            {"data": null,
                render:function(data,type,row){
                    var html= "<button id='remove' title='Editar Cliente' class='btn btn-info edit'><i class='fa fa-edit'></i></button>\
                    <button id='remove' title='Eliminar Cliente' class='btn btn-danger delete'><i class='fa fa-trash'></i></button>";
                    return html;
                }
            }
        ],
        "pageLength": 50
    });

    $('#tabla_clientes').on('click', 'button.edit', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = base_url+'Clientes/alta/'+data.id;
    });
    $('#tabla_clientes').on('click', 'button.delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        delete_record(data.id);
    }); 
}

function delete_record(id) {
    Swal.fire({
        title: 'Está seguro de eliminar este cliente?',
        text: "Se eliminará definitivamente del listado!",
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar!',
        confirmButtonColor: '#2F8BE6',
        cancelButtonColor: '#F55252',
        confirmButtonText: 'Si, eliminar!',
        confirmButtonClass: 'btn btn-warning',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: base_url+"Clientes/delete/"+id,
                    success: function (result) {
                        //console.log(result);
                        table.ajax.reload();
                        Swal.fire({
                          type: "success",
                          title: 'Borrado!',
                          text: 'Se ha eliminado correctamente.',
                          confirmButtonClass: 'btn btn-success',
                        })
                    }
                });   
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire({
            title: 'Cancelado',
            text: '',
            type: 'error',
            confirmButtonClass: 'btn btn-success',
            })
        }
    });

}

