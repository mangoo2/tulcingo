$(function () {

    $("#tipo").on("change",function(){
        //console.log(this.value);
        getSelect(this.value,$("#idaux").val());
    });

    if($("#idaux").val()>0){
        getSelect($("#tipo option:selected").val(),$("#idaux").val());
    }

    $('#form_cliente').validate({
        rules: {
            nombre: "required",
            tiendamx: "required",
            tienda: "required"
        },
        messages: {
            nombre: "Campo obligatorio",
            tiendamx: "Campo obligatorio",
            tienda: "Campo obligatorio"
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: base_url+"Clientes/submit",
                data: $(form).serialize(),
                beforeSend: function(){
                    $("#btn_submit").attr("disabled",true);
                },
                success: function (data,result) {
                    //console.log(result);
                    //console.log(data);
                    $("#btn_submit").attr("disabled",false);
                    toastr.success('Se han realizado los cambios correctamente', 'Éxito');
                    setTimeout(function () { window.location.href = base_url+"Clientes" }, 1500);
                }
            }); 
            return false; // required to block normal submit for ajax
         },
        errorPlacement: function(label, element) {
          label.addClass('mt-2 text-danger');
          label.insertAfter(element);
        },
        highlight: function(element, errorClass) {
          $(element).parent().addClass('has-danger')
          $(element).addClass('form-control-danger')
        }
    });
});

function getSelect(value,id){
    $.ajax({
        type: "POST",
        url: base_url+"Clientes/getSelectTipo",
        data: { tipo: value, id_cli:id },
        success: function (result) {
            //console.log(result);
            $("#cont_select").html(result);
        }
    });
}