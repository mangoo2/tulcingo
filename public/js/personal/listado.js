
    $(document).ready(function () {
        table = $('#tabla_pers').DataTable({
            "ajax": {
               "url": base_url+"Personal/datatable_records",
               type: "post",
                error: function(){
                   $("#tabla_pers").css("display","none");
                }
            },
            "columns": [
                {"data": "id"},
                //{"data": "empleado"},
                {"data": null,
                    render:function(data,type,row){
                        var msj="";
                        msj = row.nombre+" "+row.app+" "+row.apm;
                        return msj;
                    }
                },
                {"data": "num_licencia"},
                //{"data": "tipo_licencia"},
                {"data": null,
                    render:function(data,type,row){
                        var msj="";
                        if(row.tipo_licencia==1)
                            msj = "Particular";
                        if(row.tipo_licencia==2)
                            msj = "Mercantil";
                        if(row.tipo_licencia==3)
                            msj = "Moto";
                        return msj;
                    }
                },
                {"data": "fecha_ingreso"},
                {
                    "data": null,
                    "defaultContent": "<button id='remove' title='Editar Personal' class='btn btn-info edit'><i class='fas fa-edit'></i></button>\
                    <button id='remove' title='Eliminar Personal' class='btn btn-danger delete'><i class='ti-trash'></i></button>"
                }
            ],
            "pageLength": 50
        });

        $('#tabla_pers').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = base_url+'Personal/alta/'+data.id;
        });
        $('#tabla_pers').on('click', 'button.delete', function () {
            var tr = $(this).closest('tr');
            var data = table.row(tr).data();
            delete_record(data.id);
        }); 
        
    });

    function delete_record(id) {
        swal({
            title: "¿Desea eliminar este operador?",
            text: "Se eliminará definitivamente del listado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar",
            closeOnConfirm: false
        }).then(function (isConfirm) {
            if (isConfirm) {
               $.ajax({
                     type: "POST",
                     url: base_url+"Personal/delete/"+id,
                     success: function (result) {
                        console.log(result);
                        table.ajax.reload();
                        swal("Éxito!", "Se ha eliminado correctamente", "success");
                     }
                 });
            }
        });

    }

