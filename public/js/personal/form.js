$(function () {
    $('#form_pers').validate({
        rules: {
            nombre: "required",
            app: "required",
            apm: "required",
            num_licencia: "required",
            tipo_licencia: "required",
            fecha_ingreso: "required"
        },
        messages: {
            nombre: "Campo obligatorio",
            app: "Campo obligatorio",
            apm: "Campo obligatorio",
            num_licencia: "Campo obligatorio",
            tipo_licencia: "Campo obligatorio",
            fecha_ingreso: "Campo obligatorio"
        },
        submitHandler: function (form) {
             $.ajax({ 
                 type: "POST",
                 url: base_url+"Personal/submit/",
                 data: $(form).serialize(),
                 beforeSend: function(){
                    $("#btn_submit").attr("disabled",true);
                 },
                 success: function (result) {
                    //console.log(result);
                    $("#btn_submit").attr("disabled",false);
                    swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                    setTimeout(function () { window.location.href = base_url+"Personal" }, 1500);
                 }
             });
             
             return false; // required to block normal submit for ajax
         },
        errorPlacement: function(label, element) {
          label.addClass('mt-2 text-danger');
          label.insertAfter(element);
        },
        highlight: function(element, errorClass) {
          $(element).parent().addClass('has-danger')
          $(element).addClass('form-control-danger')
        }
    });
});