$(function () {
    $('#form_tienda').validate({
        rules: {
            nombre: "required",
            lugar: "required"
        },
        messages: {
            nombre: "Campo obligatorio",
            lugar: "Campo obligatorio"
        },
        submitHandler: function (form) {
            if($("#caja").is(":checked")==true){
                caja=1;
            }else{
                caja=0;
            }
            $.ajax({
                type: "POST",
                url: base_url+"Tiendas/submit",
                data: $(form).serialize()+"&caja="+caja,
                beforeSend: function(){
                    $("#btn_submit").attr("disabled",true);
                },
                success: function (result) {
                    //console.log(result);
                    $("#btn_submit").attr("disabled",false);
                    toastr.success('Se han realizado los cambios correctamente', 'Éxito');
                    setTimeout(function () { window.location.href = base_url+"Tiendas" }, 1500);
                }
            }); 
            return false; // required to block normal submit for ajax
         },
        errorPlacement: function(label, element) {
          label.addClass('mt-2 text-danger');
          label.insertAfter(element);
        },
        highlight: function(element, errorClass) {
          $(element).parent().addClass('has-danger')
          $(element).addClass('form-control-danger')
        }
    });
});