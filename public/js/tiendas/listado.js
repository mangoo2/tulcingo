var table;
$(document).ready(function () {
    $("#lugar").on("change",function(){
        loadTable();
    });
    loadTable();    
});

function loadTable(){
    table = $('#tabla_tiendas').DataTable({
        destroy: true,
        "ajax": {
           "url": base_url+"Tiendas/getTiendas",
           type: "post",
           data: { lugar: $("#lugar option:selected").val() },
            error: function(){
               $("#tabla_tiendas").css("display","none");
            }
        },
        "columns": [

            {"data": "id"},
            {"data": "nombre"},
            {"data": "telefono"},
            {"data": null,
                render:function(data,type,row){
                    var html="";
                    if(row.lugar==1)
                        html="EEUU <img src='"+base_url+"app-assets/img/flags/us.png' width='38px' height='20px'>";
                    else
                        html="MX <img src='"+base_url+"public/img/mx.jpg' width='38px' height='20px'>";
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html= "<button id='remove' title='Editar Tienda' class='btn btn-info edit'><i class='fa fa-edit'></i></button>\
                    <button id='remove' title='Eliminar Tienda' class='btn btn-danger delete'><i class='fa fa-trash'></i></button>";
                    return html;
                }
            }
        ],
        "pageLength": 50
    });

    $('#tabla_tiendas').on('click', 'button.edit', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = base_url+'Tiendas/alta/'+data.id;
    });
    $('#tabla_tiendas').on('click', 'button.delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        delete_record(data.id);
    });     
}

function delete_record(id) {
    Swal.fire({
        title: 'Está seguro de eliminar esta tienda?',
        text: "Se eliminará definitivamente del listado!",
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar!',
        confirmButtonColor: '#2F8BE6',
        cancelButtonColor: '#F55252',
        confirmButtonText: 'Si, eliminar!',
        confirmButtonClass: 'btn btn-warning',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: base_url+"Tiendas/delete/"+id,
                    success: function (result) {
                        //console.log(result);
                        table.ajax.reload();
                        Swal.fire({
                          type: "success",
                          title: 'Borrado!',
                          text: 'Se ha eliminado correctamente.',
                          confirmButtonClass: 'btn btn-success',
                        })
                    }
                });   
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire({
            title: 'Cancelado',
            text: '',
            type: 'error',
            confirmButtonClass: 'btn btn-success',
            })
        }
    });

}

