
$(document).ready(function () {
    changeTienda();
    $('#cliente').select2({
        width: '95%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        ajax: {
            url: base_url+'Envios/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre+" "+element.apellidos,
                        nombre: element.nombre,
                        apellidos: element.apellidos,
                        calle: element.calle,
                        ciudad: element.ciudad,
                        edo: element.edo,
                        cp: element.cp,
                        telefono: element.telefono
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
    });
    $("#entregado").on("change",function(){
        loadTable();
    });
    $("#cliente").on("change",function(){
        if($("#cliente option:selected").val()!=""){
            //changeTienda();
            setTimeout(function () { 
                loadTable(); 
            }, 1000);
        }
    });
    $("#clienteText,#num_paq,#search_tel").on("change",function(){
        loadTable();
        //changeTienda();
    });
    $("#tipo").on("change",function(){
        changeTienda();
        setTimeout(function () { 
            loadTable(); 
        }, 1000); 
    });
    $("#id_tienda").on("change",function(){
        loadTable();
    });
    $("#fi,#ff").on("change",function(){
        if($("#fi").val()!="" && $("#ff").val()!=""){
            //changeTienda();
            setTimeout(function () { 
                loadTable(); 
                changeTienda();
            }, 1000);
        }
    });
    $("#tipo_cli").on("change",function(){
        if($("#tipo_cli").is(":checked")){
            $("#cont_cliex").show("slow");
            $("#cont_nocliex").hide("slow");
        }
        else{
            $("#cont_cliex").hide("slow");
            $("#cont_nocliex").show("slow");
        }
    });
    
    //console.log("id_tienda: "+$("#id_tienda").val());
    setTimeout(function () { 
        //if($("#id_tienda").val()!="0" && $("#tipo").val()=="1" || $("#tipo").val()=="2")

        if($("#tipo").val()=="1" || $("#tipo").val()=="2")
            $("#cont_btn_list").show();
        else
            $("#cont_btn_list").hide();
    },  1000);
    

    $("#export").on("click",function(){
        if($("#tipo_cli").is(":checked")==true){ //cliente exsitente - por select
            //cliente=$("#cliente option:selected").text();
            cliente=$("#cliente option:selected").val();
            clientetxt=$("#cliente option:selected").text();
            tipocli=0;
        }else{
            cliente=$("#clienteText").val();
            tipocli=1;
            clientetxt=cliente;
        }
        if(clientetxt=="")
            clientetxt="0";

        num_paq = $("#num_paq").val();
        sea_tel = $("#search_tel").val();
        if(num_paq=="")
            num_paq="SNAPLI";
        if(sea_tel=="")
            sea_tel="SNAPLI";

        estatus=$("#entregado option:selected").val();
        tipo=$("#tipo option:selected").val();
        id_tienda=$("#id_tienda option:selected").val();
        fi=$("#fi").val();
        ff=$("#ff").val();
        semana=$("#semana").val();
        if(cliente=="" || cliente==undefined)
            cliente=0;
        if(fi=="")
            fi=0;
        if(ff=="")
            ff=0;

        //console.log("cliente: "+cliente);
        //console.log("clientetxt: "+clientetxt);
        //console.log("num_paq: "+num_paq);
        window.open(base_url+"Envios/Export/"+cliente+"/"+clientetxt+"/"+tipocli+"/"+estatus+"/"+tipo+"/"+id_tienda+"/"+fi+"/"+ff+"/"+semana+"/"+num_paq+"/"+sea_tel, '_blank');
    });

    $("#id_tienda").on("change",function(){
        //if($(this).val()!="0"  && $("#tipo").val()=="1" || $("#tipo").val()=="2")

        if($("#tipo").val()=="1" || $("#tipo").val()=="2")
            $("#cont_btn_list").show();
        else
            $("#cont_btn_list").hide();
    });

    $("#btn-list").on("click",function(){
        $("#modal_week_shop").modal();
        if($("#tipo_cli").is(":checked")==true){ //cliente exsitente - por select
            //cliente=$("#cliente option:selected").text();
            cliente=$("#cliente option:selected").val();
            clientetxt=$("#cliente option:selected").text();
            tipocli=0;
        }else{
            cliente=$("#clienteText").val();
            tipocli=1;
            clientetxt=cliente;
        }
        if(clientetxt=="")
            clientetxt="0";

        num_paq = $("#num_paq").val();
        sea_tel = $("#search_tel").val();

        var tableList = $('#tabla_list').DataTable({
            "bProcessing": true,
            "serverSide": true,
            destroy:true,
            "ajax": {
               "url": base_url+"Envios/getEnvios",
               type: "post",
               data: {cliente:cliente,clientetxt:clientetxt,tipocli:tipocli,estatus:$("#entregado option:selected").val(),tipo:$("#tipo option:selected").val(),id_tienda:$("#id_tienda option:selected").val(),fi:$("#fi").val(),ff:$("#ff").val(),tipo_list:2,semana:$("#semana").val(),num_paq:num_paq,sea_tel:sea_tel }
            },
            "columns": [
                //{"data": "id"},
                {"data": null,
                    render: function(row){
                        var html = "";
                        if(row.tipo==1){ //eu -> mx
                            //html="<div id="+row.id+">"+row.folio+"</div>";
                            html="<div id="+row.id+">"+row.mi_folio+"</div>";
                        }else{
                            //console.log(isNaN(row.letra));
                            if(isNaN(row.letra)==false){
                                letra = parseInt(row.letra);
                                //letraf = letra+parseInt(row.folio);
                                letraf = parseInt(row.folio)+parseInt(letra);
                            }else{
                               letraf = row.letra+""+row.folio; 
                            }
                            html="<div id="+row.id+">"+letraf+"</div>";
                        }

                        return html;
                    }
                },
                {"data": "envia"},
                {"data": "recibe"},
                {"data": "tel_recibe"},
                {"data": "paquetes"},
                {"data": "slibras"},
                /*{"data": "slibras",
                    render: function(row){
                        var html = parseFloat(row.slibras).toFixed(2);
                        return html;
                    }
                },*/
                {"data": "contenido"},
                {"data": "destino"},
                {"data": "origen"},
                {"data": "semana"},
            ],
            "order": [[ 0, "desc" ]],
            "pageLength": 50,
            dom: 'Blfrtip',
            buttons: [
                'print'
            ],
            footerCallback: function (row, data, start, end, display) {
                var api = this.api();
     
                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    i = parseFloat(i).toFixed(2);
                    return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                };

                // Total over all pages
                totalp = api
                    .column(4)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
     
                // Total over this page
                pageTotalp = api
                    .column(4, { page: 'current' })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
     
                // Update footer
                $(api.column(4).footer()).html(totalp);
                /////////////////////////////////////////////////////

                // Total over all pages
                total = api
                    .column(5)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
     
                // Total over this page
                pageTotal = api
                    .column(5, { page: 'current' })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
     
                // Update footer
                $(api.column(5).footer()).html(total);
            },
        });
    });

    $("#save_comts").on("click",function(){
        $.ajax({
            type: "POST",
            url: base_url+"Envios/saveComents",
            data: { id:$("#id_env_com").val(), coments:$("#comentarios").val() },
            success: function (result) {
                toastr.success("Éxito!", "Comentarios guardados correctamente", "success");
                $("#comentarios").val("");
                $("#modal_coments").modal("hide");
                loadTable();
            }
        });
    });

    $("#export_weeks").on("click",function(){
        if($("#tipo_cli").is(":checked")==true){ //cliente exsitente - por select
            //cliente=$("#cliente option:selected").text();
            cliente=$("#cliente option:selected").val();
            tipocli=0;
            clientetxt=$("#cliente option:selected").text();
        }else{
            cliente=$("#clienteText").val();
            tipocli=1;
            clientetxt=cliente;
        }
        if(clientetxt=="")
            clientetxt="0";

        num_paq = $("#num_paq").val();
        sea_tel = $("#search_tel").val();

        estatus=$("#entregado option:selected").val();
        tipo=$("#tipo option:selected").val();
        id_tienda=$("#id_tienda option:selected").val();
        fi=$("#fi").val();
        ff=$("#ff").val();
        semana=$("#semana").val();
        if(cliente=="" || cliente==undefined)
            cliente=0;
        if(fi=="")
            fi=0;
        if(ff=="")
            ff=0;

        window.open(base_url+"Envios/ExportWeekShop/"+cliente+"/"+clientetxt+"/"+tipocli+"/"+estatus+"/"+tipo+"/"+id_tienda+"/"+fi+"/"+ff+"/2/"+semana+"/"+num_paq+"/"+sea_tel, '_blank');
    });

    setTimeout(function () { 
        loadTable(); 
    }, 1500); 

    $("#semana").on("change",function(){
        loadTable();
        changeTienda();
    });
});

//document.getElementById("clienteText").addEventListener("keyup", getText);

function getText() {
	let text = document.getElementById("clienteText").value;
	loadTable();
	event.preventDefault();
	$.ajax({
		url: base_url + 'Envios/searchclientes',
		dataType: "json",
		data: {
			"search": text
		},
		success: function (data) {
			//console.log(data);
			var itemscli = [];
			data.forEach(function (element) {
				itemscli.push({
					id: element.id,
					text: element.nombre + " " + element.apellidos,
					nombre: element.nombre,
					apellidos: element.apellidos,
					calle: element.calle,
					ciudad: element.ciudad,
					edo: element.edo,
					cp: element.cp,
					telefono: element.telefono
				});
				//console.log(itemscli);
			});
			return {
				results: itemscli
			};
		}
	});
}

function changeTienda() {
    $.ajax({
        type: "POST",
        url: base_url+"Envios/getTiendasAll",
        data: {tipo:$("#tipo option:selected").val(),semana:$("#semana option:selected").val(),f1:$("#fi").val(),f2:$("#ff").val()},
        success: function (result) {
            $("#id_tienda").html(result);
        }
    });
}

function loadTable(){
    if($("#tipo_cli").is(":checked")==true){ //cliente exsitente - por select
        cliente=$("#cliente option:selected").val();
        clientetxt=$("#cliente option:selected").text();
        tipocli=0;
        if(cliente==undefined)
            cliente="";
    }else{
        cliente=$("#clienteText").val();
        clientetxt=cliente;
        tipocli=1;
    }
    num_paq = $("#num_paq").val();;
    sea_tel = $("#search_tel").val();
    //console.log("cliente: "+cliente);
    //console.log("tipocli: "+tipocli);
    //console.log("id_tienda: "+$("#id_tienda option:selected").val());
    table = $('#tabla_envios').DataTable({
        "bProcessing": true,
        "serverSide": true,
        destroy:true,
        //stateSave:true,
        "ajax": {
           "url": base_url+"Envios/getEnvios",
           type: "post",
           data: {cliente:cliente,clientetxt:clientetxt,tipocli:tipocli,estatus:$("#entregado option:selected").val(),tipo:$("#tipo option:selected").val(),id_tienda:$("#id_tienda option:selected").val(),fi:$("#fi").val(),ff:$("#ff").val(),tipo_list:1,semana:$("#semana").val(),num_paq:num_paq,sea_tel:sea_tel }
        },
        "columns": [
            /*{"data": null,
                render: function(row){
                    var html = "<div id="+row.id+">"+row.id+"</div>";
                    return html;
                }
            },*/
            {"data": null,
                render: function(row){
                    var html = "";
                    if(row.tipo==1){ //eu -> mx
                        //html="<div id="+row.id+">"+row.folio+"</div>";
                        html="<div style='margin-top:230px; vertical-align:middle' id="+row.id+"><p style='font-size:25px; vertical-align:middle'>"+row.mi_folio+"</p></div>";    
                    }else{
                        //console.log(isNaN(row.letra));
                        //console.log("folio: "+row.folio);
                        if(isNaN(row.letra)==false){ 
                            //console.log("numerica: "+row.letra);
                            letra = parseInt(row.letra);
                            //letraf = letra+parseInt(row.folio);
                            letraf = parseInt(row.folio)+parseInt(letra);
                        }else{
                            //console.log("letra: "+row.letra);
                            letraf = row.letra+""+row.folio; 
                        }
                        html="<div style='margin-top:230px; vertical-align:middle' id="+row.id+"><p style='font-size:25px; vertical-align:middle'>"+letraf+"</p></div>";
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html= "";
                    if(row.entregado==0){
                        html= "<div class='row justify-content-center'><div class='col-md-12 text-center' style='margin-top:200px; font-size:22px;'>Sin entregar</div> <div class='col-md-6'>\
                        <i style='font-size:25px; color: #FFC90E !important;' class='fa fa-spinner fa-spin fa-fw'></i>\
                        <span class='sr-only'>Loading...</span>\
                        <i style='font-size:25px; color: #FFC90E !important;' class='fa fa-circle' aria-hidden='true'></i></div></div>";
                    }else if(row.entregado==1){
                        $('#'+row.id).parent().parent().addClass('bg-danger bg-gradient text-white');
                        $('#'+row.id).parent().parent().find( '.form-control' ).addClass(' text-white ');
                        html= "<div class='row justify-content-center'><div class='col-md-12 text-center' style='margin-top:200px; font-size:22px;'>Entregado</div> <div class='col-md-6'>\
                        <i style='font-size:25px; color: green !important;' class='fa fa-check animated infinite tada' aria-hidden='true'></i>\
                        <i style='font-size:25px; color: green !important;' class='fa fa-circle' aria-hidden='true'></i></div></div>"+row.fecha_entrega+"";
                    }else if(row.entregado==3){
                        html= "<div class='row justify-content-center'><div class='col-md-12 text-center' style='margin-top:200px; font-size:22px;'>Reportado</div> <div class='col-md-6'>\
                        <i style='font-size:25px; color: red !important;' class='fa fa-spinner fa-spin fa-fw'></i>\
                        <span class='sr-only'>Loading...</span>\
                        <i style='font-size:25px; color: red !important;' class='fa fa-circle' aria-hidden='true'></i></div></div>";
                    }
                    return html;
                }
            },
            //{"data": "folio"},
            /*{"data": "fecha_reg"},
            {"data": "recibe"},
            {"data": "envia"},
            {"data": "origen"},
            {"data": "destino"},
            //{"data": "contenido"},
            {"data": "user"},*/
            {"data": null,
                render:function(data,type,row){
                    var datos="";
                    //console.log("tipo: "+row.tipo);
                    var valida_tel="";
                    if(row.id_cliente!="0" && row.id_cliente!=""){
                        /*console.log("tel_recibe: "+row.tel_recibe);
                        console.log("telcli: "+row.telcli);
                        console.log("telcli2: "+row.telcli2);*/
                        if(row.tel_recibe!=row.telcli && row.tel_recibe!=row.telcli2){
                            valida_tel="<br> Número teléfonico no conicide con mis registros";
                        }
                    }
                    if(row.tipo=="1") //eeuu
                        img = "<img src='"+base_url+"app-assets/img/flags/us.png'> <i class='fa fa-long-arrow-right' aria-hidden='true'></i> <img src='"+base_url+"public/img/mx.jpg' width='38px'>";
                    else
                        img = "<img src='"+base_url+"public/img/mx.jpg' width='38px'> <i class='fa fa-long-arrow-right' aria-hidden='true'></i> <img src='"+base_url+"app-assets/img/flags/us.png' > ";
                    datos="<span style='font-size:18px'>"+row.paquetes+" PAQ</span> - &nbsp&nbsp&nbsp Valor: $ "+parseFloat(row.stotalve).toFixed(2)+" - &nbsp&nbsp&nbsp "+parseFloat(row.slibras).toFixed(2)+" Libras - &nbsp&nbsp&nbsp&nbsp&nbsp TOTAL COBRO: $ "+parseFloat(row.stotal).toFixed(2)+"\
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp "+img+" <br>\
                        <table width='100%' cellspacing='0'>\
                            <tr>\
                                <td width='36%'>"+row.fecha_reg+" <br> Semana: "+row.semana+"</td>\
                                <td width='32%' align='center'>Origen:<br> "+row.origen+"</td>\
                                <td width='32%' align='center'>Destino:\
                                    <div class='div_destino_"+row.id+"'><button class='btn-trans' onclick='abrir_select("+row.id+","+row.id_destino+","+row.lugar2+","+row.id_origen+")'>"+row.destino+"</button></div>\
                                </td>\
                            </tr>\
                            <tr>\
                                <table width='100%'>\
                                    <tr>\
                                        <td width='50%'>Recibe: "+row.recibe+"<br> Tel: "+row.tel_recibe+" "+valida_tel+"</td>\
                                        <td width='50%'>\
                                        <label for='calle_list_recibe'>Calle</label> <input class='form-control' id='calle_list_recibe' value='"+row.calle_recibe+"' onchange='editaDireccion(this.value,calle_list_recibe,"+row.id_cliente+",this.id,"+row.id+")'>\
                                        </td>\
                                    </tr>\
                                    <tr>\
                                        <td width='16.66%'>\
                                        <label for='ciudad_list_recibe'>Ciudad</label> <input class='form-control' id='ciudad_list_recibe' value='"+row.ciudad_recibe+"' onchange='editaDireccion(this.value,ciudad_list_recibe,"+row.id_cliente+",this.id,"+row.id+")'>\
                                        </td>\
                                        <td width='16.66%'>\
                                        <label for='cp_list_recibe'>CP</label> <input class='form-control' id='cp_list_recibe' value='"+row.cp_recibe+"' onchange='editaDireccion(this.value,cp_list_recibe,"+row.id_cliente+",this.id,"+row.id+")'>\
                                        </td>\
                                    </tr>\
                                    <tr>\
                                        <td width='50%'>Envía: "+row.envia+"<br> Tel: "+row.tel_envia+"</td>\
                                        <td width='50%'>\
                                        <label for='calle_list_envia'>Calle</label> <input class='form-control' id='calle_list_envia' value='"+row.calle_envia+"' onchange='editaDireccion(this.value,calle_list_envia,"+row.id_cliente_envia+",this.id,"+row.id+")'>\
                                        </td>\
                                    </tr>\
                                    <tr>\
                                        <td width='16.66%'>\
                                        <label for='ciudad_list_envia'>Ciudad</label> <input class='form-control' id='ciudad_list_envia' value='"+row.ciudad_envia+"' onchange='editaDireccion(this.value,ciudad_list_envia,"+row.id_cliente_envia+",this.id,"+row.id+")'>\
                                        </td>\
                                        <td width='16.66%'>\
                                        <label for='cp_list_envia'>CP</label> <input class='form-control' id='cp_list_envia' value='"+row.cp_envia+"' onchange='editaDireccion(this.value,cp_list_envia,"+row.id_cliente_envia+",this.id,"+row.id+")'>\
                                        </td>\
                                    </tr>\
                                </table>\
                                <table width='100%'>\
                                    <tr>";
                                    if(row.entregado==1){
                                        datos+="\
                                        <td width='100%' style='color:#ffffff'>";
                                    } else {
                                        datos+="\
                                        <td width='100%' style='color:#a90c00'>";
                                    }
                                    datos+="\
                                        Contenido: <br>\
                                        "+row.contenido+"\
                                    </tr>\
                                </table>\
                            </tr>\
                        </table>";
                    if(row.tipo=="1"){ //eeuu
                        datos+="Realizado por: "+row.user+"<br>";
                    }else{
                        datos+="Realizado por: "+row.user+"<br><span style='font-size:18px'># de paquetes: "+row.paquetes+"</span>";
                    }
                    //console.log(row);
                    if(row.coments!='')
                        datos+="<div style='color:#342E49;border-radius:10px; padding:5px;background: #BCF9BC;border: 1px solid #94da3a;font-size:15px;margin-bottom:5px'> "+row.coments+"</div>";
                    return datos;
                }
            },

            {"data": null,
                render:function(data,type,row){
                    var btm=""; var dis=""; var dis2=""; var dis_entre="";
                    if(row.entregado==1){
                        dis_entre="disabled";
                    }
                    if(row.entregado==3){
                        dis2="disabled";
                    }
                    if(row.entregado!=1){
                        btn="<button style='margin-top:3px' "+dis2+" title='Reportar paquete' class='btn btn-danger reportar tam_tab_env'><i class='fa fa-exclamation-triangle'></i></button>";
                    }else{
                        btn="";
                        //dis="disabled";
                        dis="style='color:#342E49'";
                    }
                    
                    //var html= "<a target='_blank' href='"+base_url+"Envios/formatohoja/"+row.id+"' title='Generar Hoja' class='btn btn-info hoja'><i class='fa fa-file'></i></a>\
                    var html= "<a target='_blank' href='"+base_url+"Envios/formatorecibo/"+row.id+"' title='Generar Recibo' class='btn btn-info hoja1 tam_tab_env'><i class='fa fa-file-pdf-o'></i></a>\
                    <button title='Generar Etiqueta' class='btn btn-info etiqueta tam_tab_env'><i class='fa fa-barcode'></i></button>\
                    <button "+dis+" title='Paquete entregado' class='btn btn-warning entregado tam_tab_env'><i class='fa fa-handshake-o'></i></button>\
                    "+btn+"\
                    <button title='Ver Contenido' class='btn btn-success detalles tam_tab_env' data-toggle='modal' data-target='#modal_conts'><i class='fa fa-eye'></i></button>\
                    <button title='Insertar Comentarios' class='btn btn-info coments tam_tab_env' data-toggle='modal' data-target='#modal_coments'><i class='fa fa-comments-o'></i></button>\
                    <button title='Editar Envío' class='btn btn-info edit tam_tab_env'><i class='fa fa-edit'></i></button>\
                    <button id='remove' "+dis_entre+" title='Eliminar Envío' class='btn btn-danger delete tam_tab_env'><i class='fa fa-trash'></i></button>";
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "pageLength": 50
    });

    $('#tabla_envios').on('click', 'button.edit', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = base_url+'Envios/alta/'+data.id;
    });
    $('#tabla_envios').on('click', 'button.delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        delete_record(data.id);
    });
    $('#tabla_envios').on('click', 'button.etiqueta', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        window.open(base_url+'Envios/etiquetas/'+data.id, "Etiqueta", "width=780, height=612");
        //$('#iframeetiqueta').html('<iframe src="'+base_url+'Envios/etiqueta?id='+data.id+'&print=true"></iframe>');
    });
    $('#tabla_envios').on('click', 'button.entregado', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        if(data.entregado==1){
            window.open(base_url+'Envios/etiquetaEntrega/'+data.id, "Etiqueta", "width=780, height=612");
        }else{
            cambiarEstatus(data.id,1);
        }
    });
    $('#tabla_envios').on('click', 'button.reportar', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        cambiarEstatus(data.id,3);
    });
    $('#tabla_envios').on('click', 'button.detalles', function () { //agregar nombre, tel y direccion de cliente que envia y recibe
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        $("#id_env").val(data.id);
        $.ajax({
            type: "POST",
            url: base_url+"Envios/getContenidos",
            data: {id:data.id},
            success: function (result) {
                $("#conts_paqs").html(result);
            }
        });
    });
    $('#tabla_envios').on('click', 'button.coments', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        $("#id_env_com").val(data.id);
        //$("#comentarios").val(data.comentarios);
    });
}

function abrir_select(id,id_destino,lugar,id_origen){
    Swal.fire({
        title: 'Está seguro de cambiar destino?',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cerrar',
        confirmButtonColor: '#2F8BE6',
        cancelButtonColor: '#F55252',
        confirmButtonText: 'Si, cambiar!',
        confirmButtonClass: 'btn btn-warning',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: base_url+"Envios/get_destino_select",
                data: {id:id,id_destino:id_destino,lugar:lugar,id_origen:id_origen},
                success: function (result) {
                    $('.div_destino_'+id).html(result);     
                }
            });
        }
    });

}

function editar_direccion(id,id_origen,lugar){
    var tienda=$('#id_tienda_edit option:selected').val();
    $.ajax({
        type: "POST",
        url: base_url+"Envios/get_destino_editar",
        data: {id:id,tienda:tienda,id_origen:id_origen,lugar:lugar},
        success: function (result) {
            loadTable();
        }
    });
}

function editaDireccion(val,id_element,id_cliente,id,id_envio){
    var element = $(id_element).val();
    //var field = id_element[0].id;
    var field = id;
    var field2 = " ";
    var type = " ";
    
    if(field == "calle_list_recibe"){
        field = "calle_recibe";
        field2 = "calle";
        type = "id_cliente";
    }else if(field == "ciudad_list_recibe"){
        field = "ciudad_recibe";
        field2 = "ciudad";
        type = "id_cliente";
    }else if(field == "cp_list_recibe"){
        field = "cp_recibe";
        field2 = "cp";
        type = "id_cliente";
    }else if(field == "calle_list_envia"){
        field = "calle_envia";
        field2 = "calle";
        type = "id_cliente_envia";
    }else if(field == "ciudad_list_envia"){
        field = "ciudad_envia";
        field2 = "ciudad";
        type = "id_cliente_envia";
    }else if(field == "cp_list_envia"){
        field = "cp_envia";
        field2 = "cp";
        type = "id_cliente_envia";
    }

    //console.log("val: "+val);

    $.ajax({
        type: "POST",
        url: base_url+"Envios/editaDireccion",
        data: { id:id, id_cliente:id_cliente, type:type, field:field, field2:field2, element:val, id_envio:id_envio  },
        success: function (result) {
            //console.log(result);
            loadTable();
        }
    });
}

function cambiarEstatus(id,estatus) {
    var text=""; var title=""; var class_btn="";
    if(estatus==1){ //entregado
        text="Se cambiará a entregado!";
        title="Entregado!";
        txt_btn="Si, entregar!";
        class_btn="btn btn-success";
    }
    else if(estatus==3){ //reportado
        text="Se cambiará a destino incorrecto!";
        title="Reportado!";
        txt_btn="Si, reportar!";
        class_btn="btn btn-warning";
    }

    Swal.fire({
        title: 'Está seguro de cambiar el estatus?',
        text: text,
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar!',
        confirmButtonColor: '#2F8BE6',
        cancelButtonColor: '#F55252',
        confirmButtonText: txt_btn,
        confirmButtonClass: class_btn,
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: base_url+"Envios/entregado",
                    data: {id:id, estatus:estatus},
                    success: function (result) {
                        //console.log(result);
                        loadTable();
                        Swal.fire({
                          type: "success",
                          title: title,
                          text: 'Estatus cambiado correctamente.',
                          confirmButtonClass: 'btn btn-success',
                        });
                        if(estatus==1){ //entregado
                            window.open(base_url+'Envios/etiquetaEntrega/'+id, "Etiqueta", "width=780, height=612");
                        }
                    }
                });   
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire({
            title: 'Movimiento Cancelado',
            text: '',
            type: 'error',
            confirmButtonClass: 'btn btn-success',
            })
        }
    });

}

function delete_record(id) {
    Swal.fire({
        title: 'Está seguro de eliminar este envío?',
        text: "Se eliminará definitivamente del listado!",
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar!',
        confirmButtonColor: '#2F8BE6',
        cancelButtonColor: '#F55252',
        confirmButtonText: 'Si, eliminar!',
        confirmButtonClass: 'btn btn-warning',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: base_url+"Envios/delete/"+id,
                    success: function (result) {
                        //console.log(result);
                        loadTable();
                        Swal.fire({
                          type: "success",
                          title: 'Borrado!',
                          text: 'Se ha eliminado correctamente.',
                          confirmButtonClass: 'btn btn-success',
                        })
                    }
                });   
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire({
            title: 'Movimiento Cancelado',
            text: '',
            type: 'error',
            confirmButtonClass: 'btn btn-success',
            })
        }
    });

}

