
$(document).ready(function () {
    //loadTable();
    $("#fi,#ff,#id_tienda").on("change",function(){
        if($("#fi").val()!="" && $("#ff").val()!=""){
            loadTable();
            totalesGral();
        }
    });

    $("#export").on("click",function(){
        fi=$("#fi").val();
        ff=$("#ff").val();

        if(fi=="")
            fi=0;
        if(ff=="")
            ff=0;

        if($("#perfilid_user").val()==1){
            tienda = $("#id_tienda option:selected").val();
        }else{
            tienda = $("#id_tienda_user").val();
        }

        if(fi!=0 && ff!=0){
            window.open(base_url+"CorteCaja/Export/"+fi+"/"+ff+"/"+tienda, '_blank');
        }
    });
    if($("#perfilid_user").val()==1){
        getTienda();
    }
});

function getTienda() {
    $.ajax({
        type: "POST",
        url: base_url+"Envios/getTiendasAll",
        data: {tipo:$("#tipot_user").val()},
        success: function (result) {
            $("#id_tienda").html(result);
        }
    });
}

function totalesGral(){
    if($("#perfilid_user").val()==1){
        tienda = $("#id_tienda option:selected").val();
    }else{
        tienda = $("#id_tienda_user").val();
    }
    $.ajax({
        type: "POST",
        url: base_url+"CorteCaja/getTotalesCorte",
        data: { fi:$("#fi").val(),ff:$("#ff").val(),id_tienda:tienda },
        async:false,
        success: function (result) {
            //console.log("result: "+result);
            var array = $.parseJSON(result);
            //console.log("unica: "+array.unica);
            if(array.unica==1){
                //total_tdc=array.stotaltc+array.stotaltd;
                total_tdc=array.stotaltc;
                $("#cont_metodos").html('<table class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">\
                                        <thead>\
                                            <tr>\
                                                <th>\
                                                    <h5 id="cash" class="modal-title">Efectivo:</h5>\
                                                </th>\
                                                <th>\
                                                    <h5 id="tcash" class="modal-title">$</h5>\
                                                </th>\
                                            </tr>\
                                            <tr>\
                                                <th>\
                                                    <h5 id="tc" class="modal-title">Tarjeta de Crédito / Débito:</h5>\
                                                </th>\
                                                <th>\
                                                    <h5 id="ttarjc" class="modal-title">$</h5>\
                                                </th>\
                                            </tr>\
                                        </thead>\
                                    </table>');
                $("#tcash").html(Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.stotal));
                $("#ttarjc").html(Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total_tdc));
            }else{
                $("#cont_metodos").html("");
                var tabla_metodos="";
                datasEfec=array.datasEfec;
                datasEfec.forEach(function(m){
                    tabla_metodos+='\
                        <table class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">\
                            <thead>\
                                <tr>\
                                    <th colspan="2">\
                                        <h5 id="cash" class="modal-title">Tienda: '+m.tienda+'</h5>\
                                    </th>\
                                </tr>\
                                <tr>\
                                    <th width="60%">\
                                        <h5 id="cash" class="modal-title">Efectivo:</h5>\
                                    </th>\
                                    <th width="40%">\
                                        <h5 id="tcash" class="modal-title">'+Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(m.stotal)+'</h5>\
                                    </th>\
                                </tr>\
                            </thead>\
                        </table>'
                });
                $("#cont_metodos").append(tabla_metodos);
                var tabla_metodost="";
                datasTarj=array.datasTarj;
                datasTarj.forEach(function(m){
                    tabla_metodost+='\
                        <table class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">\
                            <thead>\
                                <tr>\
                                    <th colspan="2">\
                                        <h5 id="cash" class="modal-title">Tiendafff: '+m.tienda+'</h5>\
                                    </th>\
                                </tr>\
                                <tr>\
                                    <th width="60%">\
                                        <h5 id="tc" class="modal-title">Tarjeta de Crédito / Débito:fff</h5>\
                                    </th>\
                                    <th width="40%">\
                                        <h5 id="tcash" class="modal-title">'+Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(m.ttotal)+'</h5>\
                                    </th>\
                                </tr>\
                            </thead>\
                        </table>'
                });
                $("#cont_metodos").append(tabla_metodost);
            }
            /*total_tdc=array.stotaltc+array.stotaltd;
            $("#tcash").html(Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.stotal));
            $("#ttarjc").html(Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total_tdc));*/
            //$("#ttarjb").html(Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(array.stotaltd));
        }
    });
}

function loadTable(){
    //console.log("cliente: "+cliente);
    //console.log("tipocli: "+tipocli);
    if($("#perfilid_user").val()==1){
        tienda = $("#id_tienda option:selected").val();
    }else{
        tienda = $("#id_tienda_user").val();
    }
    table = $('#tabla_corte').DataTable({
        destroy:true,
        "ajax": {
           "url": base_url+"CorteCaja/getCorte",
           type: "post",
           data: { fi:$("#fi").val(),ff:$("#ff").val(), id_tienda:tienda }
        },
        "columns": [
            //{"data": "id"},
            {"data": null,
                render: function(row){
                    var html = "";
                    if(row.tipo==1){ //eu -> mx
                        //html="<div id="+row.id+">"+row.folio+"</div>";
                        html="<div id="+row.id+">"+row.mi_folio+"</div>";
                    }else{
                        //console.log(isNaN(row.letra));
                        if(isNaN(row.letra)==false){
                            letra = parseInt(row.letra);
                            //letraf = letra+parseInt(row.folio);
                            letraf = parseInt(row.folio)+parseInt(letra);
                        }else{
                           letraf = row.letra+""+row.folio; 
                        }
                        html="<div id="+row.id+">"+letraf+"</div>";
                    }

                    return html;
                }
            },
            {"data": "semana"},
            {"data": "envia"},
            {"data": "paquetes"},
            {"data": "slibras"},
            {"data": "destino"},
            /*{"data": "stotal",
                "render" : function (data,type,row) {
                    var met='';
                    if(row.metodo=="1")
                        met="Efectivo";
                    else if(row.metodo=="2" || row.metodo=="3")
                        met="T. Crédito/Débito";
                    return met;
                }
            },*/
            {"data": null,
                "render" : function (data,type,row) {
                    var efe='';
                    var met='';
                    var div='';
                    if(row.efectivo_cantidad!=0){
                        efe="Efectivo";
                    }
                    if(row.tarjeta_cantidad!=0){
                        met="T. Crédito/Débito";
                    }
                    if(row.efectivo_cantidad!=0 && row.tarjeta_cantidad!=0){
                        div=" - ";
                    }
                    return efe+div+met;
                }
            },
            {"data": "stotal",
                "render" : function (data,type,row) {
                    var msj='';
                    msj+= new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.stotal);
                    return msj;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        dom: 'Blfrtip',
        buttons: [
            'print'
        ],
        footerCallback: function (row, data, start, end, display) {
            var api = this.api();
 
            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
            };

            // Total over all pages
            totalp = api
                .column(3)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
 
            // Total over this page
            pageTotalp = api
                .column(3, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
 
            // Update footer
            $(api.column(3).footer()).html(totalp);
            /////////////////////////////////////////////////////

            // Total over all pages
            totall = api
                .column(4)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
 
            // Total over this page
            pageTotall = api
                .column(4, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
 
            // Update footer
            $(api.column(4).footer()).html(totall);
            /////////////////////////////////////////////////////
 
            // Total over all pages
            total = api
                .column(7)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
 
            // Total over this page
            pageTotal = api
                .column(7, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
 
            // Update footer
            $(api.column(7).footer()).html('$'+total);
        },
    });
}
