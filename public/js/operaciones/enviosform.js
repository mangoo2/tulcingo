var validar_total=0;
$(function () {
    $('#cliente').select2({
        width: '95%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        ajax: {
            url: base_url+'Envios/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    tipo:$("#tipo option:selected").val(),
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre+" "+element.apellidos,
                        nombre: element.nombre,
                        apellidos: element.apellidos,
                        calle: element.calle,
                        ciudad: element.ciudad,
                        edo: element.edo,
                        cp: element.cp,
                        telefono: element.telefono
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $("#nom_envia").val(data.nombre);
        $("#apellido_envia").val(data.apellidos);
        $("#calle_envia").val(data.calle);
        $("#ciudad_envia").val(data.ciudad);
        $("#edo_envia").val(data.edo);
        $("#cp_envia").val(data.cp);
        $("#tel_envia").val(data.telefono);
    });
    select2Recibe();

    $("#precio").on("blur",function(){
        if(row_paq==0){
            if($("#precio").val()!="" && $("#impuesto").val()!=""){
                //alert(1);
                $("#total").val(parseFloat($("#precio").val())+parseFloat($("#impuesto").val()));
                calculaSuper();
            }
        }
    });
    $("#impuesto").on("blur",function(){
        if(row_paq==0){
            //alert(1);
            if($("#precio").val()!="" && $("#impuesto").val()!=""){
                $("#total").val(parseFloat($("#precio").val())+parseFloat($("#impuesto").val()));
                calculaSuper();
            }
        }
    });



    //falta funcion para traer tiendas dependiedo del tipo de envio
    $("#tipo").on("change",function(){
        taerTiendas(); 
    });
    if($("#id").val()>0){
        taerTiendas($("#id").val());
    }

    $("#addPaq").on("click",function(){
        addPaquete();
    });

    $("#savec1").on("click",function(){
        saveCliente(1);
    });
    $("#savec2").on("click",function(){
        saveCliente(2);
    });
    $("#history").on("click",function(){
        verHistorial();
    });
    calculaSuper();
    taerTiendas(); //aca me quedo, llama funcionn para actualizar tipo de envio, falta seleccionar por default
});


function calcular_paquete(){
    var precio=$(".precio_p").val();
    var impuesto= $(".impuesto_p").val();
    precio=Number (precio); 
    impuesto=Number (impuesto);
    var suma = parseFloat(precio)+parseFloat(impuesto);
    $(".total_p").val(suma);
    calculaSuper();
}

function select2Recibe(){
    var tipo=0;
    if($("#tipo option:selected").val()==1){ //eeuu
        tipo=2;
    }else{
        tipo=1;
    }
    $('#recibe').select2({
        width: '95%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        ajax: {
            url: base_url+'Envios/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    tipo:tipo,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre+" "+element.apellidos,
                        nombre: element.nombre,
                        apellidos: element.apellidos,
                        calle: element.calle,
                        ciudad: element.ciudad,
                        edo: element.edo,
                        cp: element.cp,
                        telefono: element.telefono,
                        tiendamx: element.tiendamx
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $("#nom_recibe").val(data.nombre);
        $("#apellido_recibe").val(data.apellidos);
        $("#calle_recibe").val(data.calle);
        $("#ciudad_recibe").val(data.ciudad);
        $("#edo_recibe").val(data.edo);
        $("#cp_recibe").val(data.cp);
        $("#tel_recibe").val(data.telefono);
        //console.log("tiendamx: "+data.tiendamx);
        $("#id_destino").val(data.tiendamx);
    });
}

function verHistorial(){
   if($("#cliente").val()!=undefined){
        id_cli=$("#cliente option:selected").val();
        $("#modal_envios").modal("show");
        $("#tabla_envios").DataTable();
        var nombre_em=$("#nom_envia").val()+" "+$("#apellido_envia").val();
        $("#nomb_envia_mod").html("<strong>Nombre:</strong> " +nombre_em);
        $("#tel_envia_mod").html("<strong>Teléfono:</strong> " +$("#tel_envia").val());
        $("#dir_envia_mod").html("<strong>Dirección:</strong> " +$("#calle_envia").val()+" "+$("#ciudad_envia").val()+" "+$("#edo_envia").val()+" "+$("#cp_envia").val());
        $.ajax({
            type: "POST",
            url: base_url+"Envios/getHistorialEnvios",
            data: {id_cli:id_cli,nombre_em:nombre_em},
            success: function (result){
                //console.log("result: "+result);
                $("#tabla_envios").DataTable({ destroy:true,"ordering": false });
                $("#body_hist").html(result);
            }
        }); 
    } 
}

function selectDest(id){
    $.ajax({
        type: "POST",
        url: base_url+"Envios/getEnvioId",
        data: {id:id},
        success: function (result) {
            var i = $.parseJSON(result);
            //console.log("id_cliente: "+i[0].id_cliente);
            if(i[0].id_cliente>0){
                $("#recibe").removeClass("select2-hidden-accessible");
                //$('#recibe').val(i[0].id_cliente).trigger('change.select2');
                $("#recibe").append("<option selected value='"+i[0].id_cliente+"'>"+i[0].nom_recibe+" "+i[0].apellido_recibe+"</option>");
                //$("#recibe").addClass("select2-hidden-accessible");
                select2Recibe();
            }
            $("#nom_recibe").val(i[0].nom_recibe);
            $("#apellido_recibe").val(i[0].apellido_recibe);
            $("#calle_recibe").val(i[0].calle_recibe);
            $("#ciudad_recibe").val(i[0].ciudad_recibe);
            $("#edo_recibe").val(i[0].edo_recibe);
            $("#cp_recibe").val(i[0].cp_recibe);
            $("#tel_recibe").val(i[0].tel_recibe);
            $("#id_destino").val(i[0].id_destino);
            $("#modal_envios").modal("hide");
        }
    }); 
}

function taerTiendas(){
    $.ajax({
        type: "POST",
        url: base_url+"Envios/getTiendaTipo", //solo traer la tienda a la que pertenece el usuario
        data: {tipo:$("#tipo option:selected").val(),id:$("#id").val()},
        success: function (result) {
            $("#cont_tiendas").html(result);
        }
    });
}

function saveCliente(tipo){
    var band=0; var band_nom=0;
    //console.log("tipo: "+tipo);
    if($("#tipo option:selected").val()==1){ //eeuu
        tiendamx=$("#id_destino option:selected").val();
    }else{ //MX
        tiendamx=$("#id_origen option:selected").val();
    }
    //console.log("tiendamx: "+tiendamx);
    if(tipo==1){ //cliente que envia
        if($("#cliente").val()!=undefined){
            id_cli=$("#cliente option:selected").val();
        }else{
            id_cli=0;
        }
        band++;
        if($("#nom_envia").val()!=""){
            band_nom++;  
        }
        form=$("#form_cliente").serialize()+"&tipoc=1&id_cli="+id_cli+"&tiendamx="+tiendamx;
        //console.log("band_nom: "+band_nom);
    }
    if(tipo==2){ //cliente que recibe
        if($("#recibe").val()!=undefined){
            id_cli=$("#recibe option:selected").val();
        }else{
            id_cli=0;
        }
        band++;
        if($("#nom_recibe").val()!=""){
            band_nom++;  
        }
        form=$("#form_cliente_recibe").serialize()+"&tipoc=2&id_cli="+id_cli+"&tiendamx="+tiendamx;
    }
    /*if($("#tipo option:selected").val()==1){ //eeuu
        tipo=2;
    }else{
        tipo=1;
    }*/
    //console.log("tipo: "+tipo);
    if(band>0){
        if(band_nom>0){
            $.ajax({
                type: "POST",
                url: base_url+"Envios/submitCliente",
                data: form+"&tipo="+tipo,
                success: function (result) {
                    if(result==1){
                        Swal.fire({
                            title: 'El cliente ya existe',
                            text: '',
                            type: 'error',
                            confirmButtonClass: 'btn btn-success',
                        });
                    }else{
                        Swal.fire({
                            type: "success",
                            title: "Éxito",
                            text: 'Información guardada correctamente.',
                            confirmButtonClass: 'btn btn-success',
                        });                    
                    }
                },
            }); 
        }
        else{
            toastr.warning("Error!", "Ingrese los datos, el cliente no puede quedar vacio", "error");
        }
    }
}


var row_paq=0;
function addPaquete(){
    var html="";
    row_paq++;
    html+='<tr class="rowpaq_'+row_paq+'""><td>\
                    <input name="id" id="id" value="0" hidden />\
            <div class="row pt-12" style="border-top: 1px solid #398335;"></div><div class="row pt-6">\
                <div class="col-md-2" style="display:none">\
                    <fieldset class="form-group">\
                        <label for="num_sub"># Subpaquetes</label>\
                        <input type="number" id="num_sub" name="num_sub" class="form-control round" placeholder="# SUB" value="1">\
                    </fieldset>\
                </div>\
                <div class="col-md-3">\
                    <fieldset class="form-group">\
                        <label for="libras">Libras</label>\
                        <input type="number" id="libras" name="libras" class="form-control round" placeholder="Libras" value="">\
                    </fieldset>\
                </div>\
                <div class="col-md-3">\
                    <fieldset class="form-group">\
                        <label for="precio">Precio</label>\
                        <input type="number" oninput="calculaTot('+row_paq+')" id="precio" name="precio" class="form-control round precio_'+row_paq+'" placeholder="Precio" value="">\
                    </fieldset>\
                </div>\
                <div class="col-md-3">\
                    <fieldset class="form-group">\
                        <label for="valor_est">Valor estimado</label>\
                        <input type="number" id="valor_est" name="valor_est" class="form-control round" placeholder="Valor estimado" value="">\
                    </fieldset>\
                </div>\
                <div class="col-md-3">\
                    <fieldset class="form-group">\
                        <label for="impuesto">Impuesto</label>\
                        <input type="number" oninput="calculaTot('+row_paq+')" id="impuesto" name="impuesto" class="form-control round impuesto_'+row_paq+'" placeholder="Impuesto" value="">\
                    </fieldset>\
                </div>\
                <div class="col-md-2" style="display:none">\
                    <fieldset class="form-group">\
                        <label for="num_paquete">Paquetes</label>\
                        <input type="number" id="num_paquete" name="num_paquete" class="form-control round" placeholder="Número de Paquetes" value="">\
                    </fieldset>\
                </div>\
            </div>\
            <div class="row pt-6">\
                <div class="col-md-10">\
                    <fieldset class="form-group">\
                        <label for="contenido">Contenido del paquete</label>\
                        <textarea id="contenido" name="contenido" class="form-control round" placeholder="Contenido del Paquete" rows="3"></textarea>\
                    </fieldset>\
                </div>\
                <div class="col-md-2">\
                    <fieldset class="form-group">\
                        <label for="total">Total</label>\
                        <input readonly type="text" id="total" name="total" class="form-control round total_'+row_paq+'" placeholder="Total" value="">\
                    </fieldset>\
                    <button type="button" onclick="eliminaRow('+row_paq+')" class="btn btn-danger mb-2"><i class="ft-trash mr-2"></i></button>\
                </div>\
            </div>\
        </td></tr>';

    $("#table_servicios").append(html);

}

function eliminaRow(row){
    $('.rowpaq_'+row).remove();
    row_paq--;
    calculaSuper();
}

function calculaTot(row){

    /*if($(".precio_"+row+"").val()!="" && $(".impuesto_"+row+"").val()!=""){
        $(".total_"+row+"").val(parseFloat($(".precio_"+row+"").val())+parseFloat($(".impuesto_"+row+"").val()));
    }*/

    var precio=$(".precio_"+row+"").val();
    var impuesto= $(".impuesto_"+row+"").val();
    precio=Number (precio); 
    impuesto=Number (impuesto);
    var suma = parseFloat(precio)+parseFloat(impuesto);
    $(".total_"+row+"").val(suma);
    calculaSuper();
}

function calculaTot_e(row){
    var precio=$(".precio_e_"+row+"").val();
    var impuesto= $(".impuesto_e_"+row+"").val();
    precio=Number (precio); 
    impuesto=Number (impuesto);
    var suma = parseFloat(precio)+parseFloat(impuesto);
    $(".total_e_"+row+"").val(suma);
    calculaSuper();
}


function calculaSuper(){
    var final=0;
    $("input[name*='total']").each(function() {
        total = $(this).val();
        //console.log("total: "+total);
        var stotal = total;
        final += Number(stotal);
    });
    $("#sup_tot").val(final);
}

function save() {
    if(validar_total==0){
        $('#form_cliente').removeData('validator');
            var form_register = $('#form_cliente');
            var error_register = $('.alert-danger', form_register);
            var success_register = $('.alert-success', form_register);
            var $validator1=form_register.validate({ //para peps
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            /*rules: {
                nom_envia:{
                  required: true
                },
                edo_envia:{
                  required: true
                },
                tel_envia:{
                  required: true
                }
            },*/
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);
            },
            highlight: function (element) { // hightlight error inputs

                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        $('#form_cliente_recibe').removeData('validator');
            var form_register2 = $('#form_cliente_recibe');
            var error_register = $('.alert-danger', form_register2);
            var success_register = $('.alert-success', form_register2);
            var $validator1=form_register2.validate({ //para peps
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nom_recibe:{
                  required: true
                },
                apellido_recibe:{
                  required: true
                },
                /*
                edo_recibe:{
                  required: true
                },
                ciudad_recibe:{
                  required: true
                },
                */
                tel_recibe:{
                  required: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register2,-100);
            },
            highlight: function (element) { // hightlight error inputs
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        $('#form_dest_orig').removeData('validator');
            var form_register3= $('#form_dest_orig');
            var error_register = $('.alert-danger', form_register3);
            var success_register = $('.alert-success', form_register3);
            var $validator1=form_register3.validate({ //para peps
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                folio:{
                  required: true
                },
                id_destino:{
                  required: true
                },
                id_origen:{
                  required: true
                }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register3,-100);
            },
            highlight: function (element) { // hightlight error inputs
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        $('#form_paquete').removeData('validator');
            var form_register4= $('#form_paquete');
            var error_register = $('.alert-danger', form_register4);
            var success_register = $('.alert-success', form_register4);
            var $validator1=form_register4.validate({ //para peps
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                /*num_sub:{
                  required: false
                },*/
                libras:{
                  required: false
                },
                precio:{
                  required: false
                },
                valor_est:{
                  required: false
                },
                impuesto:{
                  required: false
                },
                num_paquete:{
                  required: false
                },
                contenido:{
                  required: false
                }
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
                $(".vd_red").attr('style',  'color:red');
            }, 
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register4,-100);
            },
            highlight: function (element) { // hightlight error inputs
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });

        var valid = form_register.valid();
        var valid2 = form_register2.valid();
        var valid3 = form_register3.valid();
        var valid4 = form_register4.valid();
        if(valid && valid2 && valid3 && valid4) {
            /*var v1=validar_clientes(1);
            var v2=validar_clientes(2);
            if(v1==0 && v2==0){*/
                $.ajax({
                    type: "POST",
                    url: base_url+"Envios/submit",
                    data: $("#form_cliente, #form_cliente_recibe, #form_dest_orig").serialize()+"&efectivo_cantidad="+$("#efectivo_cantidad").val()+"&tarjeta_cantidad="+$("#tarjeta_cantidad").val(),
                    beforeSend: function(){
                        $("#btn_submit").attr("disabled",true);
                    },
                    success: function (result) {
                        //console.log(result);
                        var DATA  = [];
                        var TABLA = $("#tabla_paqts tbody > tr"); 
                        TABLA.each(function(){         
                            item = {};
                            item ["id"] = $(this).find("input[id*='id']").val();
                            item ["num_sub"] = $(this).find("input[id*='num_sub']").val();
                            item ["libras"] = $(this).find("input[id*='libras']").val();
                            item ["precio"] = $(this).find("input[id*='precio']").val();
                            item ["valor_est"] = $(this).find("input[id*='valor_est']").val();
                            item ["impuesto"] = $(this).find("input[id*='impuesto']").val();
                            item ["num_paquete"] = $(this).find("input[id*='num_paquete']").val();
                            item ["contenido"] = $(this).find("textarea[id*='contenido']").val();
                            item ["total"] = $(this).find("input[id*='total']").val();
                            item ["id_envio"] = result;
                            DATA.push(item);
                        });
                        INFO  = new FormData();
                        aInfo = JSON.stringify(DATA);
                        INFO.append('data', aInfo);
                        /*console.log("INFO: "+INFO);
                        console.log("aInfo: "+aInfo);
                        console.log("DATA: "+DATA);*/
                        $.ajax({
                            data: INFO,
                            type: 'POST',
                            url: base_url+'Envios/submitDatosPaquete',
                            processData: false, 
                            contentType: false,
                            async: false,
                            success: function(data2){
                                $("#btn_submit").attr("disabled",false);
                                toastr.success('Envío guardado correctamente', 'Éxito');
                                setTimeout(function () { 
                                    window.location.href = base_url+"Envios";
                                }, 1500);
                            }
                        });
                        /*$.ajax({
                            type: "POST",
                            url: base_url+"Envios/submitDatosPaquete",
                            data: $("#form_paquete").serialize()+"&id_envio="+result,
                            success: function (data) {
                                $("#btn_submit").attr("disabled",false);
                                toastr.success('Envío guardado correctamente', 'Éxito');
                                setTimeout(function () { 
                                    window.location.href = base_url+"Envios";
                                }, 1500);
                            }
                        });*/

                    },error: function(response){
                        toastr.warning("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error");
                    }
                });
            /*}else{
                if(v1==1 && v2==1){
                    Swal.fire({
                        title: 'El clientes de envío ya existe y el cliente que recibe ya existe',
                        text: '',
                        type: 'error',
                        confirmButtonClass: 'btn btn-success',
                    });
                }else if(v1==1 && v2==0){
                    Swal.fire({
                        title: 'El cliente de envío ya existe',
                        text: '',
                        type: 'error',
                        confirmButtonClass: 'btn btn-success',
                    });
                }else if(v1==0 && v2==1){    
                    Swal.fire({
                        title: 'El cliente que recibe ya existe',
                        text: '',
                        type: 'error',
                        confirmButtonClass: 'btn btn-success',
                    });
                }
                
            }*/
        }
    }else{
        if(validar_total==1){
            Swal.fire({
                title: 'El precio es mayor a la cantidad',
                text: '',
                type: 'error',
                confirmButtonClass: 'btn btn-success',
            });
        }else{
            Swal.fire({
                title: 'El precio es menor a la cantidad total', 
                text: '',
                type: 'error',
                confirmButtonClass: 'btn btn-success',
            });
        }
        
    }

}

function get_validar(){
    var num=validar_clientes(1);    
    alert(num);
}


function validar_clientes(tipo){
    var vli=0;
     if(tipo==1){ //cliente que envia
        if($("#cliente").val()!=undefined){
            id_cli=$("#cliente option:selected").val();
        }else{
            id_cli=0;
        }
        form=$("#form_cliente").serialize()+"&tipoc=1&id_cli="+id_cli;
        //console.log("band_nom: "+band_nom);
    }
    if(tipo==2){ //cliente que recibe
        if($("#recibe").val()!=undefined){
            id_cli=$("#recibe option:selected").val();
        }else{
            id_cli=0;
        }
        form=$("#form_cliente_recibe").serialize()+"&id_cli="+id_cli;
    }
    $.ajax({
        type: "POST",
        url: base_url+"Envios/validarCliente",
        data: form+"&tipo="+tipo,
        async: false,
        success: function (result){
            vli=result;
        },
    });
    return vli;
}

function validar_cantida_total(num){
    var sup_tot=$("#sup_tot").val();
    sup_tot=Number (sup_tot); 
    var efectivo_cantidad= $("#efectivo_cantidad").val();
    var tarjeta_cantidad= $("#tarjeta_cantidad").val();
    efectivo_cantidad=Number (efectivo_cantidad); 
    tarjeta_cantidad=Number (tarjeta_cantidad);
    var suma = parseFloat(efectivo_cantidad)+parseFloat(tarjeta_cantidad);
    //alert(sup_tot);
    if(sup_tot==suma){
        validar_total=0;
        /*var resta=parseFloat(sup_tot)-parseFloat(suma);
        if(num==1){
            $("#tarjeta_cantidad").val(resta);
        }else{
            $("#efectivo_cantidad").val(resta);
        }*/
    }else if(sup_tot<suma){
        validar_total=1;
        /*var resta=parseFloat(sup_tot)-parseFloat(suma);
        if(num==1){
            $("#tarjeta_cantidad").val(resta);
        }else{
            $("#efectivo_cantidad").val(resta);
        }*/
        Swal.fire({
            title: 'El precio es mayor a la cantidad',
            text: '',
            type: 'error',
            confirmButtonClass: 'btn btn-success',
        });
    }else if(sup_tot>suma){
        validar_total=2;
        
    }
}